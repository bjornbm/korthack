\REM{

\FANTASY

\REM{
Ljuskälla                           | Styrka    | Slå \D6 om tappas
------------------------------------|-----------|----------------------------
Talgljus     \ex{förbrukningsvara}  | person    | ≤5 slocknar
Fackla       \ex{förbrukningsvara}  | klunga    | =1 slocknar
Oljelampa    \ex{småsak}            | klunga    | ≤4 trasig, ≤5 släckt
Plåtlykta    \ex{verktyg}           | klunga    | =1 trasig, ≤4 släckt
Riktad lykta \ex{specialistverktyg} | \närax    | ≤3 trasig, ≤4 släckt
}

| Ljuskälla     | Brinntid        | Maximal räckvidd         |
|---------------|-----------------|--------------------------|
| Ljus          | 6–8 timmar      | ett par meter            |
| Fackla        | cirka en timme  | \närax{}                 |
| Lykta         | 6–8 timmar      | \nära{}                  |
| Riktad lykta  | 6–8 timmar      | \långtx{} i en riktning  |

Lyktor förbränner lampolja. En riktad lykta har en spegel och en lins som koncentrerar ljuset i en smal kägla.

En ljuskällas förbränning markeras i steg med \checkS{} \ra{} \checkH{} \ra{} \checkD{} (utbrunnen).
En fackla brinner i bästa fall i \timmarx{} och markeras efter varje \scenx{}.
Övriga ljuskällor markeras efter \timmar{}.

}
\REM{
En ljuskälla ger fullgott ljus åt ett antal närbelägna karaktärer och _svagt ljus_ till alla andra inom dess räckvidd.
I svagt ljus har du \nackdelx{} på allt som kräver god syn.

\Newcommand{\Avs}{avstånd}

| Ljuskälla     | Ljus åt…      | Brinntid            | \Räckviddx              |
|---------------|---------------|---------------------|-------------------------|
| Fackla        | bäraren+1     | \timmarx            | \nära                   |
| Vaxljus       | bäraren       | \trippelt{\timmar}  | några meter             |
| Lykta         | bäraren+2     | \trippelt{\timmar}  | \nära                   |
| Riktad lykta  | bäraren       | \trippelt{\timmar}  | \långt{} i en riktning  |

Lyktor förbränner lampolja. En riktad lykta har en spegel och en lins som koncentrerar ljuset i en smal kägla.

\REM{En ljuskällas förbränning markeras \checkS \ra \checkH \ra \checkD{} (utbränd).}
}

\REM{
En ljuskälla ger \intro{fullgott ljus} åt ett antal närbelägna karaktärer \ex{inklusive bäraren} och \intro{svagt ljus} till alla andra i närheten.
I svagt ljus har du \nackdelx{} på allt som kräver god syn.

*   En \intro{fackla} ger ljus åt 2 karaktärer och svagt ljus på \närax{} håll.
*   Ett \intro{vaxljus} ger ljus åt bäraren och svagt ljus på några meters håll.
*   En \intro{oljelykta} ger ljus åt 3 karaktärer och svagt ljus på \nära{} håll.\REM{
    De förbränner lampolja och brinner i 6–8 timmar.}
*   En \intro{riktad lykta} är en oljelykta med en spegel och en lins som koncentrerar ljuset i en \REM{smal} kägla.
    Den ger fullgott ljus åt bäraren och svagt ljus på \långt{} håll i käglans riktning.

# Belysning
}

En ljuskälla ger fullgott ljus åt ett antal närbelägna karaktärer och _svagt ljus_ till alla andra inom dess räckvidd.

\Newcommand{\Avs}{avstånd}

| Ljuskälla     | Ger ljus åt…  | Räckvidd \ex{svagt ljus}  |
|---------------|---------------|---------------------------|
| Fackla        | bäraren+1     | \narax                    |
| Vaxljus       | bäraren       | några meter               |
| Oljelykta     | bäraren+2     | \nara                     |
| Riktad lykta  | bäraren       | \langt{} i en riktning    |

\REM{En riktad lykta är en oljelykta med en spegel och en lins som koncentrerar ljuset i en smal kägla.}

I svagt ljus får du \nackdelx{} på allt som kräver god syn, inklusive \attackerx{}.
Om du slåss i \intro{mörker} så måste du dessutom först \slafor{\HVD} för att lokalisera ditt mål.

\REM{I svagt ljus har du \nackdelx{} på allt som kräver god syn.
I \intro{mörker} \attackerarx{} \REM{slåss?} du med \nackdel{} och använder du \avstandsvapenx{} så måste du dessutom först \slafor{\HVD} för att lokalisera ditt mål.\REM{Ointressant krångligt för avståndsvapen?}
}

\REM{
I \intro{mörker} \attackerarx{} du med \nackdel{} och med \avståndsvapenx{} måste du dessutom först \slåför{\HVD} för att lokalisera ditt mål.
}


\cred{Torchbearer för antal personer och svagt ljus?}
