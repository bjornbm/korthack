\REM{Du börjar spelet med ett i alla \GE{}
} När du börjar spela är alla dina \GE{} lika med ett.
Du kan öka en \GE{} med ett genom att spendera lika många \introA{erfarenhetspoäng}{ep} som dess nuvarande värde.
Du får ett \introabbr{ep} för varje spelmöte du överlever.
