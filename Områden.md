En strid kan utspelas i ett eller flera \intro{områden} med olika karaktär:

*   \intro{Trånga} områden har begränsat svängrum
    \ex{trånga trappor,\
    balkonger, dörröppningar, smala korridorer/gränder/tunnlar…}.

*   \intro{Riskabla} områden har underlag/sikt som kräver aktsamhet\
    \ex{branta hustak/trappor, smala broar/avsatser, träsk, dimma/rök…}.

*   \intro{Plottriga} områden är fulla med bös som kommer emellan\
    \ex{butiker, tät växtlighet, slagfält, tältläger, skeppsdäck, lager…}.

*   \intro{Öppna} områden erbjuder ingenting att ta skydd bakom\
    \ex{torg, fält, stora salar, himlen, öppet vatten…}.

*   \intro{Neutrala} områden saknar utmärkande karaktär.

\cred{
  OSH
}
