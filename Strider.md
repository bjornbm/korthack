I \striderx{} används två \intro{avstånd}:

*   \intro{Nära} är inom cirka 20 meter.
*   \intro{Långt} är bortom cirka 20 meter.

\noindent
Om din rörlighet inte är begränsad så hinner du på ditt \dragx{} både förflytta dig \keyI{nära} och göra något annat.
Du får också byta vad du håller i händerna en gång per \dragx{}\REM{ utan kostnad / även om du gör något annat}.

Du kan \intro{attackera} någon för att skada eller med våld tvinga henom till något.
För varje \traff{} måste hen välja mellan att

1.  förlora en \abbr{fp},
2.  underkasta sig din vilja,
3.  få \keyxx{livshotande skador}.

\noindent
Om hen väljer 2 eller 3 ovan så får du genast \foljaupp{} med en ny \termI{attack} mot någon annan i närheten \ex{gäller ej för \motattacker{}}.

När du inleder eller lämnar en närkamp med någon får hen, om hen inte är överraskad eller i närkamp med någon annan, först utföra en \motattack{} mot dig.
\REM{Den fungerar som en vanlig \attack{} men sker på ditt \dragx{} och får inte \foljasupp{}.}
\REM{Den sker på ditt \dragx{}, innan din egen \attack{} eller förflyttning.}
