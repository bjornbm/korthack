Tiden i \varldenx{} förflyter i \intro{rundor} vars längder varierar från några sekunder till timmar eller dagar beroende på situationen.
I varje \keyI{runda} utför ni \intro{drag} i turordning, vanligtvis medurs runt bordet.
På ditt \keyI{drag} får du \REM{försöka} göra något som rimligtvis kan hinnas med i \keyI{rundan}.

Om det inte är givet huruvida turordningen bör börja med \RP{} eller \SL{} i en viss situation så får alla \RP{} \slafor{\HVD}.
Om du \lyckas{} får du utföra ett \dragx{} innan \SL{}.






\REM{
Tiden i \varldenx{} förflyter i \intro{rundor} vars längder varierar från några sekunder till timmar eller dagar beroende på situationen.

Varje \keyI{runda} utför spelare \REM{och \SL{}} \intro{drag} i turordning.
Turordningen går medurs runt bordet och börjar med \SL{} \REM{och \SLP{}} eller spelaren till vänster om \SL{} beroende på omständigheterna.

På ditt \keyI{drag} så får du försöka göra något som rimligtvis kan hinnas med i \keyI{rundan};
berätta vad du vill åstadkomma \REM{(ditt _mål_)} och hur du vill åstadkomma det\REM{ (din _metod_)}!
Om utfallet inte är givet kan du behöva \xr{slå tärningar}{grundegenskapsslag} för att se hur det går.
}
\REM{
Du kan också välja att vänta för att reagera på andras \intro{drag}.
Berätta i så fall vad du väntar på och vad du vill göra när det inträffar.
}
