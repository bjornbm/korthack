\REM{
--- ------------ ----------- ------------ ------------
  1  Elmas       Elas         Maera       Saida
  2  Corin       Adresin      Elidyr      Enania
  3  Ervyre      Arthion      Necia       Ilyrana
  4  Bryven      Arryn        Elune       Tialha
  5  Fernaco     Ruven        Langaria    Trisbella
  6  Sigil       Elrindel     Grebella    Nyana
  7  Artin       Virion       Mavu        Pafina
  8  Alewar      Goren        Deva        Finnea
  9  Amyntas     Tomo         Alanela     Matia
 10  Rolim       Estar        Alona       Saelihn
 11  Roshan      Olwyn        Nola        Anhaern
 12  Ralocan     Valcan       Garda       Merith
--- ------------ ----------- ------------ ------------

# Barbarnamn

--- ------------ ----------- ------------ ------------
  1 Thidrik      Konal       Kadlin       Jofrid
  2 Stuf         Audolf      Sigrunn      Gudfrid
  3 Hafgrim      Gærrar      Heith        Arnora
  4 Karli        Bjarki      Rannveig     Freygerd
  5 Kaupmann     Jomar       Signy        Svala
  6 Svinulf      Nærfi       Alfeid       Gyrd
  7 Thangbrand   Asfrith     Hrefna       Hallbera
  8 Hallmund     Hjarrandi   Halla        Geirhild
  9 Asmund       Asgeir      Svanlaug     Runa
 10 Hallvard     Sigguatr    Hallfrid     Yri
 11 Vigot        Sigeræd     Ljufa        Luta
 12 Sævil        Thorgest    Thorhild     Audhild
--- ------------ ----------- ------------ ------------

# Namn
}

Epitet är vanliga, efternamn är sällsynta.
Vanliga \intro{mansnamn}:

\noindent
\columnTwo

1.  Arryn
1.  Artin
1.  Bryven
1.  Corin
1.  Elas
1.  Elmas
1.  Estar
1.  Goren
1.  Jomar
1.  Karli

\next{\columnTwo}

11. Konal
1.  Ralocan
1.  Rolim
1.  Roshan
1.  Ruven
1.  Sigil
1.  Thidrik
1.  Tomo
1.  Valcan
1.  Vigot

\columnend
\vspace{6pt}

\noindent
Vanliga \intro{kvinnonamn}:

\noindent
\columnTwo

1.  Alanela
1.  Arnora
1.  Deva
1.  Fernaco
1.  Garda
1.  Grebella
1.  Halla
1.  Jofrid
1.  Kadlin
1.  Langaria

\next{\columnTwo}

11. Maera
1.  Matia
1.  Necia
1.  Nola
1.  Pafina
1.  Sigrunn
1.  Svala
1.  Tialha
1.  Trisbella
1.  Yri

\columnend

\cred{ https://www.fantasynamegenerators.com/elf_names.php }
\cred{ https://www.fantasynamegenerators.com/viking_names.php }
