# Fluff till framsidan

- titel
- setting och känd metaplot (som "abstract")
- bild

# Utfyllnad till regeluppslaget

- bilder/ornament
- titel "Regler"?

# Tabeller till baksidan

- credits
  - inspiration
  - speltestare

Användbart i spel:

- prislista
- arméns organisation
- arméns grader
- karta

Användbart vid karaktärsskapande (in order of importance?):

- namn
- startutrustning
- relationer? (följeslagare)
- hemby
- tidigare yrke
- varför i armén
- utseende/särdrag
- personlighetsdrag

# Rollformulär (mönstringsformulär?)

KRP, HVD, KNS
EP
Pengar

Utrustning
Pengar
