Du belönas med 1 \intro{ära} varje gång du uträttar något som torde öka ditt anseende i andra äventyrares ögon.
Till exempel när du:

*   deltar i en expedition,
*   återvänder från en expedition under kontrollerade former,
*   avslutar ett uppdrag åt \xr{äventyrargillet}{äventyrargillet}, \REM{ eller är belöningen pengar? }
*   vinner en betydande seger.

\REM{
*   skriver en krönika om en expedition,
*   kartlägger en ny äventyrsplats,
*   upptäcker någonting nytt och viktigt om världen, \cred{DW}
}

Du använder din \intro{ära} till att öka din \nivåx{}.
Att öka din \nivå{} med +1 kostar lika mycket \intro{ära} som \trippelt{din nuvarande \nivå{}}.

Dina hantlangare belönas med hälften så mycket \intro{ära} som du, avrundat nedåt. Det kostar 1 \intro{ära} för en hantlangare att öka sin \nivå{} från 0 till 1.

\TODO{ Hur mycket ära behöver en hantlangare med nivå-0 för att levla? }
