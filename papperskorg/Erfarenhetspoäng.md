Efter en expedition belönas du med \intro{erfarenhetspoäng} (\introEP{}) för sådant du uträttat eller upplevt som torde öka ditt anseende i andra äventyrares ögon.
Till exempel får du 1 \EP{} om du:

*   deltog i expeditionen,
*   återvände från expeditionen under kontrollerade former,
*   dokumenterar expeditionen,
*   gick till botten med ett uppdrag eller rykte, eller \REM{ eller är belöningen pengar? }
*   vann en betydande seger.

\REM{*   kartlägger en ny äventyrsplats,
*   upptäcker någonting nytt och viktigt om världen, \cred{DW}
*   varit \döendex{} men överlevt.
}

\REM{
Mer rikligt med EP? Skulle kunna belöna för diverse:

* delta i expedition
* komma hem under kontrollerade former
* lyckas med en prövning
* misslyckas med en prövning
* bli sårad
* dräpa en fiende
* finna skatter
* förlora en hantlangare
* gå i en fälla
* upptäcka/undvika en fälla
* skriva krönika
* avsluta uppdrag
* vinna en betydande seger
}
