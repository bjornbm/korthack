Om du attackerar någon i närstrid som redan har utsatts för en närstridsattack av någon annan den här \rundanx{} så får du \fördelx{} på din attack.
