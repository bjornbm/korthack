Välj värderingar:\
\columnTwo

1. medkänsla
2. lärande
3. samarbete
4. loyalitet
5. disciplin
6. optimism
7. mod
8. professionalism
9. generösitet
10. andlighet

\columnend
\columnTwo

11. lycka?
18. ihärdighet?
7. ärlighet
19. påhittighet?
8. heder
20. pålitlighet
9. ödmjukhet
21. visdom
10. humor?
22. kunskap
11. frihet
12. ledarskap

\columnend

# Svagheter

Välj svagheter.\
\columnTwo

1. tankspridd
2. arrogant
3. pratkvarn
4. grym
5. avhängig/klängig?
6. illoyal
7. egocentrisk/narcisistisk?
8. avundsjuk
9. fanatisk
10. glupsk

\columnend
\columnTwo

11. lättlurad(17?)
12. hedonist?(10?)
13. humorlös?
14. obildad
15. fördömande
16. lögnare
17. naiv
18. självsäker
19. vårdslös
20. självisk
21. envis
22. temperamentsfull
23. fåfäng
24. girig
25. högmodig(20?)

\columnend
