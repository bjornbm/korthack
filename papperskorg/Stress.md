Att befinna sig i underjordens tryckande vrår under en längre tid är mentalt påfrestande och nerbrytande.
Mörkret och de ständ\br{}iga farorna göder paranoia, tvivel och hopplöshet.

Markera en \IP{} med \checkD{} för varje dag (eller \dagarx{}) du befinner dig i underjorden eller en jämförbart nedbrytande miljö.
Om alla \IP{} redan är markerade med \checkD{} markerar du istället \FP{} med \checkD{}.

\cred{ Inspiration från Torchbearers grind }

