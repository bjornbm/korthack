När alla dina \KP{} markerats med \checkD{} är du \intro{döende}.
\Prövax{} \KRP{} omedelbart, varje gång du tar ytterligare \skadax{}, samt varje gång det blir ditt \dragx{}.
Om du bär en \intro{hjälm} så får du \fördel{}.

*   Om du \misslyckas{} så dör du.
*   Om du \lyckas{} överlever du en kort stund till.
*   Om du \lyckasexceptionelltbra{} så stabiliseras ditt tillstånd och så länge du inte tar ytterligare \skada{} behöver du inte göra någon mer \prövning{}.

Någon annan kan som ett \drag{} stabilisera ditt tillstånd.

\REM{Om du gjort dig själv döende så kan ditt tillstånd inte stabiliseras.}

\cred{ ICRPG: timer och stabilisera }
\cred{ Halberds & Helmets: hjälmar }
