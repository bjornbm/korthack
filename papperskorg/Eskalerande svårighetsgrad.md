Vanligtvis behöver du slå 5 eller högre för att \lyckas{} när du \prövar{} en \GE{}
men \SL{} kan höja detta tröskelvärde tillfälligt för att representera ökad stress och allvar i en \scenx{}.

Till exempel kan \SL{} höja tröskelvärdet till 6 i en grottas inre regioner, och kanske till 7 i era mörkaste stunder.

Det nya tröskelvärdet gäller för alla dina \prövningar{} i \scenen{} (men inte för dina fienders).
Chansen att \lyckasexceptionelltbra{} samt risken att \misslyckaskapitalt{} påverkas inte.

\cred{ICRPG}
\cred{https://www.gamedesignnotes.com/quick-thoughts-on-icrpg/}
