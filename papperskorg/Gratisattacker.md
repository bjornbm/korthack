\REM{
Om du inte redan är inblandad i närstrid med någon annan och en fiende försöker ta sig ur en strid med dig, ta sig förbi dig, eller ta tag i dig så får du omedelbart göra en attack mot henom. Attacken räknas inte som en \dragx{}.
}

Du får omedelbart och utan att det räknas som ditt \dragx{} utföra en \närstridsattackx{} mot en fiende som:

*   försöker ta sig förbi dig,\REM{
*   försöker dra sig ur en närstrid med dig, eller}
*   försöker gå in i närstrid med dig om du har ett \långtvapenx{} och hen inte har det.

Du måste se fienden och får inte redan vara inblandad i närstrid med någon annan.

\REM{Attacken räknas inte som ett \dragx{}.}
