# Utrustning {#utrustning}

\FANTASY

En del utrustning kan ge dig \fördelx{} (en jakthund när du ska spåra ett monster),
medan avsaknaden av annan utrustning kan ge dig \nackdel{} (dyrkar när du ska dyrka upp ett lås).

En del utrustning kan öka din \verkanx{} (fina kläder när du ska imponera på någon).

Vissa handlingar är omöjliga utan rätt utrustning (skjuta någon utan pilar).

Den mesta utrustning kan köpas, vanligtvis enligt \xr{prislistan}{prislista}, men tillgång och pris kan variera från plats till plats.
Fråga \SL{} om det är oklart vad ett visst föremål har för kategori i prislistan eller om det ger någon regelteknisk effekt.

