I närstrid kan du hugga/sticka/slå ner flera (jämförelsevis) svaga fiender med en enda attack.
Du får sprida din \verkanx{} över flera fiender i en grupp förutsatt att din \verkan{} är tillräcklig för att \xr{bryta}{kroppspoäng} dem alla.

