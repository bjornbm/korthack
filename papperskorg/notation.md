# Notation

En utmaning med, till exempel, \motstånd{} 1 och \arbete{} 3, mot vilken en handling tar \timmarx{} och \VIS{} ger \verkan{}, kan skrivas \utm{1:3 \VIS _h_}.
Om \verkan{} eller tidsåtgång är underförstådda utelämnas de (\utm{1:3}).
För \barnlekar{} används hakparenteser (\lätt{1:3}).

Ett vapens \verkanx{} och/eller arenor\sec{arenor} kan skrivas \dmg{\verkan, arenor} efter dess namn.
Exempel: dolk\dmg{\STY,\trånga}.


{barnlek 3:2 \UTS{} _s_}

[barnlek 3:2 \UTS{} _s_]

(barnlek 3:2 \UTS{} _s_)

(prövning 1:5 \VIS{} _h_)

