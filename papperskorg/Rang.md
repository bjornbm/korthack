Din \intro{rang} är ett mått på
din ryktbarhet som äventyrare, vilket i sin tur avspeglar
din erfarenhet, framgång och förmåga.
Som medlem i \xr{äventyrargillet}{äventyrargillet} bär du en hederstitel efter rang:
\REM{_fackelbärare_ (rang 1), _vägvisare_ (rang 2), _äventyrare_ (rang 3), _utforskare_ (rang 4), och _erövrare_ (rang 5).}

|         |                |
|---------|----------------|
| rang 1: | _fackelbärare_ |
| rang 2: | _stigfinnare_  |
| rang 3: | _äventyrare_   |
| rang 4: | _utforskare_   |
| rang 5: | _erövrare_     |

\REM{Du ökar din rang genom att betala \trippelt{din nuvarande nivå} \intro{ära}.}

Varje gång din rang ökar får du höja två olika \GE{} med +1 samt välja mellan att bli \bra{} på något nytt eller \mktbra{} på något du redan är \bra{} på.
Du kan aldrig bli bättre på det du är \dålig{} på.

\REM{
Majoriteten av icke äventyrande bönder, bybor och stadsbor har rang 0. \FANTASY
En karaktär med rang 0 har 1 i alla \GE{} och är \brax{} på en \xr{sak}{färdigheter}, \dålig{} på en sak, och \hyfsad{} på allt annat.
}
