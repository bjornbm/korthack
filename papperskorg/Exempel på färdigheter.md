Fråga \SL{} hur brett eller smalt det du är \bra{}/\dåligx{} på bör vara.
Här är exempel på breda kategorier \ex{och smala specialiteter}:

1.  Akrobatik     \ex{klättra, hoppa, balansera, simma, springa, kasta…}
2.  Djur          \ex{rida, hantera, dominera, zoologi, förstå beteenden…}
3.  Försvar       \ex{blockera, parera, befästa…}
4.  Hantverk      \ex{laga, skapa, bygga, förädla, fällor, dyrka…}
5.  Kultur        \ex{språk, religion, sägner, musik, dans, uppträda…}
6.  Lönndom       \ex{smyga, gömma, söka, speja, ficktjuveri…}
7.  Slåss         \ex{svärd, yxor, slagsmål, brottas, fasthålla…}
8.  Skjuta        \ex{pilbågar, armborst, slungor, blåsrör…}
9.  Överlevnad    \ex{jaga, fiska, navigera, spåra, motstå åkommor…}
10. Övertala      \ex{leda, förhandla, förföra, ljuga, bluffa, skrämma…}
\REM{8.  Uppmärksamma  \ex{spana, lyssna, spåra, söka, läsa personer…}}

\REM{
--- ------------------------ --- ------------------------
  1 slåss med svärd       S   11 skjuta pilbåge      P
  2 slåss med långa vapen S   12 blockera med sköld  -
  3 kasta                 S   13 fällor och lås      P
  4 simma                 S   14 motså gifter        T
  5 klättra och hoppa     S   15 läkekonst           V
  6 smyga och gömma sig   P   16 musik och dans      U
  7 övertala              U   17 jaga och spåra      V
  8 ljuga                 U   18 fasthållning        S
  9 hantverk              V   19 språk               V
 10 springa               ST  20 religionskunskap    V
--- ------------------------ --- ------------------------

## \STY

slåss med svärd       
slåss med långa vapen 
slåss med krossvapen  
kasta                 
klättra och hoppa     akrobatik
simma                 

## \TÅL

motstå åkommor        
springa               

## \PRE

skjuta pilbåge        
smyga och gömma sig   lönndom
fällor och lås        fingerfärdighet
balansera             akrobatik

## \VIS

hantverk              
språk                 
läkekonst             
jaga och spåra        
religionskunskap      

## \UTS

övertala              
ljuga                 
skrämma               
musik och dans        

## Överblivna

blockera med sköld    
försvara              
hantera/dominera djur V/U
historieberättande    U

----------------- --------------------------
närstrid          STY
kasta             STY (kastvapen)
skjuta            PRE
akrobatik         STY/PRE/TÅL (klättra, hoppa, balansera, springa, simma)
smyga             PRE (smyga, gömma sig)

religion          VIS/UTS (kunskap, utövande, predika, förstå)
språk             VIS (tala, förstå, läsa, känna igen)
övertala          UTS (förhandla, förföra, köpslå, befalla, ljuga, skrämma)
uppträda          UTS (musik, dans, historieberättande) UTS
djur              VIS/UTS (rida, hantera, dominera, zoologi, förstå beteenden)

jaga              VIS (jaga, spåra, fiska, gillra fällor)
fingerfärdighet   PRE (dyrka, ficktjuva, desarmera)
läkekonst         VIS
motstå            TÅL (gifter, åkommor, kyla, värme)
navigera          VIS (hitta, kartor, lokalsinne, väderstreck)

hantverk          VIS/PRE (laga, skapa, bygga, förädla)
----------------- --------------------------

1.  akrobatik        S/P/T (klätta, hoppa, balansera, simma, springa, kasta)
2.  djur             V/U (rida, hantera, dominera, förstå beteenden)
3.  hantverk         V/P (dyrka, fällor, laga, skapa, bygga, förädla)
4.  kultur           V/U (språk, religion, uppträda)
5.  lönndom          P (smyga, gömma, ficktjuveri)
6.  närstrid         S (inklusive blockera/försvara/fasthålla)
7.  skjuta           P
8.  uppmärksamma     V (spana, spåra, söka, höra, läsa situationer/personer)
9.  överlevnad       V (läkekonst, jaga, spåra, fiska, navigera, motstå åkommor/miljöer)
10. övertala         U (leda, förhandla, förföra, ljuga, bluffa, skrämma)

en vapensort (svärd/yxor/krossvapen…)
en vapentyp (lätta/långa…)
svärd
yxor
krossvapen
stickvapen
skjuta (pilbågar/slungor…)
skjuta (lätta/tunga)

sjöfart, jordbruk
}
