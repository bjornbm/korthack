Din \intro{nivå} är ett mått på din erfarenhet, framgång och förmåga.
Majoriteten av icke äventyrande bönder, bybor och stadsbor har nivå 0. \FANTASY
En karaktär med nivå 0 har 1 i alla \GE{}.

Som äventyrare startar du med nivå 1 och får öka den med 1 efter varje expedition du deltar i.
Varje gång din nivå ökar så får du höja en \GE{} med +1.
\REM{\Hantlangaresx{} nivåer ökar på samma sätt.}
