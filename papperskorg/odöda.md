# Lägre odöda { #lägreodöda }

Zombie \utm{1:4}
:   \bra{} på lukta, \dålig{} på springa.
    Kan inte både förflytta sig och handla på \ögonblickx{}.
    Grepp\dmg{1} håller fast\utm{1:1 \STY}, bett\dmg{2} (markera \KP{} med \checkD{}) mot fasthållen förvandlar dödad till zombie inom \D6 \ögonblick{}.

Skelett \utm{0:2}
:   \bra{} på höra, \dålig{} på smyga.
    Glesa kroppar ger \motstånd{} 2 mot skjutvapen.
    \ex{Kan bära vapen/rustning/sköld.}

Likätare \utm{0:4}
:   \bra{} på lönndom, \dålig{} på samarbete.
    Klor\dmg{2} paralyserar\utm{0:4 \VIS} vid skada, bett\dmg{3}.
    \ex{Anfaller ur bakhåll och släpar iväg paralyserade offer för att äta dem.}

Gast \utm{0:4}
:   4 \IP, \bra{} på skrämma (offer slår för moral), \dålig{} på XXX.
    Immun mot mundana vapen.
    Isande andedräkt\dmg{2} ignorerar rustning.
    \ex{Kan passera genom materia, men är platsbunden.}

# Högre odöda

Vålnad \utm{0:4}
:   6 \IP, \bra{} på lukta, \dålig{} på XXX.
    Immun mot icke-försilvrade mundana vapen.
    Stryptag{3} håller fast\utm{3:1 \STY},
    skräckinjagande aura ger \nackdel{} till alla inom 5 meter.
    \ex{Kan bära vapen/rustning/sköld.}
    \cred{ Monster of the Week (aura), Hackmaster (silver) }

\REM{
# An Aura Of Terror

> An ancient wraith has an aura of terror. Anyone within 5 metres of the wraith takes -1 ongoing, unless they act under pressure to resist the fear.

This is a penalty move, triggered by being close to the monster. It's designed to make attacking the ghost up close harder, which may get the hunters thinking of alternative ways to defeat it.
}

Mumie \utm{0:4}
:   6 \IP, \bra{} på insektsmystik, \dålig{} på XXX.
    \ex{Kan bära vapen/rustning/sköld.}

Vampyr \utm{0:6}
:   10 \IP, \bra{} på förföra, \dålig{} på XXX.
    Bett\dmg{4} paralyserar\utm{2:4 \VIS} vid skada.
    Bränns av solen (\checkD{} på \ögonblick{}).
    \ex{Kan bära vapen/rustning/sköld.}

