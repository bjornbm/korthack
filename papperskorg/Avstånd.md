Två avstånd används i \striderx{}:

*   \intro{nära} avstånd är inom cirka 20 meter,
*   \intro{långt} avstånd är bortom cirka 20 meter.

\noindent
Om din rörlighet inte är begränsad så hinner du på ditt \dragx{} både förflytta dig inom \keyI{nära} avstånd och göra något annat.
\REM{Att förflytta dig \keyI{långt} tar minst ett helt \drag{}.}

\intro{Kastvapen} kan användas mot mål på \keyI{nära} avstånd, \intro{skjutvapen} på \keyI{nära} och \keyI{långt} avstånd.
Avståndsvapen kan inte användas i närkamp.

\REM{
\intro{Kastvapen} kan användas mot mål på \intro{nära} avstånd.

\intro{Skjutvapen} kan användas mot mål på \intro{nära} och \intro{långt} avstånd.
}
