\FANTASY{ Medkits istället för omslag? }

Återhämtningen av \KP{} kan påskyndas med hjälp av omvårdnad och örtmedicin.
Att vårda någon är \lättx{} och förbrukar ett \xr{_läkande omslag_}{prislista} preparerat med smärtlindrande och helbrägdagörande örter och salvor.
Vårda någon i:

*   \ögonblickx{} för att återställa \VIS{} \KP{} markerade med \checkH{}.
*   \timmar{} för att återställa \VIS{} \KP{} markerade med \checkD{}.

Att vårda dig själv är \svårt{}\REM{ (\pröva{} \HVD{})}\REM{ och varje försök förbrukar ett \term{läkande omslag}{prislista} oavsett om du \lyckas{}}.

