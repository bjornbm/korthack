\REM{# Rollpersonsgenerator

1.  Slumpa ett \xr{namn}{namn}.

2.  Slumpa två \GE{} att ha _2_ i. Övriga \GE{} har du _1_ i.

3.  Slumpa en \xr{sak}{färdigheter} att vara \dåligx{} på.

4.  Slumpa två andra saker att vara \bra{} på.
    Om du slumpar samma sak två gånger är du istället \mktbra{} på den.

5.  Slumpa din \xr{startutrustning}{startutrustning}.

6.  Hitta på en passande bakgrund och \xr{motivation}{rpintro} baserat på dina \GE{}, färdigheter och utrustning.

7.  Anställ \hantlangare{} om du vill.


# Rollpersonsgenerator (nivå 1)

1.  Välj eller slumpa ett namn\sec{namn}.

2.  Skapa en karaktär med nivå 0\sec{nivåer} och _1_ i alla \GE{}. Välj/slumpa något\sec{färdigheter} att vara \bra{} på och något att vara \dåligx{} på.

3.  Öka din nivå till nivå 1.
    Välj/slumpa två \GE{} att öka till _2_ och något (annat än det du är \dålig{} på) att bli \bra{} på. Om du redan var \bra{} på det blir du istället \mktbra{}.

4.  Välj/slumpa din startutrustning\sec{startutrustning}.

5.  Hitta på en passande bakgrund och motivation\sec{rpintro} baserat på dina \GE{}, färdigheter och utrustning.

6.  Anställ hantlangare\sec{hantlangare} om du vill.
}

# Rollpersonsgenerator

1.  Välj ett namn\sec{namn}.

2.  Välj två grundegenskaper\sec{ge} att ha 2 i; övriga har du 1 i.

\REM{3.  Välj antingen två saker\sec{färdigheter} att vara \brax{} på eller en sak att vara \mktbra{} på.}
3.  Välj två saker\sec{färdigheter} att vara \brax{} på.

\REM{4.  Välj en sak\sec{färdigheter} att vara \dålig{} på; den blir du aldrig bättre på.}
4.  Välj en sak\sec{färdigheter} att för evigt vara \dålig{} på.

5.  Välj din startutrustning\sec{startutrustning}.

6.  Hitta på en bakgrund och motivation\sec{rpintro} till dig. Vem var du innan och vad fick dig att bli en äventyrare?

7.  Anställ hantlangare\sec{hantlangare} om du vill.
