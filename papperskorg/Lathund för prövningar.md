Slå 4 eller högre med en tärning basrerad på din \GE{}:

\hfill{} 1: \D4,
\hfill{} 2: \D6,
\hfill{} 3: \D8,
\hfill{} 4: \D10,
\hfill{}≥5: \D12.
\hfill{}

\Fördel{} ger +1 på tärningsslaget, \nackdel{} ger \minus{1} (trumfar \fördel{}).

\xr{Övning ger färdighet}{övning-ger-färdighet}; slå en bättre tärning på andra försöket.

\Hjälp{}/\hindrax{} för att ge någon +1/\minus{1} på hens \dragx{}. \REM{Inte strikt relaterat till prövningar?}
