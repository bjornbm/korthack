--- ------------ ----------- ------------ ------------
  1 Thidrik      Konal       Kadlin       Jofrid
  2 Stuf         Audolf      Sigrunn      Gudfrid
  3 Hafgrim      Gærrar      Heith        Arnora
  4 Karli        Bjarki      Rannveig     Freygerd
  5 Kaupmann     Jomar       Signy        Svala
  6 Svinulf      Nærfi       Alfeid       Gyrd
  7 Thangbrand   Asfrith     Hrefna       Hallbera
  8 Hallmund     Hjarrandi   Halla        Geirhild
  9 Asmund       Asgeir      Svanlaug     Runa
 10 Hallvard     Sigguatr    Hallfrid     Yri
 11 Vigot        Sigeræd     Ljufa        Luta
 12 Sævil        Thorgest    Thorhild     Audhild
--- ------------ ----------- ------------ ------------

\cred{ https://www.fantasynamegenerators.com/viking_names.php }
