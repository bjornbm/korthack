Dina \intro{kraftpoäng} (\introabbr{kp}) representerar din vigör, viljestyrka och ork att kämpa/försvara dig. Dessa förbrukas när du blir skadad, skrämd eller fysisk eller psykiskt utmattad eller överansträngd.
Förbrukade \KP{} markeras vanligtvis baserat på hur många som förbrukas i ett svep: bokförs med hjälp av olika marker\br{}ingar:

|          |                                             |
|--------- |---------------------------------------------|
| \checkS  | återfås efter \ögonblickx{} av ostörd vila  |
| \checkH  | återfås efter \timmar{} av ostörd vila      |
| \checkD  | återfås efter \dagar{} av ostörd vila       |

Efter en vila återfår du _alla_ \introabbr{kp} vars markeringar motsvarar vilans längd.
Du kan medan du vilar inte göra annat än att sköta om dig själv och samtala.
\REM{Om du träffas av avståndsvapen eller tvingas värja dig mot närstridsattacker…}



