# Vättar

Vättar är lömska och fega kräk, skickliga på att hålla sig dolda i skuggorna och benägna att attackera ur bakhåll.
De bor i samhällen under jord varifrån de utför kallblodiga räder.
De är otaliga och stöter man på en finns det säkerligen fler i närheten.

Vättar föder upp pack av jätteråttor som de använder som vakt- och attackhundar. De håller även med vargar som de använder som riddjur eller attackdjur. Särskilt råttorna behandlas illa och är benägna att vända sig mot sina mästare om tillfälle ges (till exempel, om en råttdrivare faller omkull).

Alla vättar är \brax på lönndom och \dåligax på moralslag\sec{moral}. Framstående vättar kan vara \bra{} eller \mktbra{} på annat.


# Vättetyper

Slödder     \utm{0:2}
:   improviserat vapen\dmg{1},
    \ex{en oorganiserad "civilist"}

Spejare     \utm{0:2}
:   dolk\dmg{1}, slunga\dmg{1},
    \ex{undviker konfrontation, kan rida på en varg för att färdas snabbare i vildmarken}

Krigare     \utm{1:2}
:   kortsvärd/spjut\dmg{2}, sköld,
    \ex{kanonmat}

Bågskytt    \utm{0:2}
:   kortbåge\dmg{2},
    \ex{föredrar krypskytte ur mörka vrår}

Vargryttare \utm{1:2}
:   kortsvärd{2}/lans\dmg{2,\riskabla}, varg,
    \ex{kavalleri}

Råttdrivare \utm{1:2}
:   piska\dmg{1,\riskabla}, \D6+2 jätteråttor i koppel,
    \ex{bär plåtbyxor som skydd mot sina råttor}

Vargskötare \utm{0:2}
:   treudd\dmg{1,\riskabla}, \D3 vargar,
    \ex{bär färskt kött}

Shaman      \utm{0:2}
:   4 \IP, stav/offerkniv\dmg{1},
    \ex{vättarnas religösa ledare}

Hövding     \utm{2:4}
:   bredsvärd\dmg{3}, sköld,
    \ex{leder genom fruktan}

Hjälte      \utm{2:6}
:   2 \IP, bredsvärd\dmg{4}, sköld, ulv,
    \ex{respekterad av alla}


# Vättiska namn

--- ------------ ----------- ------------ ------------
  1 Oilug        Furbog      Xurukk       Morn
  2 Igurg        Uggug       Zurgug       Mog
  3 Sunodagh     Zulgha      Hugagug      Ugak
  4 Mudagog      Wapkagut    Urzog        Gulfim
  5 Shobob       Guthakug    Kahigig      Homraz
  6 Zugorim      Surgug      Gulm         Shagdub
  7 Lurog        Diggu       Gujarek      Ugor
  8 Pakgu        Xoroku      Vuiltag      Mazoga
  9 Vlog         Sogugh      Nag          Glob
 10 Agugh        Uram        Xorag        Agrob
 11 Rogan        Surbag      Braugh       Shazgob
 12 Trougha      Kurmbaga    Rakgu        Shadbak
--- ------------ ----------- ------------ ------------

\cred{ http://www.fantasynamegenerators.com/orc_names.php#.WvyOnC-B3MI }


# Vättisk lorem ipsum

Men o'rmonga kirdim, chunki men bilgan holda yashashni istardim, faqat hayotning muhim faktlar oldida va o'limga kelganimda, men yashaganim yo'qligini bilishim kerakligini emas, balki nimani o'rgatish kerakligini bilishim mumkinligini bilib oldim.
Men hayot emas, balki yashashni xohlamadim, hayot juda ham qadrli edi; istisno qilishni xohlamagan edim, agar zarurat bo'lmasa.
Men chuqur yashab, hayotning barcha qismini emoqchiman, umrbod jonini berishga, keng tarozini kesishga va sochlarini silkitishga, hayotni burchakka ko'chirishga va shunga o'xshash hayot kechirishga harakat qilardim.
Uni eng kam shartlarga kamaytirish va agar u o'zini anglatadigan bo'lsa, unda nega butunlay va haqiqiy ma'nodagi ma'nosini \REM{ olishni va uning ma'nosini butun dunyoga e'lon qilishni; yoki u buyuk bo'lgan bo'lsa, uni tajriba orqali bilib olish va keyingi ekskursiya - da uni haqiqiy hisobga olishim mumkin. }

\cred{ Google translate to Uzbek of Walden:

I went to the woods because I wished to live deliberately, to front only the essential facts of life, and see if I could not learn what it had to teach, and not, when I came to die, discover that I had not lived. I did not wish to live what was not life, living is so dear; nor did I wish to practice resignation, unless it was quite necessary. I wanted to live deep and suck out all the marrow of life, to live so sturdily and Spartan-like as to put to rout all that was not life, to cut a broad swath and shave close, to drive life into a corner, and reduce it to its lowest terms, and, if it proved to be mean, why then to get the whole and genuine meanness of it, and publish its meanness to the world; or if it were sublime, to know it by experience, and be able to give a true account of it in my next excursion.

}
