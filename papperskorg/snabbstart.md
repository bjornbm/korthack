# Snabbstart för nya spelare {#snabbstart}

Ta ett rollformulär och skriv din \RP{}s namn och nivå (_1_). Följ instruktionerna nedan och fyll i rollformuläret allteftersom.

1.  Läs om \grundegenskaperx{} och välj de två som verkar intressantast.
    Skriv _2_ vid dem och _1_ vid de andra.
2.  Läs om \xr{resurser}{resurser}\sec{kroppspoäng}\sec{fokuspoäng}\sec{inspirationspoäng}.
    Lägg tll rutor vid behov.
3.  Läs om \xr{tid}{tid}, \xr{handlingar}{handlingar} och \prövningarx{}.
    Välj två saker att vara \bra{} på och en att vara \dålig{} på.
4.  Läs om \xr{utmaningar}{utmaningar}, att \satsa{} och att \xr{hjälpa/hindra}{hjalpa}.
5.  Läs om \xr{avstånd}{avstånd}, \arenor{} och \vapen{}.
    Välj ett vapen.
6.  Läs om rustningar & \sköldar{}, \xr{läkekonst}{läkekonst}, \belysning{} och \xr{belastning}{belastning}.
    Välj din \xr{startutrustning}{startutrustning}.
7.  Läs om \hantlangare{}.
    Anställ hantlangare om du vill.

