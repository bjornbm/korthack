Du kan lära dig av dina motgångar.
Om du \misslyckas{} med att \prövax{} en \GE{} på ditt \dragx{} och väljer att försöka igen nästa \drag{} \REM{kan din chans att \lyckas{} öka.
På andra försöket} så får du slå den tärning som motsvarar din \GE{}+1.

Ökningen är inte kumulativ och gäller tills du \lyckas{} eller väljer att göra något annat på ditt \drag{}.

\cred{ICRPG}
