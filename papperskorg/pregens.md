# Arketyper

      \Sty   \Tål   \Pre   \Vis   \Uts  Arketyp
---- ------ ------ ------ ------ ------ ----------------
  1.    2      2      1      1      1   Krigare
  2.    2      1      2      1      1   Tjuv
  3.    2      1      1      2      1   Jägare
  4.    2      1      1      1      2   Duelant
  5.    1      2      2      1      1   Bågskytt
  6.    1      2      1      2      1   Utforskare
  7.    1      2      1      1      2   Officer
  8.    1      1      2      2      1   Helare
  9.    1      1      2      1      2   Gentlemannatjuv
 10.    1      1      1      2      2   Präst


# Pregen: Krigaren

\Sty: 2,
\TÅL: 2,
\PRE: 1,
\VIS: 1,
\UTS: 1.

\Bra{} på: slåss med svärd, blockera (med sköld).

\Dålig{} på: förhandla.

Beväpning:
bredsvärd \ex{tungt},
spjut \ex{långt},
sköld,
rustning.

Övrig utrustning:
10 meter rep,
hammare,
5 nävar spik,
5 facklor,
5 dagsransoner mat,
2 dagsransoner vatten,
\sm{20}


# Pregen: Bågskytten

\Sty: 1,
\TÅL: 2,
\PRE: 2,
\VIS: 1,
\UTS: 1.

\Bra{} på: skjuta med pilbåge, springa.

\Dålig{} på: simma.

Beväpning:
långbåge \ex{tung},
kortsvärd \ex{lätt},
rustning,
sköld.

Övrig utrustning:
såg,
visselpipa,
10 pilar,
2 bågsträngar,
5 facklor,
5 dagsransoner mat,
2 dagsransoner vatten,
\sm{13}.


# Pregen: Officeren

\Sty: 1,
\TÅL: 2,
\PRE: 1,
\VIS: 1,
\UTS: 2.

\Bra{} på: övertala, försvara.

\Dålig{} på: hoppa.

Beväpning:
långsvärd \ex{tungt},
sköld.

Övrig utrustning:
fina kläder,
spett,
visselpipa,
riktad lykta,
5 flaskor lampolja,
5 dagsransoner mat,
2 dagsransoner vatten,
\sm{25}.


# Pregen: Tjuven

\Sty: 2,
\TÅL: 1,
\PRE: 2,
\VIS: 1,
\UTS: 1.

\Bra{} på: smyga, fällor.

\Dålig{} på: hantera djur.

Beväpning:
dolk \ex{lätt},
sköld.

Övrig utrustning:
kofot,
änterhake & rep,
dyrkar,
stålspegel,
5 nävar fotanglar,
5 ljus,
5 dagsransoner mat,
2 dagsransoner vatten,
\sm{23}.


# Pregen: Jägaren

\Sty: 2,
\TÅL: 1,
\PRE: 1,
\VIS: 2,
\UTS: 1.

\Bra{} på: spåra, kasta.

\Dålig{} på: ljuga.

Beväpning:
spjut \ex{långt},
3 kastspjut,
sköld.


Övrig utrustning:
björnsax,
5 läkande omslag,
metrev,
10 pilar,
2 bågsträngar,
5 snaror,
5 facklor,
5 dagsransoner mat,
2 dagsransoner vatten,
\sm{8}.


# Pregen: Prästen

\Sty: 1,
\TÅL: 1,
\PRE: 1,
\VIS: 2,
\UTS: 2.

\Bra{} på: övertala, läkekonst.

\Dålig{} på: springa.

Beväpning:
stav \ex{lång},
sköld.

Övrig utrustning:
5 läkande omslag,
krita & griffeltavla,
tång,
lykta,
5 flaskor lampolja,
5 dagsransoner mat,
2 dagsransoner vatten,
\sm{20}.


# Pregen: Barden

\Sty: 1,
\TÅL: 1,
\PRE: 2,
\VIS: 1,
\UTS: 2.

\Bra{} på: underhålla, bluffa.

\Dålig{} på: hantverk

Beväpning:
värja \ex{lätt},
sköld.

Övrig utrustning:
luta,
stämgaffel,
sax,
penna & bläck,
5 papper,
1 ljus,
5 facklor,
5 dagsransoner mat,
2 dagsransoner vatten,
\sm{19}.


# Pregen: Utforskaren

\Sty: 1,
\TÅL: 2,
\PRE: 1,
\VIS: 2,
\UTS: 1.

\Bra{} på: navigera, gömma sig.

\Dålig{} på: försvara.

Beväpning:
kortsvärd \ex{lätt},
sköld.

Övrig utrustning:
spade,
10 meter rep,
5 läkande omslag,
riktad lykta,
5 flaskor lampolja,
5 dagsransoner mat,
2 dagsransoner vatten,
\sm{25}.


# Pregen: Barbaren

\Sty: 2,
\TÅL: 2,
\PRE: 1,
\VIS: 1,
\UTS: 1.

\Bra{} på: krossvapen, skrämma.

\Dålig{} på: uppföra sig.

Beväpning:
rustning,
stridshammare \ex{tung},
slunga,
sköld.

Övrig utrustning:
stämjärn,
metrev,
10 meter rep,
8 slungstenar,
3 snaror,
5 facklor,
5 dagsransoner mat,
2 dagsransoner vatten,
\sm{14}.


# Pregen: Helaren

\Sty: 1,
\TÅL: 2,
\PRE: 1,
\VIS: 2,
\UTS: 1.

\Bra{} på: läkekonst, försvara.

\Dålig{} på: kasta.

Beväpning:
stav \ex{lång},
sköld.

Övrig utrustning:
5 läkande omslag,
tång,
krita & griffeltavla,
lykta,
5 flaskor lampolja,
5 dagsransoner mat,
2 dagsransoner vatten,
\sm{25}.


# Pregen: Munken

# Pregen: Köpmannen

# Pregen: Lärde mannen

# Pregen: Barbaren

# Pregen: Sköldmön

# Pregen: Adelsmannen

# Pregen: Druiden

# Pregen: Riddaren

# Pregen: Sjöfararen

# Pregen: Piraten

