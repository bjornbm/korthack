Du kan välja att \intro{fördröja} en \handlingx{} för att kunna reagera på andra karaktärers \handlingar{}.\REM{
Till exempel kanske du vill skjuta mot den första fiende som kommer genom en dörr, eller spänna snubbeltråden efter att dina vänner har passerat.}
På ditt \dragx{} berättar du vad du väntar på och vad du vill göra \REM{(samt mot vem om tillämpbart)} när det inträffar.

Du kan när som helst välja att försaka din fördröjda \handling{} istället för att fullfölja den, men du kan inte i övrigt ändra dig.

Om det du väntar på inte inträffar innan det blir ditt \drag{} igen så försakas din fördröjda \handling{}.

