Du har \dubbelt{\VIS} _inspirationspoäng_ (\introIP{}) som du kan använda för att uppbåda inre kraft inför en \prövningx{}.
Källan till din inre kraft kan till exempel vara tron att gudarna står dig bi, ett självförtroendehöjande mantra eller meditation som samlar tankarna.

Markera en \introIP{} med \checkH{} för att få \fördel{} på en \prövning{}\REM{ (trumfar inte \nackdel{})}.
Markera med \checkD{} för att få \fördel{} som trumfar \nackdel{}. Beskriv varifrån du hämtar din styrka.


\cred{ Coriolis – ber till ikonerna }
\cred{ Vampire – willpower }
\cred{ Strength in the face of adversity }


\REM{

Är det för bra att bara behöva stryka en poäng? Kanske blir obalanserat vid hög nivå, men annars får den nog VIS svårt att konkurrera med PRE som utöver FP också ger skjutvapenskada.

I situationer där man gör ett tärningsslag om dagen, till exempel för att navigera under en längre resa, så blir det i princip gratis att få fördel eftersom timmar av vila är implicit i en heldag.
Enda risken är väl att man har en oväntad encounter innan dagens slut och då har en IP mindre att använda.
Man kanske inte ska få använda IP för handlingar som tar mer än \ögonblick{} motiverat med att det går inte att vara psyched så länge?
Eller i varje fall inte mer än \timmar{}?

}
