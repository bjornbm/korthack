\hyphenation{civil-isa-tion-en}

Varför har du sökt dig till hjärtat av den fallna Thraciska civilisationen?
Vad hoppas du finna i dess ruiner?

1.  Du vill bärga dess bortglömda rikedomar.
2.  Du fascineras av underjordisk arkitektur.
3.  Stamfolket har rövat bort någon du håller kär.
4.  Något mörkt i ditt blod kallar dig hit.
5.  Du är hitsänd för att undersöka rykten om människooffer.
6.  Du söker ett mytomspunnet skinande palats under jorden.
7.  Du flydde från djurmännen. Nu vill du hämnas.
8.  En god vän är försvunnen. Spåren leder hit.
9.  Du är på pilgrimsfärd och söker Athenas tempel.
10. Någonting är på väg att hända här. Det oroar dig.
