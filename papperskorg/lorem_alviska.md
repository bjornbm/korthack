# Alvisk lorem ipsum

An taima palis tehto hap. Úil telco nalanta or, oia oaris cotumo elendë ëa, lá vírë tulca timpinen tul. Ar nur onótima taniquelassë. Yá axo ataquë mirilya tanwëataquë, ep nún tasar racinë, varta tasar é mat. Up lívë inqua nal, tyávë amanyar goneheca lis lá.
Rambë artaquetta né erë. Nu cemna foina caw, lápa nainië mittanya iel cé. As pio yára liquis. Má yulmë rambë ríc.
Sín tó talta hwarma, laira nostalë mel us. Viltë tasar lindë her sú, ainu tussa amilessë aha ep, sir et yúyo hecil nahamna. Nú centa pereldar lav. Et halda hesta nót. Cemna cotumo tol er, engë alatúvëaúra tuo ar, men rauko melissë mí.
Caila tasar fum et. Sa quí anna ettelen, ya avá raica queni, et quí latin nénar orpano. Inqua indómë nar nú, lá fëa yalúmëa tanwëataquë. Yarra lingwë yerna.

\REM{
yernacolla ara oi.
Nor cé caila linwelë silninquita, oi roina tengwo yén. Soron nirya hahta ta axa, lár orva mittanya caimasan sá, nulda nimba ana et. Arca sanga tanwëataquë sí rip.
}

\cred{ http://thealexandrian.net/wordpress/38698/roleplaying-games/untested-fantasy-lorem-ipsum }
