Om \SL{} bedömer att det du vill göra är omöjligt så misslyckas du\REM{ automatiskt}.
Om din \RP{} borde förstå att det är omöjligt så berättar \SL{} det och du kan välja att göra någonting annat istället.

Om \SL{} bedömer att det du vill göra är lätt---eller om du kan försöka om och om igen utan konsekvenser---så lyckas du\REM{ automatiskt}.

\REM{Annars måste du \slåförx{} en \GE{} för att se hur det går.}
Annars måste du \xr{slå en tärning}{grundegenskapsslag} för att se hur det går.

Du kan välja att vänta med din \intro{handling} för att kunna reagera på andras \intro{handlingar}.
Berätta i så fall vad du väntar på och vad du vill göra om och när det du väntar på inträffar.
\REM{Du kan senare välja att försaka handlingen istället för att fullfölja den, men kan inte i övrigt ändra dig.}
