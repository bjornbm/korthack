Din \intro{arketyp} bestämmer din starkaste \GE{}; övriga \GE{} har du 1 i.

1.  \intro{Krigaren} med \KRP{} 2, \tungt{} \vapenx{} och \tung{} \rustningx{}.\
    \ex{Varför slåss du?
        Vilket blodbad hemsöker dina drömmar?}

2.  \intro{Magikern} med \HVD{} 2 och \REM{en \besvarjelsex{}} två \besvarjelserx{}.\
    \ex{Vem var din läromästare?
        Vad skulle du aldrig använda magi till?}

3.  \intro{Ledaren} med \UTS{} 2 och två \foljeslagarex{}\REM{ med 1 i alla \GE{}}. \
    \ex{Varför lyssnar folk på dig?
        Vilka är dina \foljeslagare{}?}

Magikern kan be \SL{} om förslag på \besvarjelser{} eller föreslå egna.
Ledarens \foljeslagare{} har 1 i alla \GE{} och varsitt \vapen{} (ej \tungt{}).
