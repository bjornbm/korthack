Du har \dubbelt{\PRE} _fokuspoäng_ (\introFP{}) som du kan använda för att få _flyt_.

När du \lyckasx{} med en \handlingx{} så kan du välja att markera en \introFP{} med \checkH{} för att omedelbart få upprepa \handlingen{}\REM{ (en ny \prövning{} krävs)}.
Din \metod{} måste vara densamma men ditt \mål{} måste inte vara det; till exempel så kan du med flyt hugga dig igenom flera fiender.

Du kan fortsätta att upprepa \handlingen{} så länge du \lyckas{} \REM{med en \prövning{}} och har \introFP{} kvar att förbruka.

\REM{Förtydliga att det bara gäller prövningar som kommer av din handling, inte passiva prövningar eller blockering?}


\cred{Löst inspirerat från ICRPG "Archer class fires again on 13 MODIFIED or higher"}

