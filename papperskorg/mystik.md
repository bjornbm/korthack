# Mystiska krafter {#krafter}

Mystiska krafter är grupperade i mystikskolor\sec{mystikskolor}.
En mystisk artefakt av för en skola lämplig natur ger +1 \verkanx{} vid aktivering av skolans krafter.

Att aktivera en mystisk kraft är vanligtvis en utmaning\sec{utmaningar} med \arbete{} 1,
men många krafter kan förstärkas (skrivs "grundeffekt\boost{\förstärkning}") på bekostnad av ett \motstånd{} lika med antalet förstärkningar.
Några vanligt förekommande förstärkningskedjor förkortas\sec{förstärkningskedjor}.


# Förstärkningskedjor {#förstärkningskedjor}

Förstärkningskedja                              Förkortning
----------------------------------------------- ------------------
\boosta{berör}{ser\ra{tänker på}}               \berör{}
\boosta{någon}{\VIS{} stycken\ra{alla du vill}} \någon{}
\boosta{en}{\VIS{} stycken\ra{alla du vill}}    en\boost{}
\boosta{ett}{\VIS{} stycken\ra{alla du vill}}   ett\boost{}
\boosta{\sekunder}{\timmar\ra{\dagar}}          \sekunderboost{}
\boosta{\nära}{\långt}                          \näraboost{}
\boosta{1}{2\ra{3}…}                            \ettboost{}


\REM{

# Mystikskolor

Magisk: Klassisk magi genom uttalade besvärjelser. Förberedelse: memorera besvärjelse. En trollstavr kan ge +1 \verkan.

Helig: Gudomliga ingripanden genom bön eller meditation. Förberedelse: förbön. En helig symbol eller relik kan ge +1 \verkan.

Alkemisk: Kombination av reaktiva ingredienser i syfte att lagra mystisk kraft för senare användning. Förberedelse: preparera ingredienser. Ett laboratorium kan ge +1 \verkan.

Musikalisk: Förberedelse: övning. Ett virtuost musikinstrument kan ge +1 \verkan.

Druidisk: Djur och växter. Förberedelse: samtal med naturandar. En totem kan ge +1 verkan.
}


# Mystikskolor {#mystikskolor}

Skola                              |  Förberedelse           | Artefakt
-----------------------------------|-------------------------|---------------
Alkemisk\sec{alkemisk}             | preparera ingredienser  | laboratorium
Druidisk\sec{druidisk}             | samtal med naturandar   | totem
Elementmystik\sec{element}         | offer                   | ädelsten
Helig barmhärtig\sec{barmhärtig}   | förbön                  | helig relik
Helig rättfärdig\sec{rättfärdig}   | ablutation              | helig symbol
Illusionistisk\sec{illusionistisk} | visualisering           | sidenskynke
Mental\sec{mental}                 | meditation              | kristallkula
Metamystik\sec{meta}               | studier                 | pergament
Musikalisk\sec{musikalisk}         | repetition              | instrument
Nekromantisk\sec{nekromantisk}     | offer av liv            | kranium
Praktisk\sec{praktisk}             | memorera besvärjelser   | trollstav


# Alkemisk mystik {#alkemisk}

Granat:

:   Skapa \boosta{en granat}{\VIS{} granater} som exploderar med \verkan{} \ettboost{} när den kastas. \Verkan{} avtar med 1 per två meter från landningsplatsen.

Syra:

:   Skapa \boosta{en flaska}{\VIS{} flaskor} med syra som fräter bort materia med \verkan{} \ettboost{}.

Bloss:

:   Skapa \boosta{ett bloss}{\VIS{} bloss} som skiner som \boosta{en fackla}{dagsljus\ra{solen}} i \sekunderboost{} när det antänds.

Blixtladdning:

:   Ladda ett vapen med blixtar som ger \verkan{} \ettboost{} vid \boosta{den första träffen}{de \VIS{} första träffarna}.


# Druidisk mystik {#druidisk}

Lockrop

:   \boosta{Den}{\VIS{} stycken\ra{alla}} närmaste av en bestämd djurart rör sig mot dig så fort den förmår och bistår dig i \sekunderboost{}.

Naturens språk:

:   Du kan under \sekunderboost{} tala med \boosta{ett}{\VIS{}\ra{alla}} \boosta{djur}{insekt\ra{växt}}.

Djurform:

:   \boosta{En av}{\VIS{} av\ra{alla}} dina sinnen och kroppsdelar antar formen av ett \boosta{däggdjur}{djur} så länge du önskar.

Växtkraft:

:   \boosta{Ett}{\VIS{} stycken} frö eller växt växer motsvarande decennier på bara \boosta{\dagar}{\timmar\ra{\sekunder}}, ett år på bara \boosta{\timmar}{\sekunder}, eller veckor på bara \sekunder, i en form du önskar.


# Elementmystik {#element}

Element:

:   En \boosta{handfull}{anseenlig mängd\ra{massa}} av ett av de fyra elementen skapas på en plats du \berör{} och behåller önskad form i \sekunderboost{}.

Elementarande:

:   En elementarande med \ettboost{} i alla \GE{} frigör sig från sitt element och lyder dina order i \sekunderboost{} innan den försvinner.

Elementprojektil:

:   Du slungar en projektil bestående av ett av de fyra elementen mot \någon{} på \näraboost{} håll. Projektilen har verkan \ettboost{}.

Elementväg:

:   \boosta{Du}{och dina tillhörigheter} kan i \sekunderboost{} \boosta{färdas}{oskadda} genom ett av de fyra elementen.



# Helig barmhärtig mystik {#barmhärtig}

Hela:

:   \Någon{} du \boosta{berör}{ser} återfår alla \KP{} markerade med \boosta{\checkS}{\checkH\ra\checkD} eller lindrigare.
\REM{En odöd som utsätts för _hela_ tar istället en skada av grad motsvarande _manifestationen_.}

Kurera:

:   \Någon{} du berör kureras från en \boosta{mundan}{mystisk} sjukdom, åkomma, förgiftning, förtrollning, eller förbannelse.

Skydda:

:   \Någon{} du \berör{} får i \sekunderboost{} \motståndx{} \ettboost{} mot \verkan{} från attacker.

Välsigna:

:   \Någon{} du \berör{} får \fördelx{} på sin nästa prövning\boost{resten av scenen}. Denna \fördel{} trumfar mundana \nackdelar.


# Helig rättfärdig mystik {#rättfärdig}

Inspirera:

:   \Någon{} du \berör{} får i \sekunderboost{} sin \verkanx{} ökad med \ettboost{}.
\REM{ Redundant med kampsång? }

Förbanna:

:   \Någon{} du \berör{} får \nackdelx{} i \sekunderboost{}.

Helga:

:   \Någon{} mundan utrustning du \berör{} blir mystisk i \sekunderboost{}.
\REM{ Ett mystiskt vapen ger till exempel mystisk \verkanx, en mystisk rustning eller sköld skyddar mot mystiska attacker, och mystisk belysning slocknar inte. }

Vänd odöda:

:   Alla odöda du ser drabbas av en mystisk attack med \verkan{} \ettboost{} och kan i \sekunder{} inte närma sig dig.


# Illusionistisk mystik {#illusionistisk}

Osynlighet:

:   \Någon{} du \berör{} blir osynlig i \sekunderboost{}, eller till du väljer att hen ska bli synlig igen.

Illusion:

:   \Någon{} du ser upplever en illusion som påverkar \ettboost{} av hens sinnen i \sekunderboost{}.

Sannsyn:

:   Du får i \sekunderboost{} förmågan att genomskåda mundana\boost{och mystiska} illusioner, kamofleringar, och formförändringar.

Tystnad:

:   Det blir knäpptyst i ett område du \berör{} stort som en skrubb\boost{rum\ra{sal}}. Tystnaden varar i \sekunderboost{}.


# Mental mystisk {#mental}

Telekinesi:

:   Du kan i \sekunderboost{} manipulera ett föremål på \näraboost{} håll med \boosta{grov(0)}{viss(1)\ra{stor(2)…}} precision och \boosta{svag(0)}{viss(1)\ra{stor(2)…}} styrka.

Teleportera:

:   \boosta{Du}{och \VIS{} andra du berör\ra{alla} du berör} teleporteras till en valfri plats du kan se eller har besökt inom \boosta{\timmar}{\dagar\ra{någonsin}}.
\REM{Alternativ variant där man markerar destination med en _hemruna_?}

Telepati: (magisk)

:   Du kan under \sekunderboost{} kommunicera telepatiskt med \någon{} du \berör{}.

Tankeläsning:

:   Du kan i \sekunderboost{} \boosta{genomskåda lögner}{läsa ytliga tankar\ra{förstå motiven}} från \någon{} du \berör{}.


# Metamystik {#meta}

Antimystik:

:   Effekten från en mystisk kraft, eller all effekt av mystik mot \någon{}, negeras under \sekunderboost{}.

Läs mystik:

:   Du kan avgöra om något du \berör{} \boosta{är mystiskt}{samt dess krafter\ra{samt hur de aktiveras}}.
\REM{:   du kan avgöra om något du \berör{} är mystiskt\boost{dess mystikskola/-skolor\ra{dess krafter}\ra{hur de aktiveras}}.}

Skrift:

:   Nedteckna en skrift med en av dina mystiska krafter.
Vid uppläsning upplöses skriften och kraften aktiveras. \Ip{} för kraftens \verkan{} förbrukas vid nedtecknandet.

Dryck:

:   Tillred \boosta{en dryck}{\VIS{} drycker} med en av dina mystiska krafter som normalt påverkar någon du berör.
Drycken påverkar istället den som förtär den.
\Ip{} för kraftens \verkan{} förbrukas vid tillredningen.


# Musikalisk mystik {#musikalisk}

Vaggvisa:

:   \boosta{Någon}{\UTS{} personer\ra{alla}} som hör dig somnar och sover i \sekunderboost{} efter att du slutat sjunga.

Valkyrierytt:

:   \boosta{Någon}{\UTS{} personer\ra{alla}} som hör dig måste \boosta{slå för moral}{med \nackdel}.

Virtuos:

:   \boosta{Någon}{\UTS{} personer\ra{alla}} som hör dig imponeras och söker din gunst i \sekunderboost{} efter att du slutat sjunga.

Kampsång:

:   \boosta{Någon}{\UTS{} vänner} får sin \verkanx{} ökad med \ettboost{} så länge de kan höra dig sjunga.


# Nekromantisk mystik {#nekromantisk}

Animera död:

:   Du tar kontroll över \någon{} död kropp du \berör{}.
Den har \enboost{} \KP{} och faller livlös ihop igen efter \sekunderboost{}.

Tala med död:

:   Du kan under \sekunderboost{} tala med \någon{} som varit död i \boosta{\timmar}{\dagar\ra{månader}\ra{åratal}}.

Kontrollera lägre odöd:

:   \Någon{} lägre odöd lyder dina order i \sekunderboost{}.

Paralysera:

:   \Någon{} du \berör{} paralyseras. Hen kan varken röra sig eller tala förrän hen utfört \arbetex{} \boosta{1}{\VIS{}} med \VIS{} (en \prövningx{} som tar \sekunder{}).
Attacker mot paralyserade har \fördelx{}.


# Praktisk mystik {#praktisk}

Öppna:

:   \boosta{En}{låst\ra{mystiskt förseglad}} dörr eller liknande du \berör{} öppnas.

Stäng:

:   En dörr eller liknande du \berör{} \boosta{stängs}{och låses\ra{förseglas}}.

Ljus:

:   Ett föremål du \berör{} lyser som \boosta{ett vaxljus}{en fackla\ra{dagsljus}\ra{solen}} i \sekunderboost{}.

Mörker:

:   Ett hörn\boost{rum/korridor\ra{sal}\ra{komplex}} fylls av ett kompakt mörker som inte kan genomträngas av ljus.
Mörkret varar i \sekunderboost{}.

\REM{
Rena:

:   \boosta{En dagsranson}{\VIS{} dagsransoner} mat eller dryck du berör renas från alla \boosta{mundana}{och mystiska} gifter, sjukdomar, förtrollningar, och andra föroreningar.
\REM{ Poänglös med kurera? }
}




\REM{

# Redundant mystik 

Attack: (magisk/helig)

:   \Någon{} du \berör{} drabbas av en mystisk attack med \verkan{} \ettboost{}. ERSÄTTS AV ELEMENTPROJEKTIL.

Mörkerseende: (magisk)

:   \Någon{} du \berör{} får perfekt mörkerseende i \sekunderboost{}. ERSÄTTS AV DJURFORM FÖR KASTAREN

Andas: (magisk)

:   \Någon{} du \berör{} behöver inte andas i \boosta{\timmar}{\dagar}. ERSÄTTS AV DJURFORM FÖR KASTAREN

Flyga: (magisk)

:   \Någon{} du \berör{} kan flyga med \boosta{sin}{din} viljas kraft i \sekunderboost{}. ERSÄTTS AV DJURFORM FÖR KASTAREN

}
