\REM{

Vättar är människor, förvridna och förkrympta av generationer i underjordiskt mörker och onda tankar. Fega, själviska, grymma. Blodtörstiga och kannibalistiska. Grå hud, ljuskänsliga ögon, ljusskygga, mörkerseende, krigsmålningar,

Illvättar är större, starkare, och mer disciplinerade än vättar. Fortfarande själviska men mer ärelystna och inte lika fega.

}


|    |                 |    |                |    |              |
|---:|-----------------|---:|----------------|---:|--------------|
| A: | jägare          | K: | ödlefolk       | A: | jägare          
| B: | vättar          | I: | skogshuggare   | B: | vättar          
| C: | rovdjur         | J: | rymlingar      | C: | rovdjur         
| D: | banditer        | L: | trollpacka     | D: | banditer        
| E: | soldater        | M: | megafauna      | E: | soldater        
| F: | pilgrimer       | N: | monster        | F: | pilgrimer       
| G: | handelsmän      | O: | ämbetsmän      | G: | handelsmän      
| H: | bönder          | P: | kultister      | H: | bönder          
| I: | tiggare         | Q: | odöda          | I: | tiggare         
| J: | prisjägare      | R: | djupingar      | J: | prisjägare      


# Faktioner

    A             B             C            D
--- ------------- ------------- ------------ ------------
  1 handelsmän    banditer      kultister    fyndighet
  2 tiggare       kultister     Sigrunn      bosättning
  3 jägare        äventyrare    Heith        helgedom
  4 skogshuggare  prisjägare    Rannveig     grav
  5 bönder        eremit        Signy        ruin
  6 resenärer     adelsman      Alfeid       utpost
  7 bybor         budbärare     Hrefna       vrak
  8 soldater      boskap        Halla        flora
  9 pilgrimer     vaktstyrka    Svanlaug     fästning
 10 ämbetsmän     fångtranport  Hallfrid     jordbruk
--- ------------- ------------- ------------ ------------


# Faktioner

    A              
--- ---------------
  1 handelsresande
  2 tiggare      
  3 vaktstyrka
  4 resenärer
  5 bönder       
  6 pilgrimer
  7 bybor        
  8 adelsfölje
  9 fångtransport
 10 soldater
--- ---------------

    B            
--- -------------
  1 jägare
  2 skogshuggare
  3 rymlingar
  4 eremit
  5 äventyrare
  6 prisjägare
  7 nomader
  8 missionärer
  9 adelsfölje
 10 samlare
--- -------------

    C            
--- -------------
  1 banditer
  2 kultister
  3 vildmän
  4 trollpacka
  5 vättar
  6 ödlefolk
  7 odöda
  8 monster
  9 megafauna
 10 vilddjur
--- -------------

# Faktioner

    A               B             C             D
--- --------------- ------------- ------------- ------------
  1 handelsresande  jägare        banditer      fyndighet
  2 tiggare         skogshuggare  kultister     bosättning
  3 vaktstyrka      rymlingar     vildmän       helgedom
  4 resenärer       eremit        trollpacka    grav
  5 bönder          äventyrare    vättar        ruin
  6 pilgrimer       prisjägare    ödlefolk      utpost
  7 bybor           nomader       odöda         vrak
  8 adelsfölje      missionärer   monster       flora
  9 fångtransport   adelsfölje    megafauna     fästning
 10 soldater        samlare       vilddjur      jordbruk
--- --------------- ------------- ------------- ------------


# Faktioner

    A Folk
--- ---------------
  1 jägare
  2 timmermän
  3 nomader
  4 bönder
  5 soldater
  6 resenärer
  7 religösa
  8 handelsmän
  9 vakter
 10 tiggare
--- ---------------

    B Slödder
--- ---------------
  1 rymlingar
  2 äventyrare
  3 pirater
  4 prisjägare
  5 vildmän
  6 banditer
  7 kultister
  8 druider
  9 enstöring
 10 trollpacka
--- ---------------

    C Monster
--- ---------------
  1 vilddjur
  2 rövare
  3 kultister
  4 vildmän
  5 vättar
  6 illvättar
  7 ödlefolk
  8 djupingar
  9 odöda
 10 monster
--- ---------------

    D Platser
--- ---------------
  1 fyndighet
  2 bosättning
  3 helgedom
  4 grav
  5 ruin
  6 utpost
  7 vrak
  8 flora
  9 fästning
 10 jordbruk
--- ---------------


# Faktioner

A               B             C             D
--------------- ------------- ------------- ------------
handelsresande  jägare        banditer      fyndighet
tiggare         skogshuggare  kultister     bosättning
vaktstyrka      rymlingar     vildmän       helgedom
resenärer       eremit        trollpacka    grav
--------------- ------------- ------------- ------------

A               B             C             D
--------------- ------------- ------------- ------------
bönder          äventyrare    vättar        ruin
pilgrimer       prisjägare    ödlefolk      utpost
bybor           nomader       odöda         vrak
adelsfölje      missionärer   monster       flora
--------------- ------------- ------------- ------------

    A Folk
--- ---------------
  1 jägare
  2 timmermän
  3 nomader
  4 bönder

    B Resande
--- ---------------
  5 soldater
  6 resenärer
  7 religösa
  8 handelsmän
--- ---------------

    C Slödder
--- ---------------
  1 rymlingar
  2 äventyrare
  4 prisjägare
  3 slavar
--- ---------------

    D Fiender
--- ---------------
  1 vilddjur
  2 rövare
  3 kultister
  4 vildmän
--- ---------------

    E Vättar
--- ---------------
  1 vättar
  2 illvättar
  4 ödlefolk
  3 djupingar
--- ---------------

    F Monster
--- ---------------
  1 monster
  2 troll
  3 odöda
  4 
--- ---------------


vilddjur
vättar
illvättar
monster



\REM{


\REM{

# Faktioner { #faktioner }

1.  Vättar
2.  Odöda
4.  Rövare
3.  Äventyrare
5.  En trollkarl (med underhuggare)
6.  En stor best (mundan eller magisk)
7.  Orker (vargpack, ulvryttare, elitstyrkor/chocktrupper)
8.  Troll
9.  En demon
10. Kultister
11. Djupingar
12. Dvärgar (grenadjärer, musköter, chocktrupper, ingenjörer)
13. Kobolder
14. Lokal fauna
15. En minotaur
16. Kentaurer
17. Varulvar
18. Ödlemän ("vanlig" med spjut/harpun, shaman, hövding)
19. En drake
20. Vildar
xx. Råttmänn ("vanlig" med kniv och slunga, shaman, hövding)


# Faktioner { #faktioner }

1.  Banditer/pirater
2.  Kultister
3.  Trollkarl
4.  Vättar
5.  Orker
6.  Ödlemän/kobolder/djupingar
7.  Råttmän/varulvar/kentaurer
9.  Fauna (samhälle eller individ)
10. Mytologiskt monster
11. Odöda
12. Kaos/demoner/mythos

}
