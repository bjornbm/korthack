# Hjältepoäng

Om du gör något riktigt beundransvärt, till exempel en stor uppoffring eller riskfylld handling, så kan \SL{} tilldela dig en _hjälte\br{}poäng_. Du kan spendera en hjältepoäng för att få:

1.  slå om ett av dina tärningsslag, eller
2.  dubbla din \verkanx{} från en handling.

Du kan när som helst ge bort hjältepoäng till en annan \RP{} om du vill.

