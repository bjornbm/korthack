# \Slp-generator { #slp }

Slå \D20 för vad \SLP{} är \brax på, och \D10 för vad hen är \dålig{} på. Hitta på passande yrke, motivation och \xr{namn}{namn}.

--- ------------- --- ---------------------------------------
  1 Ljuga          11 Jaga \ex{spåra, fällor}
  2 Lönndom        12 Jordbruk \ex{väder, jordarter, växter}
  3 Springa        13 Hantverk \ex{smide, sömnad, slöjd}
  4 Slåss          14 Fingerfärdighet \ex{stjäla, dyrka}
  5 Simma          15 Veta \ex{sägner, vetenskap}
  6 Skvallra       16 Tigga
  7 Hantera djur   17 Sjöfart
  8 Överlevnad     18 Klättra
  9 Förhandla      19 Läkekonst \ex{omslag, kirurgi}
 10 Sjunga         20 Mystik \ex{böner, brygder, magi, sia}
--- ------------- --- ---------------------------------------

