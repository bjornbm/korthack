\REM{ 

  Dessa är problematiska eftersom deras användning
  är en disassociated mechanic[1]. Det vill säga att
  karaktären inte kan förstå eller ens vara medveten
  om mechaniken, och att spelar därmed per definition
  inte rollspelar när hen använder den.

  Karaktären kan inte ha välja mellan att "veta något
  nu" och "veta något senare".

  Metapoäng var kanske ett lämpligt namn, men frågan
  är om man vill ha den här typen av "meta" alls i
  spel.

  [1]: https://thealexandrian.net/wordpress/17231/roleplaying-games/dissociated-mechanics-a-brief-primer

}

# Vetskapspoäng (\VP{}) { #vetskapspoäng }

Du har \dubbelt{\VIS} _vetskapspoäng_ (\VP{}) som representerar kunskaper din \RP{} inhämtat genom studier, erfarenhet, eller observation och som du kan nyttja när du står inför en \prövningx{}.

Markera en \VP{} med \checkH{} för att känna till något som ger dig \fördelx{} på \prövningen{} (trumfar inte \nackdel{}).

Markera en \VP{} med \checkD{} för att känna till något som gör \prövningen{} till en \barnlekx{} för dig.

Berätta vad det är du känner till och hur du fått reda på det.

\REM{

Är det för bra att bara behöva stryka en MP? Kanske blir obalanserat vid hög nivå, men annars får den nog VIS svårt att konkurrera med PRE som utöver FP också ger skjutvapenskada.

Ett alternativ:

# Vetskapspoäng { #vetskapspoäng }

Du har \dubbelt{\VIS} _vetskapspoäng_ (\VP) som representerar information.

Förbruka 1 \VP{} (\checkS) för att känna till något av ringa betydelse.

Förbruka 2 \VP{} (\checkHH) för att känna till något som ger dig \fördelx{} på en \prövningx{}.

Förbruka 3 \VP{} (\checkDDD) för att känna till något som gör en \prövning{} till en \barnlekx{}.

Berätta vad det är du känner till och hur du fått reda på det---kanske är det något du läste i en bok för åratel sedan, eller kanske la du precis märke till det.

}
