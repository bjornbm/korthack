\Prövax{\KRP} för att ta fast någon eller för att ta dig loss ur någons grepp.
Du måste ha minst lika mycket i \KRP{} som din motståndare.

När du försöker ta fast någon så får hen först utföra en gratis attack mot dig, även om hen redan är inblandad i närstrid med någon annan.

Någon som är fasthållen kan inte göra något som innefattar rörelse och alla \närstridsattackerx{} mot henom har \fördelx{}.

Du kan kväva någon du håller fast med \KRP{} skada.
