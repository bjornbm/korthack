\REM{Jag tror att hjälpa/hindra kan hanteras tillräckligt väl av de vanliga reglerna för fördel/nackdel. Man får förklara hur man på sitt drag gör någon som ger fördel/nackdel till någon annan, och så får hen det på sitt drag.}


Du kan välja att försaka ditt eget \dragx{} för att istället \intro{hjälpa} någon annan.
Den du hjälper får på sitt kommande \drag{} +1 när hen \slårförx{} eller \slår{en \GE}.
Förklara hur du hjälper henom!

På samma sätt så kan du \intro{hindra} någon. Hen får då \minus{1} när hen \slårför{} en \GE{}.

