Med en \intro{sköld} kan du blockera en \lyckad{} \attack{} \ex{du ser komma} per \runda{},
eller två \attacker{} på bekostnad av att skölden förstörs.
