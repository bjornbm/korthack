\REM{
Ditt \intro{försvarsvärde} (\in troFV{}) är \KRP{} men ökas av viss utrustning:

|                |    |
|----------------|----|----
| Närstridsvapen | +1 |
| Sköld          | +1 | (\tung{} +2)
| Rustning       | +1 | (\tung{} +2, \mkttung{} +3)

Om en attack övervinner ditt \introFV{} så blir du \intro{sårad}.
Om du redan var \intro{sårad} eller om attacken övervinner \dubbelt{ditt \introFV} så måste du \provax{\KRP}; misslyckas du så dör du, annars så blir du bara medvetslös.
En hjälm ger dig \fordel{} på slaget.

Du kan inte simma, smyga eller sova iklädd \tung{} rustning.


# Försvarsvärde 2
}


# Grundegenskapsslag

För att avgöra om du lyckas med något kan du behöva \intro{slå för} en \GE{}.
Då slår du en tärning och lägger till din mest relevanta \GE{}.
Om resultatet blir ≥6 så \intro{lyckas} du\REM{ och om resultatet blir ≥8 så lyckas du \intro{exceptionellt bra}},
annars \intro{misslyckas} du.

\REM{
För att avgöra huruvida du lyckas med något kan du behöva \intro{slå för} en \GE{}.
Då slår du en tärning och lägger till din för situationen mest relevanta \GE{}.
Om resultatet blir ≥6 så \intro{lyckas} du.
}

\REM{
* Om resultatet blir ≤5 så \intro{misslyckas} du.
* Om resultatet blir ≥6 så \intro{lyckas} du.
* Om resultatet blir ≥8 så \intro{lyckas} du \intro{exceptionellt bra}.
}

\REM{Du lyckas dock alltid om du slår en sexa, och misslyckas alltid om du slår en etta.}

Om omständigheter ger dig betydande \intro{fördel} så får du +2 på resultatet.
Betydande \intro{nackdel} ger dig \minus{2}.
Dessa kumuleras inte och \intro{nackdel} trumfar \intro{fördel}.

\REM{
Genom att använda ditt \dragx{} till att hjälpa eller motarbeta någon kan du ge henom \intro{fördel} respektive \intro{nackdel} på hens \drag{}.

Genom att på ditt \dragx{} hjälpa eller motarbeta någon \ex{förklara hur du gör!} kan du ge henom \intro{fördel} eller \intro{nackdel} på hens \drag{}.
}


\REM{
# Attacker

När du försöker \intro{attackera} någon så slår du en tärning,
lägger till ditt \intro{attackvärde} (\introAV{}),
drar av fiendens \intro{försvarsvärde} (\introFV{})
och tolkar resultatet som när du \provarx{} en \GE{}.

Ditt \introAV{} är lika med \KRP{} plus ditt \vapensx{} \bonus{}.
Ditt \introFV{} är lika med din \KRP{} plus modifikationer från din utrustning:

|                    |    |
|--------------------|----|
| Sköld              | +1 |
| Rustning           | +1 |
| \Tungx{} rustning  | +2 |



# Attacker

Genom att \intro{attackera} en fiende kan du antingen skada henom eller med hot om våld försöka tvinga henom till något.

\Slaforx{\KRP}, men lägg till ditt \vapensx{} \bonus{} och dra ifrån din fiendes \KRP{} från \resultatet{}.
Om hen bär sköld eller rustning ger de ytterligare \minus{1} vardera. \Tungx{} rustning ger \minus{2}.

Om du \lyckas{} måste fienden välja mellan att underkasta sig din vilja eller bli \sarad{} (\svartsarad{} om du \lyckasexceptionelltbra{}).


# Attacker

Genom att \intro{attackera} en fiende kan du antingen skada henom eller med hot om våld försöka tvinga henom till något.

\Slaforx{\KRP} men lägg till ditt \vapensx{} \bonus{} på \resultatet{}, dra ifrån din fiendes \KRP{} och gör avdrag för hens \termxx{utrustning}:

|                    |    |
|--------------------|----|
| Sköld              | \minus{1} |
| Rustning           | \minus{1} |
| \Tungx{} rustning  | \minus{2}  |


Om du \lyckas{} måste fienden välja mellan att underkasta sig din vilja eller bli \sarad{} (\svartsarad{} om du \lyckasexceptionelltbra{}).

# Attacker B

Genom att \intro{attackera} en fiende kan du antingen skada henom eller med hot om våld försöka tvinga henom till något.

\Slaforx{\KRP} men dra av fiendens \KRP{} från \resultatet{}. Lägg till ditt \vapensx{} \bonus{} och gör avdrag för fiendens \termxx{utrustning}:

|                    |    |
|--------------------|----|
| Sköld              | \minus{1} |
| Rustning           | \minus{1} |
| \Tungx{} rustning  | \minus{2}  |


Om du \lyckas{} måste fienden välja mellan att underkasta sig din vilja eller bli \saradx{} (\svartsarad{} om du \lyckasexceptionelltbra{}).



# Attacker

Genom att \intro{attackera} en fiende kan du antingen skada henom eller med hot om våld försöka tvinga henom till något.

1.  \Slaforx{\KRP}
2.  lägg till ditt \vapensx{} \bonus{} på \resultatet{}
3.  dra ifrån din fiendes \KRP{}
4.  gör avdrag för hens utrustning:
    (sköld ger \minus{1},
    rustning ger \minus{1},
    \tungx{} rustning ger \minus{2})

Om du \lyckas{} måste fienden välja mellan att underkasta sig din vilja eller bli _sårad_ (_svårt sårad_ om du \lyckasexceptionelltbra{}).
}


# Strid

\REM{
Du kan \intro{attackera} någon för att skada henom eller med våld försöka tvinga henom till något.
Det går till som att \slaforx{\KRP} men resultatet jämförs med din fiendes \intro{försvarsvärde} (\introFV).

Om resultatet >\introFV{} så \lyckas{} attacken och din fiende måste välja mellan att underkasta sig din vilja eller bli \sarad{}.
Du får dessutom genast utföra en till attack mot henom eller en någon annan intill dig.

Du kan \intro{attackera} någon för att skada eller med våld tvinga henom till något.
Då \slarduforx{\KRP} men \lyckas{} om resultatet är högre än din fiendes \intro{försvarsvärde} (\introFV{}).

Du kan \intro{attackera} någon för att skada eller med våld tvinga henom till något.
Det går till som att \slaforx{\KRP} men du \lyckas{} om resultatet är högre än din fiendes \intro{försvarsvärde} (\introFV{}).

Du kan \intro{attackera} någon för att skada eller med våld tvinga henom till något.
\Slaforx{\KRP} men jämför resultatet med din fiendes \intro{försvarsvärde} (\introFV);
om resultatet >\introFV{} så \lyckas{} attacken.

Om attacken \lyckas{} måste din fiende välja mellan att underkasta sig din vilja eller bli \saradx{}.
Du får dessutom genast utföra en till attack mot henom eller någon annan intill er.
}

Du kan \intro{attackera} någon för att \sarax{} eller med våld försöka tvinga henom till något.
\Slaforx{\KRP} men jämför resultatet med din fiendes \intro{försvarsvärde} (\introFV).

Om resultatet >\introFV{} så \lyckas{} din attack och fienden måste välja mellan att underkasta sig din vilja eller bli \sarad{}.
Du får dessutom genast utföra en till attack \ex{mot henom eller någon annan}.

\REM{
\Slaforx{\KRP} men justera resultatet med ditt \vapens{} \bonus{} och jämför det med din fiendes \intro{försvarsvärde} (\introFV).

Om resultatet >\introFV{} så \lyckas{} attacken och din fiende måste välja mellan att underkasta sig din vilja eller bli \sarad{}.
Du får dessutom genast utföra en till attack mot henom eller en annan fiende intill dig.
}



\REM{
Om resultatet >\introFV{} måste hen välja mellan att underkasta sig din vilja eller bli \sarad{} (\svartsarad{} om >\introFV+2).

* Om resultatet är > fiendens \introFV{} blir fienden \saradx{}.
* Om resultatet är > fiendens \dubbelt{\introFV} blir fienden \svartsarad{}.
}

\REM{
Ditt \introFV{} är lika med din \KRP{}+2 men en \intro{rustning} och/eller en \intro{sköld} ökar ditt \introFV{} med +1 vardera.

Ditt \introFV{} är lika med din \KRP{}+2 men kan höjas av en \intro{rustning} (+1) och/eller en \intro{sköld} (+1).

Ditt \introFV{} är lika med din \KRP{}+2 men ökar till \KRP+3 om du bär \intro{rustning} eller \intro{sköld} och \KRP+4 om du bär båda.

Ditt \introFV{} är lika med \KRP+2 samt +1 vardera för \intro{rustning} och \intro{sköld}.
}

Ditt \introFV{} är lika med \KRP+3.
En \intro{sköld} ger dig ytterligare +1.
\REM{Har du en \intro{sköld} får du ytterligare +1.}

Du kan som ett \dragx{} \intro{försvara} någon.
Fram till ditt nästa \drag{} hanteras då attacker mot henom istället som attacker mot dig.
Om du försvarar dig själv så får attacker mot dig \nackdelx{}.


# Sår

\REM{
Att vara \intro{sårad} medför inga omedelbara konsekvenser förutom att du blir \intro{utslagen} om du \intro{såras} igen.
}

Att vara \intro{sårad} medför inga omedelbara konsekvenser men om du blir \intro{sårad} en andra gång så måste du \slaforx{\KRP}. 
Om du \misslyckas{} så dör du,
annars förlorar du medvetandet i någon timme\REM{och är därefter \intro{sårad}}.
En \intro{hjälm} ger dig \fordel{} på slaget.

\REM{
\Slafor{\KRP} första gången du får tillfälle att tvätta och förbinda dina sår.
Om du \lyckas{} visar det sig att de inte var särskilt allvarliga och du är inte längre \intro{sårad}.
Annars krävs det ordentlig vård och vila för att såren ska läka.
}

\TODO{
Är det roligare att slå för HVD istället för att det är KRP igen? Då blir det effektiv första hjälpen som tar hand om såren snarare än fysisk ”hårdhet”. Är det kul att HVD blir lite eftertraktat/uppskattat, och att det inte bara är KRP som avgör hur bra man mår i det långa loppet? Eller blir krigarna för nerfade om de inte läker snabbt och lätt?

KRP (och hjälm) håller dem ju ändå levande om de blir utslagna. Med KRP 3 och hjälm så är de ju "odödliga" sånär som på TPK och/eller coup de grace!

I ett team går det förhoppningsvis på ett ut, men en ensam ensidig krigare blir skörare.
}

\Slafor{\HVD} när du är den förste som tvättar och förbinder någons sår.
Om du \lyckas{} så är hen inte längre \intro{sårad}, annars behöver hen ordentlig vila och vård för att läka.

En \intro{rustning} låter dig undvika att bli \intro{sårad} en gång per \stridx{}.
Du kan inte smyga, simma eller sova \REM{och får \nackdel{} när du springer, hoppar eller klättrar} iklädd rustning.
\REM{Du kan dock inte smyga, simma eller sova iklädd rustning.}


# Vapen

\intro{Långa} vapen \ex{spjut, stångyxor…} används med två händer och ger dig, om du inte redan slåss med någon, en gratis \attackx{} mot fiender som stormar dig utan långa vapen.

\Tungax{} vapen \ex{långsvärd, stridsyxor…} kan användas med en eller två händer.
När de används med två händer ger de +1 på \attacker{}.

\REM{Med två händer så ger de +1 på \attacker{}.}

\intro{Improviserade} vapen ger \minus{1}.
Om du är \intro{obeväpnad} får du \minus{2}.

\REM{
Du får \minus{1} på \attacker{} med \intro{improviserade} vapen.
Om du är \intro{obeväpnad} får du \minus{2}.

Om du slåss med ett \intro{improviserat} vapen får du \minus{1} på \attacker{}.
Om du är \intro{obeväpnad} får du \minus{2}.
}

När du slåss med två vapen så \attackerar{} du först med det ena vapnet.
Om du inte \lyckas{} får du istället \attackera{} med det andra.

\REM{
När du slåss med två vapen \attackerar{} du först med det ena och om du inte \lyckas{} får du istället \attackera{} med det andra vapnet.

Om du slåss med ett vapen i vardera handen och din första \attack{} \misslyckas{} så får du även \attackera{} med ditt andra vapen.

Om du slåss med ett vapen i vardera handen \attackerar{} du först med det ena.
Om du inte \lyckas{} får du \attackera{} med det andra.
}

Du kan inte använda \intro{skjutvapen} \REM{\intro{avståndsvapen}} om du är inblandad i närstrid.



