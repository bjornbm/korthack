\REM{
# Magi

Magi är en fundamentalt irrationell företeelse som människors hjärnor inte är konstruerade för att förstå eller nyttja.
Endast genom att erfara magi kan man acceptera dess förnuftsvidriga obergiplighet och lära sig böja den till sin fördel.

Att nyttja magi kräver intensiv mental gymnastik i kamp mot sin egen rationalitet.
I den mörka underjorden och den orörda vildmarken segrar det irrationella lätt, medan spår av levande människor och deras samhällens rationella strukturer undertrycker de irrationella tankebanorna.


# Magiska krafter

Efter varje expedition där du upplevt något magiskt får du \slåförx{} \HVD{}.
Om du lyckas så lär du dig en magisk kraft relaterad till det upplevda
\ex{fråga \SL{} hur den fungerar}.

Du kan hålla upp till \HVD{} magiska krafter i huvudet.
Om du lär dig fler än \HVD{} krafter så måste du välja vilka du vill behålla.

Du kan använda magiska krafter \HVD{} gånger per expedition\REM{dygn?}. \REM{Samma kraft kan användas flera gånger.}
I underjorden och vildmarken fungerar de alltid.
I glesbygd måste du \slåför{} \HVD{}; i tätbebyggelse med \nackdel{}.

Du kan inte förklara dina krafter och därmed inte heller nedteckna eller lära ut dem.


# Magi 2
}

Endast genom att erfara magi kan man acceptera dess förnuftsvidrighet och lära sig böja den till sin fördel.
\Prövax{\HVD} efter en expedition med magiska inslag
för att lära dig en magisk kraft relaterad till det upplevda
\ex{fråga \SL{} hur den fungerar}.

Du kan hålla upp till \HVD{} magiska krafter i huvudet; om du lär dig fler måste du välja vilka du vill behålla.

Att använda en magiskt kraft kräver en intensiv kamp mot din egen rationalitet som du förmår utföra \HVD{} gånger per expedition.
I den mörka underjorden och den orörda vildmarken segrar det irrationella lätt men i civiliserade trakter måste du \pröva{\HVD}, i tätbebyggelse med \nackdel{}.

\REM{
* Man kan använda samma kraft flera gånger, eller olika krafter varje gång.
* Att använda en kraft är ett drag (tror jag?).
* Även om man misslyckas med sitt slag förbrukar man en användning (tror jag).
}

\REM{
# Magi 3

Du kan lära dig upp till \HVD{} magiska \intro{krafter}, som du kan använda upp till \HVD{} gånger \ex{sammanlagt, inte vardera} mellan dygnsvilor.

För att lära dig en ny magisk kraft måste du studera \REM{en skrift med} den i en vecka och \slåförx{\HVD}. En lärare ger dig \fördel{} på slaget.

Den vanligaste sortens krafter är \intro{besvärjelser}---fraser på drakarnas magiska språk som manifesterar effekter i världen när de uttalas.
}
