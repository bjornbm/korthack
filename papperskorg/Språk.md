
\REM{
De flesta vuxna kan utöver sitt modermål även göra sig förstådda på det mest utbredda handelsspråket i sin del av världen.
Utöver dessa talar du ytterligare \VIS{} språk som du kan välja i samband med att du \xr{skapar din rollperson}{skaparp} och/eller \xr{ökar}{nivåer} din \VIS{}, eller låta vara obestämda tills vidare.

Utöver dessa talar du ytterligare ett språk för varje \VIS{} utöver 1. Du kan välja dessa i samband med att du \xr{skapar din rollperson}{skaparp} och/eller \xr{ökar}{nivåer} din \VIS{}, eller låta dem vara obestämda tills vidare.

När du stöter på ett nytt språk får du för varje obestämt språk genomföra en \prövningx{}\REM{kultur}. Om du lyckas så är det just det språket du kan; förklara hur det kommer sig!
}
\REM{
Du talar \VIS{} språk utöver ditt modersmål. Ett av dessa är det mest utbredda handelsspråket i din del av världen, som de flesta vuxna kan göra sig förstådda på.

De övriga språken kan du antingen välja i samband med att du skapar din karaktär och/eller \xr{ökar}{Nivåer} din \VIS{}, eller låta vara obestämda.

Om du lyckas med en \prövning{} när du stöter på ett nytt språk så kan du välja att låta ett av dina obestämda språk vara just det språket. Förklara i så fall hur det kommer sig att du kan just det språket.
}
\REM{
Utöver ditt modermål så kan du---likt de flesta vuxna---göra dig förstådd på det dominerande handelsspråket i din del av världen.

För varje \VIS{} du har utöver 1 så kan du ytterligare ett språk. Du kan antingen välja språket med en gång eller låta det vara obestämt tills vidare.

Om du kan obestämda språk så får du om du vill genomföra en \prövningx{}\REM{kultur} när du stöter på ett nytt språk. Om du lyckas så är det just det språket du kan; förklara hur det kommer sig!

--------

}
Utöver ditt modermål så kan du---likt de flesta vuxna---göra dig förstådd på det dominerande handelsspråket i din del av världen.

För varje \HVD{} du har utöver 1 så kan du ytterligare ett \intro{språk} som
du får välja med en gång eller låta vara obestämt tills vidare.

Om du låtit ett språk vara obestämt så kan du välja att \provax{\HVD} närhelst du stöter på ett nytt språk. Om du lyckas så är det just det språket du kan; förklara hur det kommer sig!
