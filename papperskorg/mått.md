\REM{# Verkmått

Mått        Är lika med…
----------- ------------- ----------------------
linje       --            \approx 2 millimeter
tum         12 linjer     \approx 25 millimeter
kvarter     6 tum         \approx 15 centimeter
fot         12 tum        \approx 30 centimeter
aln         2 fot         \approx 0.6 meter
famn        3 alnar       \approx 1.8 meter
rev         100 fot       \approx 30 meter
stop        --            \approx 1.3 liter
kanna       2 stop        \approx 2.6 liter
tunna       48 kannor     \approx 125 liter
öre         --            \approx 25 gram
mark        8 ören        \approx 200 gram
markpund    20 mark       
lod         --            \approx 13 gram
skålpund    32 lod        \approx 425 gram
lispung     20 skålpund   \approx 8.5 kilogram
}

# Måttenheter

Nedan är ett urval av historiska mått som kan användas som fluff.
Måttangivelserna är väldigt ungefärliga och valda för att göra det enkelt att höfta till kvantiteter.

---------------------------------------- -----------------------------------
linje \ex{\approx 2 millimeter}          jungfru \ex{\approx 1 deciliter}
tum \ex{\approx 25 millimeter}           stop     \ex{\approx 1 liter}
fot \ex{\approx 30 centimeter}           kanna    \ex{\approx 2.5 liter}
aln \ex{\approx 0.6 meter}               tunna    \ex{\approx 100 liter}
famn \ex{\approx 2 meter}                
rev \ex{\approx 30 meter}                lod      \ex{\approx 15 gram}
stenkast \ex{\approx 50 meter}           pund     \ex{\approx 0.5 kilogram}
fjärdingsväg \ex{\approx 2.5 kilometer}  lispund  \ex{\approx 10 kilogram}
---------------------------------------- -----------------------------------

