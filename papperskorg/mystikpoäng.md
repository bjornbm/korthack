# Mystikpoäng { #mystikpoäng }

Du har \dubbelt{\VIS} _mystikpoäng_ (\IP) som du förbrukar för att få \verkanx{} 1 (\checkS), 2 (\checkHH), eller 3 (\checkDDD) när du _aktiverar_ mystiska krafter\sec{krafter}.
Att aktivera en kraft tar normalt \ögonblickx{}, men du kan öka din \verkan{} genom att istället spendera \timmar{} (+1) eller \dagar{} (+2).

För att kunna aktivera en kraft måste du tidigare ha spenderat _timmar_ på att _förbereda_\sec{mystikskolor} den.
Du kan förbereda upp till \VIS{} krafter och du kan aktivera en förberedd kraft upprepade gånger.

Din \verkan{} kan _inte_ ökas av hjälp\sec{hjalpa} eller andra mystiska krafter.

Att förbereda och aktivera mystiska krafter är en \barnlekx{}.

