# Ungefärlig förbrukning { #förbrukning }

Förbrukningsvaror kan bokföras med ungefärlig förbrukning.

Förbrukningsvaran ges en markering som representerar ungefärligt antal användingar.
Varje gång varan används så slås en tärning baserat på markeringen.
Om tärningen visar en etta så uppgraderas markeringen ett steg (\checkO{} \ra{} \checkS{} \ra{} \checkH{} \ra{} \checkD{}).

    Antal     Markering   Tärning
------------ ----------- ---------
 \approx 20    \checkO      \D20
 \approx 10    \checkS      \D10
 \approx 5     \checkH      \D6
    =1         \checkD      --

