Ibland kommer det att uppstå en \intro{tävling} mellan dig och en annan karaktär, till exempel om du försöker hålla fast någon eller springa ifrån någon.

En \intro{tävling} avgörs genom att du och din motståndare slår varsin tärning och lägger till den \GE{} som är mest relevant.
Vilken tärning var och en slår och eventuella \fördelarx{} eller \nackdelar{} hanteras precis som vid \prövningar{}.

Den som får högst resultat vinner \intro{tävlingen}.
Vid lika resultat blir det oavgjort om rimligt, annars slår ni om till någon vinner.

