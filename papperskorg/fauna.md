# Fauna

Typ          SPTVU KP Rust Vapen
------------ ----- -- ---- ---------------------------
Råttsvärm    10200  4      tänder/klor 1
Jätteråtta   10000  1      tänder/klor 1
Varg         20100  2      tänder/klor 2
Ulv          30200  4      tänder/klor 3
Spindel      10000  1      bett 1
Jättespindel 20100  2  1   bett 2
Shelob       40300  6  2   bett 4
Fladdermus   10000  1      tänder/klor 1
Orm          10000  1      bett 1
Pythonorm    20100  2  1   bett 2, kram 2
Jätteorm     30300  6  2   bett 3, kram 3

