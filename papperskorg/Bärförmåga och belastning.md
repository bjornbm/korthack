\FANTASY

Förutom det du bär i händerna kan du ha två lättåtkomliga före\br{}mål som kan användas inom \ögonblickx{}.
Annan utrustning tar minst \ögonblick{} att komma åt.

Bär du fler \tunga{} föremål än din \KRP{} så \misslyckas{} du med allt \svårtx{} som innefattar rörelse.
Ett \mkttungt{} föremål räknas som två \tunga{} föremål.
Vidare räknas följande som \tungt{}:

*   varje påbörjade \sm{100} utöver de 100 första,
*   varje \vapenx{}/\sköldx{} utöver de 3 första,
*   varje påbörjade 10 pilar eller spikar utöver de 10 första,
*   varje påbörjade 5 utöver de 5 första av det mesta annat \ex{dagsransoner vatten eller mat, facklor…}.

\TODO{
*   Skriv om som >100, och låt det vara underförstått att >200 = 2 tunga?
*   Är 10 projektiler för snålt?
*   Sluta specialbehandla kastvapen?
*   Öka gränsen till 4 eller 5 vapen/sköldar? Inkludera hjälmar?
}

\REM{


*   \Silver: varje 100 utöver de 100 första,
*   Vapen/sköldar \ex{förutom kastvapen}: varje utöver de första 3,
*   Pilar/slungstenar: varje 10 utöver de 10 första,
*   Det mesta annat \ex{kastvapen, dagsransoner vatten, färdkost, belysning…}: varje 5 utöver de 5 första.

                                 \Tungt    \Mkttungt
-------------------------- ------------- ------------
\Silver                    >100          >300
Vapen och sköldar          varje >3
Kastvapen                  >5            >10
Pilar                      >10           >20
Övriga förbrukningsvaror   >5            >10
Dagsranson vatten/färdkost >5            >10
Dagsranson färdkost        >5            >10
-------------------------- ------------- ------------

                                 \Tungt    \Mkttungt
-------------------------- ------------- ------------
\Silver                    >100          >300
Vapen och sköldar          varje >3
Pilar/slungstenar          >10           >20
Det mesta annat            >5            >10
-------------------------- ------------- ------------


Tabell med tungt/mkt tungt för olika förbrukningsvaror?

Eller mycket tungt för [ ] och tungt för [\], oavsett förbrukningsvara?? Taskigt mot pilar? Kastvapen är förbrukningsvaror?

}

\cred{
  Maze Rats för bältet.
  OSH för (mkt) tunga föremål.
}
