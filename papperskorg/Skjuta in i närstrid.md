Om du skjuter eller använder \kastvapenx{} mot någon som slåss i närstrid så får du \nackdelx{} på \prövningen{}, och om du \misslyckaskapitalt{} så träffar du en av hens motståndare istället.
