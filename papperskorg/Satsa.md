Genom att \intro{satsa} när du \prövarx{} en \GE{} kan du öka din handlings \skadax{} med 1 på bekostnad av att du får slå den tärning som motsvarar din \GE{}\minus{1}.

Om din \GE{} är 1 så kan du inte \intro{satsa}.
\REM{Man kan alltså bara satsa om man höjt en GE. Vad har det för implikationer?}

\cred{ Mutiné }

