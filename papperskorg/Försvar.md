Istället för att \SLP{} ska \lyckas{} med en \prövningx{} för att attackera dig så kan \SL{} låta det vara en \prövning{} för dig att försvara dig.
Du får i så fall försvara dig varje gång du blir attackerad; \lyckas{} du så missar attacken.
Att försvara sig räknas inte som ett \dragx{}.

Du får \fördel{} på \prövningen{} om din angripare \satsarx{}.

Du får \nackdel{} mot närstridsattacker om du överraskas, är \fasthållenx{}, eller saknar något att parera med.
\REM{ En angripare bör ALLTID satsa i dessa situationer eftersom det inte medför någon kostnad! }

Om du är inblandad i närstrid eller skyddas av bröstvärn eller dylikt så får du \fördel{} mot \avståndsvapenx{}.


\TODO{
Normalt kan man "hindra" angriparen för att ge henom nackdel. Men kan man "hjälpa" sig själv för att få fördel på icke-handingen försvar? Är man begränsad till att "hindra" för att minska angriparesn verkan??
}

\TODO{
Behövs regler för att ducka? (akrobatik, sköld ger ej fördel)
Ducka endast (alltid?) mot avståndsvapen?
}

\REM{

Fördelar:

-   Engagerar spelaren, mindre passivt offer.
-   Sköldar blir goare och medför inga extra-slag.
-   Försvar blir en intressantare färdighet.

Nackdelar:

-   Fiender får färre taktiska möjligheter (se nedan).
-   Fiender kan inte satsa (de kan bara öka skada genom att hjälpa en anfallaren).
-   En fiendes vapen/arena saknar betydelse.

Aspekter:

-   Kapitalt misslyckande = dubbel verkan? Ger mycket större risk för dödlig skada även från mesiga fiender. Gör skölden ännu bättre eftersom kapitalt misslyckande blir omöjligt (om man inte har nackdel förstås).

Det verkar för hårt att ge nackdel om fienden har en lämplig vapen/arena-kombo, så det blir meningslöst vad fienden har för vapen (förutom att tungt ger mer skada, och att RPs loot påverkas).
}
