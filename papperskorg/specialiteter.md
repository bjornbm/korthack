# Specialiteter {#specialiteter}

En \RP{} har vanligtis två _specialiteter_.
Välj specialiteter nedan eller hitta på egna med \SL{}s medgivande.

|    |                                     |    |
|---:|-------------------------------------|---:|-------------------------------------
|  1.| Passiv inkomst\sec{passivinkomst}   | 11.| Kirurgisk precision\sec{kirurgiskprecision}
| 2. | Riddare\sec{riddare}                | 12.| Regenerera\sec{regenerera}
| 3. | Hamnskiftare\sec{hamnskiftare}      | 13.| Mörkerseende\sec{mörkerseende}
| 4. | Följeslagare\sec{följeslagare}
| 5. | Djurföljeslagare\sec{djurföljeslagare}
| 6. | Mystiktränad\sec{mystiktränad}
| 7. | Självförsvarsexpert\sec{självförsvarsexpert}
| 8. | Favoritvapen\sec{favoritvapen}
| 9. | Eterisk\sec{eterisk}
| 10.| Vass tunga\sec{vasstunga}


\REM{
# Specialiteter {#specialiteter}

Välj specialiteter nedan eller hitta på egna med \SL{}s medgivande.

1.  Passiv inkomst\sec{passivinkomst}
2.  Riddare\sec{riddare}
3.  Hamnskiftare\sec{hamnskiftare}
4.  Följeslagare\sec{följeslagare}
5.  Djurföljeslagare\sec{djurföljeslagare}
6.  Mystiktränad\sec{mystiktränad}
7.  Självförsvarsexpert\sec{självförsvarsexpert}
8.  Favoritvapen\sec{favoritvapen}
9.  Eterisk\sec{eterisk}
10. Vass tunga\sec{vasstunga}
11. Kirurgisk precision\sec{kirurgiskprecision}
}


# Passiv inkomst {#passivinkomst}

Du är delägare i ett vidsträckt företag som genererar en stadig inkomst.
Andra står för den dagliga driften av företaget men du har rätt till en avkastning motsvarande din nivå gånger \sm{\D8} per dag.

Du kan arrangera att få din ackumulerade avkastning utbetald inom \dagarx{} i de flesta samhällen med goda kommunikationer.
I större städer kan du ofta få den på \timmarx{}.

Du börjar spelet med \sm{\D100} extra.


# Riddare {#riddare}

Du har en svuren plikt att tjäna och representera en länsherre eller en orden.
I gengäld har du tilldelats en titel, en \tung{} rustning, ett valfritt vapen\sec{vapen} och ett stridstränat riddjur.

I dina förpliktelser ingår att följa ett eller flera påbud, samt att på begäran bistå din sponsor.
Ett påbud kan exempelvis röra ditt uppträdande, upprätthållandet av ditt eller din sponsors rykte, eller en ekonomisk plikt.


# Hamnskiftare {#hamnskiftare}

Du har förmågan att förvandla din kropp till en specifik djurform. Förvandlingen omfattar hela din kropp och tar \sekunderx{}.

Din djurform har en starkare och en svagare \GE{}.
När du förvandlar dig till din djurform kan du välja att öka din starkare \GE{} med på bekostnad av att din svagare \GE{} minskar (till ett minimum av 1).
Dina resurser förändras inte.

Dina tillhörigheter förvandlas inte med dig.


# Följeslagare {#följeslagare}

Du har en följeslagare som av någon anledning är förpliktigad eller svuren att följa och tjäna dig vart du än må gå.

Din följeslagare börjar som en nivå\sec{nivåer} 0-karaktär med en specialitet.
Hen får \EP{} och kan öka sin nivå precis som du, men måste alltid ha lägre nivå än du har.

Du behöver kunna betala din följeslagares omkostnader, men inte någon dagstaxa.


# Djurföljeslagare {#djurföljeslagare}

Du har ett starkt emotionellt band till ett djur som hängivet följer dig överallt.
Du kan med ljud och gester kommunicera med din djurföljeslagare på en nivå motsvarande dess intelligens, och den lyder dina order efter bästa förmåga.

Om du förlorar ditt djur kan du knyta nya band till ett annat djur i din närhet genom att spendera \dagarx{} på tillvänjning och träning av det.
Detta bryter bandet till ditt gamla djur.


# Mystiktränad {#mystiktränad}

Du har tränats i en valri mystikskola\sec{mystikskolor} hos en mästare, på en akademi, av en religiös orden, eller i en kult.

Du äger en \tung{} bok innehållande alla skolans krafter, samt en mystisk artefakt\sec{mystikskolor} av en för skolan lämplig natur.
Denna artefakt ger dig +1 i \verkanx{} vid aktivering av skolans krafter.


# Självförsvarsexpert {#självförsvarsexpert}

Du är tränad av mästare i självförsvar i att träffa dina motståndares svagaste punkter och orsaka fruktansvärda skador.
Dina bara händer kan vara dödligare än det vassaste vapen.

När du slåss obeväpnad och träffar en fiende kan du spendera 1 (\checkS{}), 2 (\checkHH{}), eller 3 (\checkDDD{}) \FP{} för att öka din \verkanx{} med 1, 2, respektive 3.


# Favoritvapen {#favoritvapen}

Du äger ett vapen till vilket du har ett själsligt band.
Vapnet kan exempelvis vara en släktklenod eller av religös betydelse för dig och du känner och värderar det lika högt som dina egna händer.

Välj ett vapen\sec{vapen} och ge det ett namn.
När du träffar en fiende med vapnet kan du spendera 1 (\checkS{}), 2 (\checkHH{}), eller 3 (\checkDDD{}) \IP{} för att öka din \verkanx{} med 1, 2, respektive 3.

Om du förlorar ditt vapen kan du knyta nya band till ett annat vapen i din ägo genom att spendera \dagarx{} i träning med det.
Detta bryter bandet till ditt gamla vapen.


# Eterisk {#eterisk}

Av någon anledning saknar din kropp fysisk form.
Du har normalt utseende och kan tala, men kan i övrigt enbart interagera med den mundana världen genom mystik (till exempel _telekinesi_\sec{mental}).

Du kan inte passera igenom fysiska barriärer.
Du påverkas av gravitation men du kan inte bli skadad av mundan \verkanx{}.

Du har eteriska kläder och utrustning men de fyller mest en estetisk funktion.


# Vass tunga {#vasstunga}

Din tunga är vassare än svärdet och du vet exakt vad du ska säga för att reta upp dina motståndare, få dem att handla överilat och blotta sig i sin ilska.

När du slåss i närstrid mot någon som förstår dina förolämpningar så får du om du vill använda \UTS{} istället för \STY{} som \verkanx{}.


# Kirurgisk precision {#kirurgiskprecision}

Du besitter de nerver som krävs för att vänta ut dina fienders misstag samt den precision som krävs för att nyttja de blottor de exponerar.

När du slåss i närstrid med ett lätt vapen så får du om du vill använda \PRE{} istället för \STY{} som \verkanx{}.


# Regenerera {#regenerera}

Du har av någon anledning oerhört gott läkekött. Kanske har du en gnutta trollblod, eller är av en nästan odödlig ras.

Du återfår \KP{} markerade med \checkD{} efter \timmarx{} av ostörd vila, \KP{} markerade med \checkH{} efter \ögonblick{} av ostörd vila, och \KP{} markerade med \checkS{} automatiskt i början av din nästa tur (utan vila).


# Mörkerseende {#mörkerseende}

Du har extremt känsliga ögon, och de kan till viss del även uppfatta värmestrålning. Detta gör att du kan se i kompakt mörker.

I kompakt mörker ser du dina närmaste omgivningar, motsvarande en facklas ljus\sec{ljus}.
Med endast en gnuttas ljus så ser du allt som är \nära{}, och med en fackla eller liknande belysning så ser du som i fullt dagsljus.

\REM{ Behöver du skydda dina ögon mot starkt ljus/solen? }

\REM{
  Mörkerseende
  Adlig
  Immun
  Polyglot
}
