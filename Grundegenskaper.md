Du har tre \introA{grundegenskaper}{ge} som mäter hur kapabel du är i olika sammanhang:
\REM{Ju högre värde du har i en \introGE{} desto bättre.}

*   \introA{Kropp}{krp}
när du behöver styrka, koordination eller ork \REM{"uthållighet" får inte plats}
\ex{slåss, \SCIFI{skjuta,} springa, simma, balansera, hoppa, klättra …}.
*   \introA{Huvud}{hvd}
när du behöver kunskap eller sinnesskärpa
\ex{\SCIFI{vetenskap, teknologi, }\FANTASY{skjuta, hantverk, }språk, överlevnad, uppmärksamma, spåra …}. \TODO{Fult byte från verb till substantiv}
*   \introA{Känsla}{kns}
när du behöver påverka dig själv, andra eller hur du uppfattas
\ex{övertala, bluffa, smyga, uppträda\REM{gömma dig}, mod …}.

\REM{
när du behöver mod eller kunna påverka andra och hur du uppfattas
när du behöver inre styrka eller  påverka hur du uppfattas
}
