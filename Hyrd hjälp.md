Du kan anställa tillfällig hjälp enligt dagstaxorna nedan.
\REM{ Ersättning för deras omkostnader i din tjänst tillkommer. -- Inte så viktigt nu när förbrukningsvaror är "oändliga"!}

* Tjänare    \ex{flyr om angripen}                    \hfill  \sm{1}\qquad{} 
* Vägvisare  \ex{slåss i självförsvar}                \hfill  \sm{2}\qquad{} 
* Hyrsvärd   \ex{slåss på befallning}                 \hfill  \sm{5}\qquad{} 
* Specialist \ex{slåss om specialitet}                \hfill \sm{10}\qquad{} 

\noindent
Dina anställda handlar på ditt \dragx{}.
Du kan ge order till \UTS{} av dem,
övriga kommer att agera i egenintresse.
Illa behandlade anställda \slarformoralx{} med \nackdelx{}.

\REM{
\Slafor{\UTS} efter ett spelmöte för att övertala en av dina anställda att istället bli din \foljeslagarex{} \ex{från och med nästa spelmöte}.
}

\TODO{
-   Rekrytering? (Övertala för att få med dom?)
-   Moralslag efter uppdrag (Övertala för att behålla?)
}
