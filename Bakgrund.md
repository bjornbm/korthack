Du har tröttnat på ditt obetydliga liv i din obetydliga hemby och gett dig av i jakt på äventyr och spänning. 
\REM{Var du:}Hur utmärkte du dig?

1.  Du var starkast    i byn \ex{\KHU{2}{1}{1}}.
2.  Du var smartast    i byn \ex{\KHU{1}{2}{1}}.
3.  Du var vältaligast i byn \ex{\KHU{1}{1}{2}}.

\REM{
Vad hade du för \yrkex{} innan du begav dig av?
Ditt \yrke{} kan, om det är relevant, ge dig \fordelx{} en gång per spelmöte.
}

\REM{
Välj två saker nedan som du fått med dig.
Du får välja samma sak två gånger om du har 2 i den angivna \grundegenskapenx{}.

1.  en \tung{} \rustningx{} eller ett \tungt{} \vapenx{} \ex{\KRP}
2.  en \besvarjelsex{} \ex{\HVD}
3.  en \foljeslagarex{} med 1 i alla \GE{} \ex{\UTS}
}
