Spelets fantasivärld är kulturellt och teknologiskt snarlik medeltida Europa, men med utbredd polyteism och ett fantastiskt förflutet.
För inte länge sedan härskade fantastiska folkslag över magiska riken med gudars nåd, men dessa folkslag har dragit sig undan eller dött ut och gudarna har blivit likgiltiga.
Kvar är människorna, arvtagarna till en magi- och gudalös värld.

Vid civilisationens utkanter och bortglömda vrår, långt ut i vildmarken eller djupt under jord, kan man dock ännu finna spår av det som en gång var.
Fantastiska spår, och fruktansvärda spår.

