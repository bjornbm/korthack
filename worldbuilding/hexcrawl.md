# Hexkräl

*   Väder
    -   sol (brännande)
    -   moln (dimma eller blåst)
    -   regn (skyfall och/eller åska)
    -   snö (snöstorm)

*   Berg
*   Kullar
*   Slätt
*   Träsk
*   Hav

*   Lövskog
*   Barrskog
*   Jordbruk
*   Flod
*   Sjö
*   Ö
*   Väg
*   Begbyggelse

                                        
            A1      A2      A3          
            T      Båska   Kbarr        
                                        
        B1      B2      B3      B4      
       Tmang   Sbarr   Kbarr  •Bglac    
   ••••                    •••          
  ••C1••    C2      C3    : C4      C5  
  ••Vö••   Slöv    Sbarr • Kklyk ••Bfall
   •••••••••••••••••|••••••••:•••       
      ••D2••    D3  |   D4      D5      
      ••V••#   Sbro | Slöv    Klöv      
       •••• `----#-----------------     
            E3   |  E4      E5          
           Sjord | Kgruv   Klöv         
                 |                      

              |-----|
             /       \
      |-----|         |
     /       \       /
    |         |-----|
     \       /       \
      |-----|         |
             \       /
              |-----|

*   Träsk som gränsar till berg
*   Träsk med flod genom
*   Träsk med skog (mangroove?)
*   Flod med raserad bro & färjkarl
*   Bergspass till dold dal
*   Väg genom skog
*   Sjö med ö
*   Vadställen
*   Jordbruk
*   Skogshuggare
*   Jägare
*   Vildar
*   Kullar med skog
*   Bara kullar med gravar


1.  Träsk som gränsar till berg
2.  Träsk med skog (mangroove) som gränsar till hav
3.  Berg med åska
4.  Berg med sjö och vattenfall
5.  Berg med glaciär
6.  Kullar med skog
7.  Bergspass till dold dal
8.  Kullar med flodgren och vadställen
9.  Lövskog med väg
10. Hav med fiskeby
11. Hav som gränsar till träsk
12. Stad omgärdad av jordbruk
13. Jordbruk med byar
14. Jordbruk med byar
15. Lövskog
16. Kullar med gruvor
17.
18.
19. Dold dal kullar med barrskog


# Dungeons

*   The Tomb of Durahn Oakenshield     [bergen norr om vägen]
*   The Screams from Jedder's Hole     [gamla klostret]
*   The Burial Mound of Esur the Red   [kullarna i klykan]
*   Lord of the Rats (Dungeonslayers)  [staden?]
*   Fortress of Doom (Dungeonslayers)  []
*   The Goblin Grail Gang              []
*   Marsklandet?                       [träsken?]
*   Malachite Mines (Blood & Bronze)   [gruvorna]
*   The Halls Untoward                 [kullarna? åskbergen? dalen?]
*   Goffmans Koffert (GrottZine 6)     [vägen?]
*   Karl Stjernberg one-pagers
*   Goblin mine dungeon zone??
*   Caves of Cormakir the Conjourer (FFB2RD)
*   Grey Halls (EMO) [[2019_01_16_2208]]
*   Dyson Logos's Lair of the Frogs [[2019_01_18_1551]]
*   Lost Tomb of the Skeleton King (ICRPG) [Moldy Codex 1-sample.pdf]
*   Tomb of the Serpent Kings 4.0
*   The Old Well [THE 52 PAGES Marcqh 2016.pdf, page 51]
*   Häxans brunn [Niklas Wistedt]
*   Dorgotan Dungeon (Axebane) [Dorgotar_Dungeon.pdf]


# Vad hände med förra expeditionen?

Om du inte tar dig tillbaka till äventyrargillets högkvarter vid spelmötets slut, slå \D8 om du är i vildmarken och \D6 om du är i en dungeon.

\REM{Slå för överlevnad? Använd IP?}

1.  Du är död.
2.  Du är försvunnen.
3.  Du förlorar din ryggsäck (allt du inte kan bära i händer och bälte)
4.  Du återvänder utan vapen och rustning(ar).
5.  Du återvänder utan silvermynt och skatter.
6.  Du förlorar allt utom ett föremål.
6.  Du förlorar alla \tunga{} föremål.
7.  Du återvänder men först efter att du har spelat ett spelmöte med en annan \RP{}.
8.  Du återvänder helskinnad.


# Geotyper

Slätt
Slätt med skog
Träsk
Träsk med skog
Kullar
Kullar med skog
Berg
Berg med glaciär


# Landmärken

Ovanligt träd (stort, bränt, dött)
Bäck
Djurstig
Ovanlig sten (stod, bumling)
Krön
Övergiven hydda
Ruin
Dunge
Glänta
Tjärn
Sänka
Gammal mur
Gammal brunn
Ravin
Klippvägg
Stenstod (naturlig)
Stenstod (onaturlig)
Spår av strid
Trasig/övergiven vagn

