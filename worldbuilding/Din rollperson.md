Du är en \xr{äventyrare}{skapa-din-rollperson} som av någon anledning trotsar faror för att uppsöka spår och rester från en magisk \xr{forntid}{världen}.

Kanske söker du artefakter som ska ge dig rikedom eller makt.
Kanske söker du kunskap och sanning. \REM{Kanske söker du mening.}
Kanske söker du utplåna rester du fruktar kan vakna i mörkret och tillintetgöra mänskligheten.
Kanske är du bara på jakt efter spänning och ära.
Kanske duger du inte till annat, eller är på flykt undan ditt eget förflutna.

För att minska riskerna med ditt värv är du medlem i ett \xr{äventyrargille}{äventyrargillet}.
Tillsammans organiserar gillets medlemmar expeditioner i det okända och delar information och erfarenheter med varandra.

\REM{
Läs igenom reglerna och skapa din \RP{}\sec{skaparp}.
Om du har bråttom kan du skapa din \RP{} medan du läser de viktigaste reglerna\sec{snabbstart}.
}
