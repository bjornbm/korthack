Som medlem i äventyrargillet får du följande förmåner:

*   lokal skattbärgar- och vapenbärarlicens,\REM{
*   tillgång till gillets kartor och informationsarkiv,}
*   ramavtal med hantlangare\sec{hantlangare} med gillet som borgenär,
*   konstnadsfri mat och sovplats på gillets högkvarter.

Om du avlider under en expedition åtar sig gillets medlemmar att, såvida riskerna och kostnaderna det innebär är rimliga:

*   omhänderta din kropp enligt din religions sed,
*   fördela dina tillhörigheter enligt ditt testamente.

I gengäld förväntas du löpande betala ett tionde till gillet.

