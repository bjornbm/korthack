-   Mystikskolor
    -   Barmhärtig helig mystik
        +   Hela
        +   Kurera (gift, åkommor, paralys, förstening)
        +   Rena (mat/vatten)
        +   Välsignelser
    -   Rättfärdig helig mystik
        +   Sannsyn – kan flyttas?
        +   Vända odöda
        +   Gudomlig rustning
        +   Guds vrede (blixtar?)
    -   Nekromanti
        +   Animera död
        +   Väck död
        +   Dominera lägre odöd
        +   Återuppliva
    +   Alkemi
        +   Granater
        +   Syra (mundant eller hi-tech?)
        +   Bloss
        +   Blixtladdning
    -   Musikalisk mystik
        +   Inspirera (+1 effekt)
        +   Söva
        +   Valkyrierytten (moralslag)
        +   Imponera (Locka/förföra/charma)
    -   Häxkonst
        +   Mystisk dryck
        -   Förbannelser?
            -   Onda ögat?
        -   Kärlek
    -   Shamanism/druidism
        -   Djurform/Djursinnen
        -   Kalla på/tala med/kontrollera djur
        -   Tala med djur/livsform/föremål
        -   Väderkontroll
        -   så (buske, litet träd, stort träd) dagar, timmar, sekunder.
    -   Demon/kaotisk/mörk magi
        -   Frammana (minions/prins)
        -   Skräck/skrämma
        +   Mörker
        -   Galenskap
    -   Illusionism
        +   Illusion (antal sinnen)
        +   Osynlighet
        +   Tystnad
        +   Mörker
    +   Elementmystik
        +   Skapa element
        +   Forma element (kräver elementet)?
        +   Frammana elementar (kräver elementet)
        +   Elementfärd
    -   Trolldom/magi
        +   Ljus
        +   Mörkerseende
        +   Öppna
        +   Försegla
        -   Hypnotisera
        -   Glömska
        -   Portal?
        +   Teleportera
    -   Kommunikativ mystik
        +   Telepati
        -   Fjärrmeddelande
        -   Pergamentportal (länka två papper så skrift överförs automatiskt … spara en karta på värdshuset?)
        -   Polyglot
    -   Skärskådande mystik
        +   Läs tankar
        -   Sanning
        -   X-ray vision
        -   Sannsyn
    -   Metamystik
        +   Läs mystik
        +   Antimystik
        +   Mystifiera (gör rustning/vapen tillfälligt mystiska)
        +   Skriftmystik
    -   Övriga/tveksamma/metagaming
        -   Sia/spå (fördel för handlingar i linje med spådomen)
        -   Föraning (nästa slag)
        -   Dödsmärkt
        -   Dödens hand?
        -   Blodsmagi? (Thaumaturgi?)
        +   Vattenandning – ersätts av elementväg
        +   Flyga – ersätts av elementväg
        +   Elementprojektil

