# Nordfäst

Statsstat i norra hörnet av civilisationen.

Näringsliv:

*   Saltutvinning ur saltvattenkanalerna i väster. Boskap.
*   Jordbruk i norr i bördiga marker längs floden.
*   Gruvdrift i kullarna i öster. Slavar, straffarbetare och utfattiga. Mineraler och prydnadsmaterial (marmor, malakit, agat, kristaller, amethyster…).
*   Mindre:
    -   Skogshuggare/timmermän
    -   Jägare (päls)
    -   Fiske
    -   Pilgrimmer


# Nomader

Nomaderna kommer från ingen vet vart och flyttar omkring efter outgrundade mönster. De har vagnar dragna av oxar och tält. De har sierskor och hallucinatoriska drycker. De har sång och dans och muntlig tradition. De har fyrverkerier (rökbomber, flashbangs, till och med granater).


# Södra landsvägen

I söder har Nordfäst en orolig gräns med sina kulturellt närstående grannstat Tyrrne. Ibland orolig vapenvila, ibland skärmystlingar, ibland öppet krig.

Överlag går det ganska civiliserat till och tack vare den stora mängden skötsamma soldater i området går resande och handelsmän går säkra på södra landsvägen när det inte är öppet krig (visitationer förekommer och vapen kan beslagtas).

Nordfäst har en garnisson i en fästning ungefär en mil söder om staden. Civila från den omkringliggande landsbyggden tar skydd där (eller i staden) i orostider). Garnissonen består av c:a 200 soldater och 20 kavallerister.

Många av Nordfästs soldater är hyrsvärd från barbarländerna i öster. De leds alltid av officerer från Nordfäst och är överlag skötsamma tack vare hårda straff för olydnad och övergrepp mot civila (dödsstraff eller livstids straffarbete i gruvorna).


# Östra landsvägen

I öster gränsar Nordfäst mot de så kallade barbarländerna. Barbarerna är fragmenterade i klaner och på grund av ständiga inbördes maktkamper teknologiskt och ekonomiskt mindre utvecklade än Nordfäst och Tyrrne.

Landsvägen österut terroriseras av rövare som visat sig var svåra att rota ur skogarna den passerar igenom, men den är ända en viktig livlina för Nordfäst i tider av krig med grannarna i söder.

Östra landsvägen används till handel med barbarerna och med företagsamma handelsmän som kommer resande i välbeväpnade karavaner med exotiska varor från fjärran länder. Den förser även Nordfäst med en strid ström av hyrsvärd till södra fronten och slavar till gruvorna.



