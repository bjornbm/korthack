# Hexor

      a b c             a
     d e f g        b       c
    h i j k l   d       e       f
     m n o p        g       h
      q r s     i       j       k
                    l       m
                n       o       p
                    q       r
                        s

Åskbergen
Isbergen
Bergsjön
Döda träsken
Mangrooveträsket
Ön
Skogsdeltat
Vadkullarna
Gruvkullarna
Östra skogen
Östra skogskullarna
Södra skogskullarna
Ruinskogen
Glömda dalen


# Noder

Staden
Fiskebyn

# Kanter

Östra landsvägen
Södra landsvägen
Södra vadstället
Norra vadstället
Gamla vägen
Gamla bron
