---
title:  'Guide till spelvärlden'
subtitle: 2019-03-17
...


# Riket

Riket är benämningen på den samling kulturellt närbesläktade länder som utgör huvuddelen av den kända världen.
Riket är kulturellt och teknologiskt snarlikt medeltida Europa.

Riket var en gång ett verkligt rike, enat och mäktigt, men är numer splittrat i en mängd oberoende och inte sällan rivaliserande länder.
Trots denna söndring är Rikets invånare stolta över sin härkomst och anser sig vara kulturellt överlägsna barbarerna från bortom rikets gränser.

Rikets tideräkning utgår från året för den unionsbildning som anses vara Rikets begynnelse.


# Barbarländerna

Bortom Rikets gränser ligger de så kallade barbarländerna med många olika folkslag och kulturer.
Barbarerna är, med några få undantag, fragmenterade i klaner och på grund av ständiga inbördes maktkamper teknologiskt och ekonomiskt mindre utvecklade än Rikets länder.

Barbarländerna är viktiga källor av råvaror, slavar och legosoldater till Riket som i sin tur exporterar verktyg och lyxartiklar som barbarerna själva inte förmår producera.


# Dvärgar och alver

Dvärgarna var en mäktig ras med enastående ingenjörskunskaper och arkitektur och i ständig kamp med vättar om underjordens utrymmen och rikedomar.

Riket hade diplomatiska relationer och handel med dvärgar, men dvärgarna bröt kontakten och försvann in i bergen. \REM{Ingen vet varför eller vart de tog vägen.}
Inga kända dvärgsamhällen finns kvar men dvärgarna har lämnat många ruiner, underjordiska städer och andra komplex efter sig.
Dessa är numer i bästa fall tomma, i värsta fall övertagna av vättar eller något ännu värre.

Det finns sagor och legender äldre än Riket om ädla och uråldriga alver men det saknas otvetydiga spår av deras existens.

\REM{
# Vättar

_Vätte_ är ett samlingsnamn för alla de onaturliga och onda människoliknande varelser som kryper ner från de branta bergen och ut ur de mörka skogarna för att terrorisera mänskligheten.

Majoriteten av de vättar som gör räder mot bosättningar och handelsresande rapporteras vara småväxta och ljusskygga men lömska, blodtörstiga och hänsynslösa.

Vättar som i storlek är jämförbara med vuxna människor brukar kallas _illvättar_.
Ännu större varelser benämns _troll_ eller _jättar_.


# Odöda

Det finns dokumenterade fall av bosättningar som angripits av odöda horder.
Många menar att det också förekommer spöken och gengångare av andra slag men det är omtvistat vad som är sanning och vad som är sägen.

Det finns en stor fruktan för nekromantiker, men de sista kända nekromantikerna besegrades av Sols förkämpar för hundratals år sedan.


# Andra monster

Historier och skrönor om varelser och monster av de mest fantastiska slag är populära både i Riket och bland barbarerna.
Detaljerna är dock ofta luddiga och trovärdigheten tveksam.

Drakar har funnits men det är länge sedan någon siktades i Riket.
}

# Monster

_Vätte_ är ett samlingsnamn för de ondskefulla människoliknande varelser som kryper fram ur mörkret och terroriserar bosättningar och resenärer.
De flesta vättar rapporteras vara småväxta och ljusskygga men blodtörstiga och hänsynslösa.
De som i storlek är jämförbara med vuxna människor brukar kallas för _illvättar_ medan de som är ännu större benämns _troll_ eller _jättar_.

Odöda skeletthorder härjade innan Sols förkämpar besegrade de sista nekromantikerna för tvåhundra år sedan.
Många tror också på spöken och gengångare av andra slag.

Det är länge sedan någon drake siktades i Riket och historier om andra fantastiska varelser och monster kan sällan styrkas.


# Mystik och magi

Magi, mirakler och gudomliga ingripanden sägs ha varit vanligt förekommande i Rikets tidiga dagar, liksom häxkonster, förbannelser och demoniska styggelser.
Numera finns det dock inte längre några kända magiker kvar i Riket och gudarna verkar ha blivit likgiltiga till människornas vädjanden och vedermödor.

De få magiska artefakter som överlevt till nutiden med sina förmågor i behåll anses ovärderliga.


# Språk

Inom Riket talas framförallt _riksmål_.
Dialektala variationer av riksmål förekommer i olika länder och regioner.

Barbarländerna har inget enhetligt språk, men ofta talas närbesläktade språk inom en region.
Tack vare den viktiga handeln med Riket talar många barbarer även knaggligt riksmål.

Dvärgarna talade och ristade runor på så kallat _dvärgamål_.

Många vättar verkar använda ett gemensamt förhatligt språk som i Riket benämns _vättetunga_.


# Religion

Den dominanta religionen i Riket är soldyrkan vars gudom är solen (”Sol”).
Den största religiösa organisationen är Solkyrkan som härstammar från Rikets storhetstid.

Det finns många faktioner inom Solkyrkan och Sol ses av vissa som en barmhärtig och vårdande gud \ex{”må Sols strålar värma dig”, ”må Sols ljus lysa din väg”} och av andra som en hård och bestraffande gud \ex{”må Sols strålar bränna dig”}.

Mission från Riket har omvänt många av barbarerna till soldyrkan, men shamanism, naturkulter och gammal vidskepelse är fortfarande vanligt förekommande i barbarländerna.

\TODO{

"Sols väg"
Sol
Häxjakt
Mirakel
Gudarna tappat intresset
Sekter/kulter
Gamla
Nya
}


\REM{ Kampanjen}

# Tyrrne

Landet _Tyrrne_ ligger i Rikets nordöstra hörn och utgör ytterligheten av rikets expansion mot vildmarkens mörker i norr och mot barbarländerna i öster.

Bland Tyrrnes kullar och berg finns lämningar från ett forna dvärgarike och i Tyrrnes norra delar har det också uppdagats spår av andra svunna folkslag och bortglömda civilisationer.

Tyrrne styrs av _drottning Alyndra III_ från huvudstaden _Gagnved_.


# Norrläna

I norra Tyrrne, gränsande mot outforskad vildmark, ligger det ganska obetydliga _Norrläna_.
Norrläna var ett av de sista geografiska områdena som erövrades av riket och är än idag hårt ansatt av vättar som kryper fram ur dess mörka skogar och underjordiska hålor för att terrorisera människornas bosättningar.

Norrläna är förlänat till _friherre Tathor_ som styr från sin borg i _Nordfäst_, Norrlänas enda stad.

I Nordfäst finns ett unikt äventyrargille vars medlemmar åtnjuter generösa licenser och villkor under den så kallade _Vätteutdrivningsdeklarationen_, upprättad av friherren i Rikets år 785.


# Vätteutdrivningsdeklarationen

Till stridsföra män och kvinnor av alla nationaliteter  som registrerar sig som fullvärdiga medlemmar i det för ändamålet upprättade äventyrargillet erbjuder friherre Tathor:

1.  Uppehållstillstånd, vapenbärarlicenser och skattbärgarrättigheter giltiga i Norrläna och angränsande vildmarker.
2.  En ersättning på \sm{5} per dräpt vätte (mot bevisning).
3.  Begränsad amnesti för tidigare brott begångna utanför Norrlänas gränser, gällande endast inom Norrläna och under förutsättning att ytterligare brott inte begås.

I gengäld förbinder sig äventyrargillets medlemmar att till friherre Tathor betala ett tionde av värdet av alla skatter som bärgas.


# Äventyrargillet

Äventyrargillets medlemmar organiserar tillsammans expeditioner i det okända och delar information och erfarenheter med varandra.
På gillets högkvarter har medlemmarna tillgång till gillets arkiv samt kostnadsfri mat och sovplats av enklare slag.

Som medlem i äventyrargillet bär man en hederstitel baserad på sin erfarenhet:
_fackelbärare_ (nivå 1),
_vägvisare_ (nivå 2),
_äventyrare_ (nivå 3),
_utforskare_ (nivå 4), eller
_erövrare_ (nivå 5+).
\REM{
1) _fackelbärare_,
2) _vägvisare_,
3) _äventyrare_,
4) _utforskare_, och
5) _erövrare_.
}
\REM{
_fackelbärare_ (\nivå{} 1),
_vägvisare_    (\nivå{} 2),
_äventyrare_   (\nivå{} 3),
_utforskare_   (\nivå{} 4), och
_erövrare_     (\nivå{} 5).
}
\REM{
|            |                |
|------------|----------------|
| \nivå{} 1: | _fackelbärare_ |
| \nivå{} 2: | _stigfinnare_  |
| \nivå{} 3: | _äventyrare_   |
| \nivå{} 4: | _utforskare_   |
| \nivå{} 5: | _erövrare_     |
}

Om en medlem skulle avlida under en expedition åtar sig övriga medlemmar att, såvida riskerna och kostnaderna det innebär är rimliga, omhänderta hens kropp och fördela hens tillhörigheter enligt hens testamente.

