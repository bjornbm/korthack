\REM{
https://www.gp.se/nyheter/göteborg/hur-skadad-är-en-allvarligt-skadad-person-1.1235531
}

När du får \intro{livshotande skador} så måste du \slaforx{\KRP}.
\FANTASY{En \intro{hjälm} ger dig \fordel{} på slaget.
} Om du \misslyckas{} så dör du.
Annars är du utslagen med skador som består tills du får långvarig vård,
till exempel en

1.  kapad lårmuskel \ex{kan inte springa}.
2.  krossad axel       \ex{armen är obrukbar}.
3.  punkterad lunga    \ex{\slarfor{\KRP} med \nackdel{}}.
4.  hjärnskakning      \ex{\slarfor{\HVD} med \nackdel{}}.
5.  bruten käke        \ex{\slarfor{\UTS} med \nackdel{}}.
6.  inre blödning      \ex{en \FP{} återfås inte}.

\REM{
2.  Krossad hand       \ex{handen är obrukbar}
3.  Brutna revben      \ex{\nackdel{} på \KRP{}}
}
