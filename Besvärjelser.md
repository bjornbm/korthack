\hyphenation{ma-ni-fest-er-ar}

\intro{Besvärjelser} är fraser på drakarnas magiska språk som manifesterar effekter i \varldenx{} när de uttalas. \REM{Deras effektivitet beror på precisionen i uttalet.}

Du kan lära dig upp till \HVD{} besvärjelser som du kan använda upp till \HVD{} gånger \ex{sammanlagt, inte vardera} mellan dygnsvilor.

För att lära dig en besvärjelse måste du studera \REM{en skrift med} den i en vecka och \slaforx{\HVD}. En lärare ger dig \fordel{} på slaget.
