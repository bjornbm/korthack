Du kan förvandla blodet hos en levande varelse du rör vid till ett gift som får musklerna \ex{inklusive hjärtat} att krampa och blodkärlen att svartna under huden.

Om ditt offers \KRP{} är lägre än ditt \HVD{} så dör hen omedelbart, annars får hen \sHVD{}+\HVD{} \skadax{}.
