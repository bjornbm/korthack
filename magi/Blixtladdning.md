Du kan ladda ett metallföremål med elektrisk energi i form av \HVD{} blixtar.
När föremålet vidrör någon laddas en blixt ur i henom och hen får \sHVD{} \skadax{}.
Energi som inte laddats ur avklingar efter en \HVD{} timmar.
