Du kan göra dig själv osynlig.
Allt du bär på eller plockar upp blir också osynligt.

Om du påverkar andras sinnen (exempelvis genom beröring, höga ljud, eller synbar påverkan på omgivningen) \HVD{} gånger så blir du genast synlig.
