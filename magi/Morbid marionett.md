Du kan animera en död kropp inom synhåll i \HVD{} \rundorx{}.
Kroppen har \KRP lika med ditt \HVD{} och 3\ggr\KRP{} \KP{}.
Den handlar på ditt \drag{} och enligt dina önskemål.
