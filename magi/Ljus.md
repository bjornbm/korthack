Du kan få ett valfritt föremål \narax{} dig att lysa i \HVD{} timmar.
Det ger fullgott \ljusx{} till \HVD{} närbelägna karaktärer och \term{svagt ljus}{belysning} på \nara{} håll.

Du kan låta ljuset ta slut i förtid med ett intensivt blixtsken.
Alla som tittar mot ljuset måste då \provax{\HVD} för att inte bli bländas i en \rundax{} (\sHVD{} \rundor{} om deras ögon var anpassade för mörker).
