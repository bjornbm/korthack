Du kan väcka döda ur deras rofyllda sömn.
De vaknar fyllda av avundsjuka och hat mot alla levande \REM{inkusive dig} som de söker upp och attackerar i blint raseri tills de själva blir nedgjorda.

Du väcker \sHVD{} döda som väljs slumpvis från kroppar \ex{möjligtvis begravda} i närheten och har \KRP{} och \KP{} som när de levde.

De väckta behöver aldrig \provasinmoralx{}.
De kan inte återfå \KP{} och somnar in igen när deras \KP{} tar slut.

\REM{Du kan inte väcka redan odöda/vandöda.}
