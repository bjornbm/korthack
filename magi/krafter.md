\REM{ # Magiska krafter }

\cardNN{magi/Blixtladdning}
\cardNN{magi/De hatande döda}
\cardNN{magi/Dödens blod}
\cardNN{magi/Falsk vänskap}
\cardNN{magi/Flammande intet}
\cardNN{magi/Illusion}
\cardNN{magi/Insektssvärm}
\cardNN{magi/Ljus}
\cardNN{magi/Morbid marionett}
\cardNN{magi/Osynlighet}
\cardNN{magi/Sköld}
\cardNN{magi/Sömn}
\cardNN{magi/Åskvigg}

\NOcard{magi/Fruktan}
\NOcard{magi/Förvirra}
\NOcard{magi/Förtrollad sömn} \REM{ Sleep }
\NOcard{magi/Förtrolla}       \REM{ Charm }
\NOcard{magi/Förtrolla}       \REM{ Dancing lights }
\NOcard{magi/Paralysera}
\NOcard{magi/Förslag}         \REM{ Suggestion }
\NOcard{magi/Mörker}

\REM{ Hold portal }
\REM{ Knock }
\REM{ Light }
\REM{ Cure?? }
