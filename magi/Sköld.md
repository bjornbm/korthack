Upp till \HVD{} varelser \narax{} dig skyddas av en magiskt kraftfält i \HVD{} \rundorx{}.
Kraftfältet minskar \skadornax{} varje varelse får med upp till \HVD{} varje \runda{}.

\REM{ Nerfad:
En varelse du rör vid skyddas av en magiskt kraftfält i \HVD{} \rundorx{}.
Kraftfältet minskar \skadornax{} varelsen får med upp till \HVD{} varje \runda{}.
}

Till skillnad från mundana \rustningarx{} och \skoldar{} så kan kraftfältet minska \skada{} från \attackerx{} till noll.
