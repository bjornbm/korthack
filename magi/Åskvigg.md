Du kan skjuta en blixt ur ett metallföremål du håller i.
Blixten träffar de upp till \HVD{} närmaste varelserna inom synhåll framför föremålet och var och en av dem får \sHVD{} \skadax{}.
\REM{Slå för var och en eller får alla samma skada?}
