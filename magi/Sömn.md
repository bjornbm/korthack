Du kan få upp till \sHVD{}+\HVD{} \REM{valfria} varelser inom synhåll att falla i djup sömn.
De sover normalt djupt i \HVD{} timmar men kan väckas av hårdhänt behandling redan efter \HVD{} \rundorx{}.

Varelserna får \provax{} \HVD{} för att motstå besvärjelsen.
Även om de lyckas så är de sömniga och får \nackdel{} på sitt nästa \drag{}.

Besvärjelsen påverkar inte varelser för vilka sömn inte är ett naturligt tillstånd.
