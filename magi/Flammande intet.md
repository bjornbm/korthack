Du kan tillfälligt förvandla en oorganisk yta på upp till \HVD{} kvadratmeter till ett svart intet ur vilket det flammar heta gröna lågor.
Du måste vara \narax{} ytan och
förvandlingen varar i \HVD{} \rundorx{}.
Om någon bränns av lågorna får hen \sHVD{} \skadax{}.
