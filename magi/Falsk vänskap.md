Du kan få \HVD{} intelligenta varelser med \HVD{} lika med eller lägre än du att betrakta dig som deras goda vän.
De villiga att hjälpa och försvara dig bäst de kan även om det medför betydande risk eller kostnad för dem.
För att de ska handla i konflikt med sina naturliga intressen och/eller vänner behöver de dock övertalas\REM{ (\provax{\UTS})}.

Efter varje dygnsvila \ex{eller dygn för varelser som inte vilar} samt varje gång de ser dig uppenbart handla mot deras naturliga intressen och/eller vänner så får de \prova{\HVD} för att frigöra sig från förtrollningen
De kommer då att förstå att de manipulerats och förmodligen inte vara vänligt inställda.
