Skalbaggar, tusenfotingar och andra insekter kryper i tusental ut ur sprickor och springor på en valfri plats inom synhåll \REM{eller \narax{} dig?} och svärmar över fiender \narax{} platsen i \HVD{} \rundorx{}.

Varje \runda{} får dessa fiender får 1 i \skadax{} och måste \provax{\HVD} för att inte bli desorienterade och förlora sitt \drag{}. \REM{Eller få \nackdel{}?}
