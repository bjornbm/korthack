Att klättra är en \utmaningx{} vars \KP{} representerar sträckan du ska klättra (1 \KP{} motsvarar cirka 2 meter).
Om klättringen är svår minskar \skadanx{} du gör.
Varje \tungtx{} föremål du bär minskar \skadan{} du gör med 1.
\Prövax{} \KRP{}:

* \K{} du faller och tar \skada{} motsvarande förbrukade \KP{} (eller återstånde \KP{} om du klättrar nedåt).
* \M{} du vågar inte fortsätta; stanna eller börja om.
* \L{} du utför \KRP{} \skada{}.
* \E{} du utför \dubbelt{\KRP} \skada{}.

Hammare, pitonger/spikar och rep ger tillsammans \fördelx{} till en förstaklättrare. Rep ger \fördel{} till efterföljande.

\REM{Har man väldigt bråttom kan man \satsax{} och/eller använda \flytx{}.}
