Någorlunda homogena grupper av fiender kan hanteras som enheter. Till exempel så kan _Enhet A_ bestå av Vätte 1, Vätte 2 och Vätte 3. Enhetens medlemmar rör sig ihop och befinner sig i samma \omradex{} (men kan delas upp i mindre enheter vid behov).

Du får inte välja vilken specifik fiende i en enhet du \attackerarx{}, utan första \attacken{} mot Enhet A i en \rundax{} sker alltid mot Vätte 1, andra \attacken{} mot Vätte 2, och så vidare.

När alla \SLP{} i en enhet behöver \provasinmoralx{} så görs endast ett tärningsslag för hela enheten. \REM{Misslyckas slaget så flyr/kapitulerar/ordervägrar hela enheten.}

\cred{Halberds & Helmets}
