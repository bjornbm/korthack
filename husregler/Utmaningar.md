Du övervinner en fiende i strid genom att \förbrukax{} alla hens \KP{} med \skada{}.
Din \skada{} är lika med din \KRP{} men kan ökas av \vapenx{} och/eller reduceras om fienden bär \rustningx{}.

Andra \intro{utmaningar} kan representeras på ett likande sätt:
en utmaning ges ett antal \KP{} och övervinns när dessa \förbrukats{}.
Den ”\skada{}” en av dina \handlingar{} gör på utmaningen är lika med din mest relevanta \GE{}.
Väldigt krävande utmaningar kan reducera \skadan{}.
Lämplig utrustning kan öka \skadan{}.

Att flytta en hög med sand kan vara en utmaning med 4 \KP{}.
Att flytta en stor sten kan vara en utmaning med 1 \KP{} men ge \minus{3} på \skadan{}.
En spade eller hävstång kan ge +1 på \skadan{}.

