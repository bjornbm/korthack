Blåsrör kan använda mot mål på \intro{nära} avstånd.
Pilar från blåsrör gör ingen \skadax{} i sig utan är endast medel för att transportera \xrr{gifter}.
En blåsrörspil måste dock ändå penetrera målets \rustningx{} med \sKRP{} för att giftet ska kunna verka.
