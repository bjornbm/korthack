När du slåss med ett \vapenx{} i varje hand väljer du inför varje \attackx{} vilket \vapen{} du använder.
Din \attack{} får +1 på \skadanx{}.

\REM Detta kan läggas till Strid om vill göra dubbla vapen "officiellt":
Om du slåss med två \vapen{} ger dina attacker +1 i \skada{}.
}
