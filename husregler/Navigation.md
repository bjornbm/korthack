Att navigera är en \utmaningx{} vars \KP{} representerar sträckan du ska navigera (1 \KP{} motsvarar \timmarsx{} marsch).
\Prövax{} \HVD{}:

* \K{} du går vilse.
* \M{} du tar en omväg eller stöter på ett hinder.
* \L{} du utför \HVD{} \skadax{}.
* \E{} du utför \dubbelt{\HVD} \skada{}.

Mörker eller dålig sikt ger \nackdelx{}.
Karta, kompass, eller god vägbeskrivning ger \fördel{}.
Har du färdats sträckan förut under liknanade förhållanden så lyckas du automatiskt.

\REM{Har man väldigt bråttom kan man \satsax{} och/eller använda \flytx{}.}
