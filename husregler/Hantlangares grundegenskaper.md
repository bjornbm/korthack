När \hantlangarex{} ökar sina \nivåerx{} så höjer de sina \GE{} i cykler i ordningen \UTS{}\ra{}\HVD{}\ra{}\KRP{}\ra{}\UTS{}….

En \tjänare{} börjar cykeln med att höja \UTS{}.

En \vägvisare{} börjar cykeln med att höja \HVD{}.

Ett \hyrsvärd{} börjar cykeln med att höja \KRP{}.

En \specialist{} höjer alltid den \GE{} som är mest relevant för hens specialisering.

En \följeslagarex{} höjer den \GE{} deras mentor önskar.
