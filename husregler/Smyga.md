Att smyga är en \utmaningx{} vars \KP{} representerar sträckan du ska smyga (en abstraktion för skyddade positioner som du måste smyga emellan).
\Prövax{} \UTS{}:

* \K{} du röjer dig.
* \M{} du vågar inte fortsätta; stanna eller börja om.
* \L{} du utför \UTS{} \skadax{}.
* \E{} du utför \dubbelt{\UTS} \skada{}.

\Öppna{} \områdenx{} ger \nackdelx{}.
Ouppmärksamma fiender ger \fördel{}.
Du kan inte smyga med \tung{} \rustningx{}.

\REM{Har man väldigt bråttom kan man \satsax{} och/eller använda \flytx{}.}
