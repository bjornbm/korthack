En \hantlangarex{} ”bränner” sina ihoptjänade silvermynt snarast möjligt.

En \följeslagarex{} spar silvermynt till mat och förbrukningsvaror för nästa expedition.
\Slåförx{\UTS} för att övertala en \följeslagare{} att köpa annan \xr{utrustning}{prislista}.
Resten av hens silvermynt bränner hen.
