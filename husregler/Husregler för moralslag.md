När din \hantlangarex{} \slårförmoralx{}, välj \xr{tärning}{prövning} efter hur bra du är på att \övertalax{}.

Allmänt kan en ledares \övertala{} användas vid \moralslag{}.

I de få fall när du själv måste \slåförmoral{} får du också slå med \övertala{}. Det representerar hur väl du lyckas övertyga dig själva att din rädsla är irrationell.
