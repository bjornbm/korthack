Vid attacker där syftet inte är att skada, till exempel en duell eller turnering där oslipade vapen eller flatsidor används, brottning, eller boxning med handskar, markeras alla förbrukade \KP{} med \checkS{} och överflödande \skadax{} ersätter _inte_ markeringar med \checkD{}.

