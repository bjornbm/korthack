Många gifter ger \skadax{}.
\Provax{\KRP} för att motstå ett gift\REM{ görs en gång per förgiftning};
om du lyckas så får du minska dess \skada{} med \KRP{}.

*   En giftig gas ger kanske \D8 \skada{} varje \rundax{} du befinner dig i den.
*   Ett förtärt gift ger kanske \D4 \skada{} varje timme i \D6 timmar efter att du fått i dig det.
*   Nervgiftet från en förgiftad pil ger kanske \D6 \skada{} varje \runda{} i \D4 \rundor{} efter att pilen träffat dig.

Andra gifter kan ha andra effekter än att skada dig. Till exempel så kan de vara sövande, förvirrande, eller paralyserande.

\REM{ Giftet skulle kunna ges en "styrka" (utöver dess verkan??), och istället för en prövning kan det bli en tävling mellan RPs TÅL och giftets styrka. Men det verkan onödigt komplicerat. }

