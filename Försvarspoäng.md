Du har \KRP{} \introA{försvarspoäng}{fp} som kan användas för att undvika, avvärja eller uthärda \attackerx{}.

När dina \FP{} tar slut är du för utmattad, omtöcknad eller utmanövrerad för att kunna försvara dig effektivt och riskerar att få \xrr{livshotande skador} om du \attackeras{}.
\REM{
Du återfår vanligen dina \introFP{} genom att dricka vatten och vila i några minuter.
Du återfår vanligen dina \introFP{} genom några minuters vätske- och vilopaus.
I de flesta fall återfår du dina \introFP{} när du dricker vatten och vilar i några minuter.
Du återfår vanligen dina \introFP{} när du dricker vatten och vilar i några minuter.
Dina \introFP{} återfås vanligtvis av lite vatten och några minuters vila.
}

\Include{Döden}


\FANTASY{
\REM{
\noindent
Normalt så kvicknar du till \REM{om utslagen} och/eller återfår dina \FP{} genom att dricka vatten och vila i några minuter.

\noindent
Normalt kvicknar du till \REM{om utslagen} och återfår dina \FP{} genom att dricka vatten och vila en kvart.
}
Normalt så kvicknar du till \REM{om du är utslagen} och/eller återfår dina \FP{} efter att ha druckit vatten och hämtat andan \REM{vilat} i några minuter.
}


\SCIFI{
\noindent
Tabellen i ***Mothership*** sidan 10.4 avgör hur snart du kvicknar till.

Du återfår \FP{} genom dygnsvila
(\slafor{\KRP} och återfå antalet \traffar{}).

När du får \intro{första hjälpen} av någon får hen \slafor{\HVD}. Du återfår lika många \FP{} som antalet \traffarx{}.

En *Vaccsuit* (\tungx{}) eller en *Standard Battle Dress* ger dig en extra \FP{}. En *Advanced Battle Dress* (\tungx{}) ger dig två. \FP{} från dräkter återfås omedelbart efter \strider{}.
}


\TODO{Måste förtydliga vad utslagen betyder. Hur länge? Väckning? Läkning? Första hjälpen?}
