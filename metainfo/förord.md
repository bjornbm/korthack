# Förord

Det här spelet är en produkt av:

*   önskan om en enkel men "spelig" och intressant mekanik
*   en fix idé att varje koncept ska kunna beskrivas på ett \textsc{a7} registerkort
*   ambitionen att kunna driva ett öppet spelbord med minimal friktion för alla inblandade

Designmål utöver de som är implicita från ovan är:

*   lättpresenterade regler med få rörliga delar
*   snabbt karaktärskapande med få men betydelsefulla beslut
*   erfarenhetspoäng och nivåbaserad förbättring
*   strider med intressanta taktiska val och få tärningsslag

