[ ] Credits
[ ] K26 publishing?
[ ] Name? Korthack är rätt trist
[ ] Artwork
    [ ] Framsida/omslag
    [ ] Baksidor av kort
        - en art piece per A4, korten kan pusslas ihop Garbage Pail Kids-style?
        - kan vara samma artpiece som omslaget?
        - mer använbart med tom baksida för att kunna skriva husregler?

# Proofing

[ ] Kolla all stavning
[ ] Kolla alla avstavningar
[ ] Be någon korrekturläsa
    - grammatik
    - stavning

# Kod

[ ] Få wiki-regler att funka
[ ] Städa bort redundanta definitioner i gpp
[ ] Städa bort redundans i rf.md

# Världsbygge

[ ] Gemensam "cause" för RP annat än pengar och desperation
    - Jobbar i samma organisation?
        - acolyter a la Dark Heresy?
        - kan förklara "career paths"

# Rollpersoner

[ ] Världsneutral bakgrund?

# Resurser

[ ] SLP-generator
    [ ] AI: agenda och instinkt (2018_06_18_1420)
    [ ] Individers motiv
        - Imponera på sin ledare
        - Ersätta sin ledare
        - Rikedom
        - Kunskap
        - Personlig säkerhet
        - Imponera på en gud
        – …
        - Relaterat till värderingar?

[ ] Rumgenerator

[ ] Monster
    [ ] Riktlinjer för att konvertera från ODD m fl?
    [ ] Riktlinjer för att improvisera arketyper ("use the stats for bear")
    [ ] Sno monster?
        [ ] Vättar från Beyond the Wall (wicked ones)
        [ ] Odöda från Beyond the Wall/Veil
        [ ] drakar, demoner (generators) från Beyond the Wall/Veil?
        [ ] Demoner från Warband
        [ ] Faktioner från Warband

[ ] Pregens
    [ ] uppdatera kod för ny startutrustning etc
