# Definition

1.  What is the game “about”?

Det här spelet handlar om desperata äventyrare som samarbetar för att överleva när de utför expeditioner till världens mörkaste vrår i strävan att öka sin "kraft" (i form av bättre utrustning och högre nivåer).
För att lyckas måste de hantera resurser stragiskt och handla taktiskt i snabba strider.

2.  How is it about that?

Äventyrarna börjar så svaga och illa utrustade att de knappast kan överleva utan samarbete.
Regelmekaniken ger utrymme för intressanta taktiska beslut trots att det är lättöverskådligt och kräver få tärningsslag.

3.  What reward structure exists to encourage behaviour toward that goal?

Framgångar som torde öka en äventyrares anseende bland sina geliker (utforskning, överlevnad, segrar, …) belönas med erfarenhetspoäng som i förlängningen leder till nivåökningar.
Äventyrarnas "kraft" ökar drastiskt med nivåökning och bättre utrustning (som kan hittas eller köpas med hjälp av skatter).



# Spelet

## Mål

Att utforska grottor och finna skatter

## Motivation

Erfarenhetspoäng, ära, förbättring.

## Narrativa element

### Story

Byggs av spelarna, men handlar om hur de riskerar sina liv i jakt på belöningar.

### Setting

### Teman

Samarbete, risk vs reward, resurshantering, utforskning.

### Ton

Fara, pushing luck, svaga och dåligt utrustade, mörker, hopp.

