Idéer har stulits från eller inspirerats av bland andra:

*   Halberds & Helmets\sec{sår}
    \ex{<https://tinyurl.com/halberdshelmets>} \REM{<https://alexschroeder.ch/wiki/Halberds_and_Helmets>}
*   Index Card RPG\sec{tid-och-turordning
                 }\sec{grundegenskapsslag \REM{övning ger färdighet}
                 }
    \ex{<https://www.icrpg.com>}
\REM{*   Into the Odd\sec{strid}
    \ex{<https://www.drivethrurpg.com/product/145536>}
}*   Maze Rats\sec{stridsmoral}
    \ex{<https://www.drivethrurpg.com/product/197158>} \REM{<https://tinyurl.com/mazerats>}
*   Old School Hack\sec{utrustning}\REM{\sec{rustningar-och-sköldar}\sec{områden}\sec{vapen}}
    \ex{<http://www.oldschoolhack.net>}
*   The Book of Romance (1902) \ex{illustrationer av Henry J Ford}
*   Torchbearer\sec{belysning}
    \ex{<https://www.torchbearerrpg.com>}
*   <https://www.fantasynamegenerators.com>\sec{namn}\REM{vättenamn,barbarnamn}

\REM{
*   Mutiné\sec{satsa}
    \ex{<https://tinyurl.com/mutantmatine>} \REM{https://www.urverkspel.com/component/jdownloads/category/7-mutantmatine}
}
