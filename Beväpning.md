En \intro{rustning} ger dig en extra \introFP{}. En \intro{\tungx{} \termI{rustning}} \ex{omöjlig att smyga, simma eller sova i} ger dig två.

Med en \intro{sköld} kan du ge \nackdelx{} till upp till \KRP{} \attacker{} du ser komma per \rundax{}.

\intro{Kastvapen} kan användas mot mål på \keyI{nära} avstånd, \intro{skjutvapen} även på \keyI{långt}.
Ingetdera kan användas i närkamp. \REM{Avståndsvapen kan inte användas i närkamp.}

\intro{Långa vapen} \ex{spjut, stångyxor …} används med två händer och kan användas från andra ledet i en formation.

\intro{\Tungax{} vapen} \ex{långsvärd, bredyxor …} används med två händer och
låter dig räkna varje \traff{} dubbelt.

Med ett \intro{improviserat vapen} får du varken \foljaupp{} \REM{\attacker{}} eller göra \motattacker{}.
Om du är \intro{obeväpnad} gäller detsamma samt att väpnade närstrids\attacker{} mot dig får \fordel{}.


\REM{

\intro{två vapen}

\intro{automatvapen}

\intro{granater}

}
