De flesta \SLP{} måste \slafor{\HVD},
eller \REM{för} sin ledares \UTS{} om hen är närvarande,
när de utsätts för mer fara än de förväntat sig. Till exempel när de

* ser en vän falla i strid.
* förlorar hälften av sina \FP{}.
* beordras ta allvarliga risker.
* angrips av något övernaturligt.

\noindent
Om de \misslyckas{} så retirerar, kapitulerar, ordervägrar eller flyr de.

\REM{I en konflikt slår en \SLP{} för moral med \fordel{} om hens sida förefaller vara överlägsen\REM{ motståndarna}.}

\REM{ Nackdel behöver inte skrivas explicit. SLP som lyckas med sitt moralslag är ändå rationella, så om de ligger uppenbart illa till kommer da att försöka fly eller förhandla istället för att strida in i det sista. }

\cred{ Maze Rats }
