\FANTASY

En ljuskälla ger \intro{fullgott ljus} åt ett antal närbelägna karaktärer \ex{inklusive bäraren} och \intro{svagt ljus} till andra.
I \termI{svagt ljus} får du \nackdelx{} på allt som kräver god syn, inklusive \attackerx{}.
Om du slåss i \intro{mörker} så måste du dessutom först \slafor{\HVD} för att lokalisera ditt mål.

Ett \intro{vaxljus} ger \fullgottljus{} åt bäraren och \svagtljus{} på några meters håll.

En \intro{fackla} ger \fullgottljus{} åt två personer och \svagtljus{} på \narax{} håll.

En \intro{oljelykta} ger \fullgottljus{} åt tre personer och \svagtljus{} på \nara{} håll.

En \intro{riktad lykta} ger \fullgottljus{} åt bäraren och svagt ljus på \langt{} håll i en riktning.


\cred{Torchbearer för antal personer och svagt ljus?}
