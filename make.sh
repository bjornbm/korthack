#!/bin/bash

set -o nounset
set -o errexit
#set -o verbose


function usage {
  echo $"Usage: $0 yaml-file jobname [{1L|2P|4L|8P|9L|16L|25L|RF}] [pages]"
  exit 1
}

if [ "$#" -lt 2 ] || [ "$#" -gt 4 ]
  then
    usage
fi

mkdir -p "${WORKDIR:=tmp}"
mkdir -p "${OUTDIR:=out}"

gpp="gpp -x -T --nostdinc -I. --include latex.gpp"

pandoc="\
    pandoc \
    -Vlang=sv-SE \
    -Fpandoc-crossref \
    -MautoSectionLabels=True \
    -Hheader.tex \
    -Vmathfont=STIX2Math.otf \
    -Vtables \
    --to=latex \
    "

jobname=$(basename -s .md "$2")
jobdir=$(dirname "$2")
jobfile="${jobdir}/${jobname}.md"

# Make sure job file exists.
if [ ! -r "$jobfile" ]
  then
    echo "Error: input file '$jobfile' does not exist."
    usage
fi

yaml="$1"
dobreak="no"
#genre="scifi"
genre="${GENRE:-scifi}"

$gpp -Ddobreak="$dobreak" -Dgenre="$genre" "$jobfile" | $pandoc --metadata-file="$yaml" > "$WORKDIR/$jobname.tex"
tectonic --print "$WORKDIR/$jobname.tex"


COMBINATION=${3:-}
if [ -n "$COMBINATION" ]
  then  # Combine pages.

    # Make sure chosen combination is valid.
    if [ ! -r "combine/combine$COMBINATION.tex" ]
      then
        usage
    fi

    PAGES=${4:-"1-"}

    cp "$WORKDIR/$jobname.pdf" "$WORKDIR/combine.pdf"
    gpp -T -DJOBNAME="$jobname" -DPAGES="$PAGES" "combine/combine${COMBINATION}.tex" > "$WORKDIR/combine.tex"
    tectonic --print "$WORKDIR/combine.tex"

    datedpdf="$OUTDIR/$(date +%Y-%m-%d) $jobname $COMBINATION.pdf"
    cp "$WORKDIR/combine.pdf" "$datedpdf"
    open "$datedpdf"

  else  # Don't combine pages (default).

    datedpdf="$OUTDIR/$(date +%Y-%m-%d) $jobname.pdf"
    cp "$WORKDIR/$jobname.pdf" "$datedpdf"
    open "$datedpdf"

fi
