\REM{---
title:  Korthack
author: Björn Buckwalter
date: 2018-10
...}


\setcounter{section}{100}

\REM{# Spelledarresurser}

\card{resurser/Introduktion för spelledaren}
\card{resurser/Spelledarens tur}
\card{resurser/Tips till spelledaren}
\card{resurser/Värderingar}
\card{resurser/Svagheter}
\card{resurser/Yrken}
\NOcard{resurser/Adjudicering}
\NOcard{resurser/Kapitalt misslyckade attacker}
\NOcard{resurser/Odds för prövningar}

\pagebreak

# husregler

\NOcard{husregler/Icke-dödligt våld}
\NOcard{husregler/Utmaningar}
\NOcard{husregler/Fasthållning}
\NOcard{husregler/Hantlangares grundegenskaper}
\NOcard{husregler/Hantlangares silvermynt}
\card{husregler/Grupper av fiender}
\NOcard{husregler/Återvinning av pilar}
\NOcard{husregler/Slungstenar}
\card{husregler/Gifter}
\card{husregler/Blåsrör}
\card{husregler/Dubbla vapen}
\NOcard{husregler/Klättring}
\NOcard{husregler/Navigation}
\NOcard{husregler/Smyga}

\pagebreak

# Mötesprotokoll

\card{resurser/Mötesprotokoll}


\pagebreak

# Monster

\card{resurser/Fiendeklasser}
\card{resurser/Vättenamn}
\card{resurser/Vättar}


# Teratic monsters

1.  they are inhuman,
2.  they have a place in the world,
3.  they are unique or close to it,
4.  it is possible to identify them even in their absence.

Hit all four points, and even (mechanically standard) goblins and zombies can be teratic.

<http://www.kjd-imc.org/blog/teratic-monster-design/>


# Johnn Four's bullseye method

1.  take something global, like goblins,
2.  figure out how they are unique in the region,
3.  figure out how that regional variant is even more unique locally.

<http://www.kjd-imc.org/blog/teratic-monster-design/#comment-7001>


\pagebreak

# Dungeons

\card{resurser/Äventyrsmall}
\card{resurser/Kartgenerator}
\card{resurser/Rumsgenerator}
\card{resurser/Skatter}
\card{resurser/Slumpmötestabell}


\REM{ # Spelledartips

# Timers

# LOG Location Obstacle Goal

# TTT Timer Threat Treat

# Adjucating actions

# Murky mirror

# Monster AI (GMGrizzly)

}

