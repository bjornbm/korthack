Att vara \intro{sårad} medför inga omedelbara konsekvenser men om du blir \intro{sårad} en andra gång så måste du \slaforx{\KRP}. 
Om du \misslyckas{} så dör du,
annars förlorar du medvetandet i någon timme\REM{och är därefter \intro{sårad}}.
En \intro{hjälm} ger dig \fordel{} på slaget.

\REM{En \intro{sköld} låter dig undvika att bli \intro{sårad} en gång per \stridx{}.}
En \intro{sköld} låter dig undvika att bli \keyI{sårad} av en 
\attackx{} per \rundax{}.


\REM{Du kan inte smyga, simma eller sova \REM{och får \nackdel{} när du springer, hoppar eller klättrar} iklädd rustning.}
\REM{Du kan dock inte smyga, simma eller sova iklädd rustning.}

\REM{
\Slafor{\KRP} första gången du får tillfälle att tvätta och förbinda dina sår.
Om du \lyckas{} visar det sig att de inte var särskilt allvarliga och du är inte längre \keyI{sårad}.
Annars krävs det ordentlig vård och vila för att såren ska läka.
}

\TODO{
Är det roligare att slå för HVD istället för att det är KRP igen? Då blir det effektiv första hjälpen som tar hand om såren snarare än fysisk ”hårdhet”. Är det kul att HVD blir lite eftertraktat/uppskattat, och att det inte bara är KRP som avgör hur bra man mår i det långa loppet? Eller blir krigarna för nerfade om de inte läker snabbt och lätt?

KRP (och hjälm) håller dem ju ändå levande om de blir utslagna. Med KRP 3 och hjälm så är de ju "odödliga" sånär som på TPK och/eller coup de grace!

I ett team går det förhoppningsvis på ett ut, men en ensam ensidig krigare blir skörare.
}

\Slafor{\HVD} när du är den förste som tvättar och förbinder någons sår.
Om du \lyckas{} så är hen inte längre \keyI{sårad}, annars behöver hen ordentlig vila och vård för att läka.

