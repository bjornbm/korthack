\Sl{} bör gå igenom alla punkterna den här listan på hens tur.

1.  \SLP \provarsinmoralx{} om nödvändigt. Fast om det är rationellt så strunta i tärningsslaget och fly rakt av!

2.  Uppdatera aktiva timers.

3.  \xr{Handla}{tid-och-turordning} med \SLP{}.

4.  Introducera nya timers.

5.  Bokför förfluten \xr{tid}{tid-och-turordning} i äventyret.
