\TODO{ Behöver ett bättre namn? "[Personality] Flaws" }

Välj ett föga smickrande karaktärsdrag till din \RP{}.
Kommer det att sätta henom i klistret?
Kommer hen att kunna bättra sig?
\
\columnTwo

1.  arrogant
1.  envis
1.  fanatisk
1.  feg
1.  fåfäng
1.  fördömande
1.  girig
1.  grym
1.  hämndlysten
1.  lat

\next{\columnTwo}

11. misstänksam
1.  naiv
1.  självisk
1.  självsäker
1.  självupptagen
1.  skrytsam
1.  snål
1.  stolt
1.  temperamentsfull
1.  vårdslös

\columnend

\REM{
svekfull/oärlig
högmodig
lat
orättvis/godtycklig
egoistisk (självisk)
avhängig/klängig?
glupsk
lättlurad (naiv?)
hedonist
humorlös
fördomsfull
avundsjuk
illoyal
tankspridd
obildad
lögnaktig
lögnare
pratkvarn
}
