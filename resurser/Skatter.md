Slå för både \smsymbol{} och föremål;
smulor: \D4,
rov: 2 \D6,
gömma: 3 \D8,
kista: 4 \D10 + \D12,
skatt: 5 \D12 + \D20,
drakskatt: 10 \D20.

|    |                                |    |                                  |
|---:|--------------------------------|---:|----------------------------------|
|  1 | \D6 förbrukningsvaror          | 11 | \Tung{} \rustningx
|  2 | Småsak                         | 12 | \Mkttungt{} \vapenx
|  3 | Verktyg                        | 13 | \Tung{} \skoldx
|  4 | \Vapenx                        | 14 | Smycke/ädelsten (\sm{100})
|  5 | \Skoldx                        | 15 | \Mkttung{} \rustningx
|  6 | \Hjalmx                        | 16 | \Besvarjelsex
|  7 | Smycke/ädelsten (\sm{50})      | 17 | Mystiskt föremål
|  8 | Lyxartikel                     | 18 | Mystiskt vapen
|  9 | \Tungt{} \vapenx               | 19 | Kronjuvel (\sm{1000})
| 10 | \Rustningx                     | 20 | Kungakrona (ovärderlig)

\TODO{
*   Minska/ta bort mystiken
*   Sortera om enligt uppdaterad prislista
*   Gör om till pipe table
*   Referenser till vapen, rustningar, etc.
}
