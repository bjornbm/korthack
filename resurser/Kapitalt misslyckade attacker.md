\Sl{} hittar på, väljer eller slumpar en bieffekt när du \misslyckaskapitaltx{} med en attack.

1.  Du fälls/faller omkull.
2.  Du blottar dig (din motståndare får en \gratisattackx{}).
3.  Du överanstränger dig (markera en \KP{}).
4.  Du desarmeras/tappar ditt vapen.
5.  Du tappar något annat än ditt vapen.
6.  Du träffar en av dina vänner.
\REM{7.  Du slår sönder något viktigt.}
\REM{8.  Du kommer ur balans och missar din nästa \handlingx{}.}

Om din attack \lyckasexceptionelltbra{} så gör den normalt dubbel \skadax{}, men du kan föreslå något annan effekt om du vill!
