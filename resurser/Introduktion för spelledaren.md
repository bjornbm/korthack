Det här spelet innehåller ingen utförlig introduktion till rollspel utan det förutsätts att du som \SL{} har tidigare erfarenhet av (traditionella) rollspel och kan förklara hur de går till för nya spelare.

Reglerbeskrivningarna riktar sig mot spelarna och pronomenet ”du” syftar därför på spelaren och/eller hens \RP{}.
Generellt gäller samma regler för \SLP{}.

Du kommer att behöva extrapolera spelets regler när de är otillräckliga för de situationer som uppstår i ert spel.
\REM{Detta är normalt och väntat.}

