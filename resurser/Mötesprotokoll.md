# Spejare

\Rp{} designerar en spejare som \provarx{} \HVD{} när det är dags att kolla slumpmöten.
Aktiv spaning (utskickad i förväg?) ger \fordel{}.
Oväsen eller oförsiktighet med \belysningx{} ger \nackdel{}.

-   \K{} \RP{} blir överrumplade, deras motpart handlar först och \RP{} har \nackdel{} första \rundanx{}
-   \M{} ett möte förebådas/inträffar om det redan förebådats
-   \L{} \RP{} drar inte till sig någon uppmärksamhet \REM{inget slumpmöte}
-   \E{} \RP{} upptäcker motparten först och kan välja att undvika eller överraska den


# Slumpa motpart

Slå på dedikerad slumpmötestabell.
Normalt slås \D6 med +2 i farliga omständigheter/nattetid.
Exempel:

1.  Skogshuggare (\D8+2)
2.  Patrullerande soldater (\D8+4)
3.  Nomader (\D12+6)
4.  Häxa
5.  Vargar (\D8+1)
6.  Vättespejare (\D6 ridandes på vargar)
7.  Vättar (\D20)
8.  Vättestormtrupper (\D8+4)

Bestäm vad motparten sysslar med om det inte är givet.
Exempel:

Ouppmärksam       Inaktiv       Aktiv              Uppmärksam
----------------- ------------- ------------------ -----------------
festar            vilar         för hem loot       ligger i bakhåll
käbblar           äter          för hem fångar     patrullerar
slåss (mot vem?)  campar        på väg någonstans  rekognoserar
flyr (från vad?)  slickar sår   jagar/samlar       vaktar

Bestäm avståndet (i \rundor{} eller \KP{}---se \xrr{smyga}) till motparten med en \D6 (1 = \narax{})

# Mötesprotokoll

Efter att ett möte slumpats fram, bestäm förhållanden för mötet:
\REM{1.  Slumpa fram ett möte. \REM{Slå för möten enligt instruktioner och/eller tabeller för den aktuella platsen och tiden.
Fortsätt nedan när ett möte och \RP{}s motparten bestämts.}}

1.  Bestäm avståndet till motparten med en \D6 \ex{+1 i vildmark};

    \hfill{}1-2: \narax{},
    \hfill{}3–5: \langt{},
    \hfill{}6+: fjärran.
    \hfill{} 

2.  Låt den mest uppmärksamme \RP{} \provax{\HVD} för att de ska få börja \turordningenx{};
    annars börjar motparten.

3.  Slå för hur den \xr{reagerar}{reaktion} på \RP{}
    och \provadessmoralx{} för att avgöra om den agerar dominant eller undergivet.

\REM{4.  En \xrr{reaktion} på ≤4 försämras generellt om \RP{} inte är tillmötesgående eller \xr{förhandlar}{förhandling} väl.}

4.  \Rp{} kan försöka förbättra motpartens inställning genom att \xr{förhandla}{förhandling}.
    En \termm{reaktion} på ≤4 försämras generellt om \RP{} inte är tillmötesgående eller \term{förhandlar}{förhandling} väl.


# Reaktion \REM{Inställning?}

Om den inte är given bestäms motpartens inställning till \RP{} av en \D6 med +2 för nominellt allierade och \minus{2} för naturliga fiender.

|    |           |                                                           |
|---:|-----------|-----------------------------------------------------------|
| ≤1 | Fientlig  |\ex{angriper \RP{} omedelbart _eller_ flyr}                |
|  2 | Hotfull   |\ex{kräver något av \RP{} _eller_ avlägsnar sig}           |
|  3 | Ovänlig   |\ex{kräver att \RP{} avlägsnar sig _eller_ undviker \RP{}} |
|  4 | Skeptisk  |\ex{behöver övertygas om \RP{}s välvilja}                  |
|  5 | Likgiltig |\ex{tolererar \RP{}s närvaro}                              |
|  6 | Nyfiken   |\ex{intresserad av byteshandel med \RP{}}                  |
|  7 | Vänlig    |\ex{hjälper \RP{} om låg risk/kostnad}                     |
|  8 | Hjälpsam  |\ex{hjälper \RP{} även om betydande risk/kostnad}          |

\REM{
Motparten \provarsinmoralx{} och agerar därefter vid resultat ≤3.
}

\cred{moralslaget: Paul_T [[2019_02_03_1802]]}

\REM{ Behövs alla dessa steg eller kan det förenklas till fientlig, ovänlig, vänlig? Fientlig skulle betyda attack. Vänlig skulle betyda att man kan förhandla på civiliserat vis. }


# Förhandling

\Rp{} kan förbättra motpartens inställning genom att tillmötesgå eventuella krav och/eller genom förhandling eller övertalning.

Låt talmannen för \RP{} \provax{} \UTS{}, med \nackdel{} om det saknas ett gemensamt språk eller om \RP{} pratar i mun på varandra.
Om hen lyckas förbättras motpartens inställning \UTS{} steg.

Gåvor kan ge \fordel{}, ett fientligt förflutet ger \nackdel{}.

En skeptisk eller sämre inställning försämras automatiskt ett steg per \rundax{} om inte \RP{} lyckas förbättra den.

\cred{Eero [[2019_02_03_1802]]}

