--- ------------ ----------- ------------ ------------
  1 Oilug        Furbog      Xurukk       Morn
  2 Igurg        Uggug       Zurgug       Mog
  3 Sunodagh     Zulgha      Hugagug      Ugak
  4 Mudagog      Wapkagut    Urzog        Gulfim
  5 Shobob       Guthakug    Kahigig      Homraz
  6 Zugorim      Surgug      Gulm         Shagdub
  7 Lurog        Diggu       Gujarek      Ugor
  8 Pakgu        Xoroku      Vuiltag      Mazoga
  9 Vlog         Sogugh      Nag          Glob
 10 Agugh        Uram        Xorag        Agrob
 11 Rogan        Surbag      Braugh       Shazgob
 12 Trougha      Kurmbaga    Rakgu        Shadbak
--- ------------ ----------- ------------ ------------

\cred{ http://www.fantasynamegenerators.com/orc_names.php }
