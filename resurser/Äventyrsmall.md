1.  Bestäm var äventyret ska utspelas\sec{platsgenerator} och
    a.  välj/rita en inspirerande karta, eller
    b.  generera en karta\sec{kartgenerator}\sec{rumsgenerator}\sec{rumsfunktion} i realtid under spel.

2.  Bestäm vilken fraktion\sec{platsgenerator} som kontrollerar platsen.

3.  Bestäm vilken fraktion de är allierade med.

4.  Bestäm vilken fraktion som är deras rivaler.

5.  Bestäm vad platsen har för funktion\sec{platsgenerator} om det inte redan är uppenbart.

6.  Bestäm vad spelarna har för uppdrag eller ärende\sec{platsgenerator} dit.


\REM{

Bestäm vilken faktion som kontrollerar den, deras allierade, och deras rivaler. Bestäm dungeonens syfte om det inte redan är uppenbart.

Syfte

1.  Högkvarter    \REM{ kan vara huvudstad och samhälle }
2.  Samhälle      \REM{ by/stad, med "kvinnor och barn" och andra civila }
3.  Vaktpost      \REM{ skydda eller bevaka en gräns eller ett område, kalla på förstärkning }
4.  Militärläger  \REM{ samlingsplats för invasionshär eller basläger för räder }
5.  Förtrupp      \REM{ militär förtrupp för att spana/utföra räder/känna på motståndet }
6.  Utvinning     \REM{ odling/gruva för leverans av råvaror till högkvarter/samhälle }
7.  Prospektering \REM{ basläger för grupp som letar efter områden/resurser att expandera till }
8.  Helgedom      \REM{ begravningsplats/tempel/kloster/relikförvaring }
9.  Gömställe     \REM{ "Helm's klyfta" för försvar/flykt, skattgömma }
10. Fängelse      \REM{ fångläger/straffarbetslåger/infångade monster/harem }

\Rp{}s uppdrag

1.  Frita       \REM{ frita en eller flera fångar }
2.  Döda        \REM{ lönnmord av nyckelfigur i fraktionen }
3.  Fördriva    \REM{ driva ut fraktionen från platsen }
4.  Rekognosera \REM{ kolla vad de håller på med, antal, hur väl utrustade, planerar }
5.  Stjäla      \REM{ ta någonting (eller kidnappa någon?) från dungeonen }
6.  Förstöra    \REM{ förstöra platsen eller något inuti den }
7.  Stoppa      \REM{ hindra fraktionens plan, invasion, portalöppning, jordens undergång }
8.  Passera     \REM{ ta sig igenom på väg någon annanstans }
9.  Eskortera   \REM{ till eller genom platsen }
10. Förhandla   \REM{ fredsavtal, affärsuppgörelse, gisslanbyte }

Fraktion

1.  Banditer     \REM{ rövare, pirater, vildar }
2.  Kultister    \REM{ kannibaler, människooffrare, demondyrkare, domedagskult }
3.  Mystiker     \REM{ nekromantiker }
4.  Vättefolk    \REM{ vättar, orker, resar }
5.  Reptilfolk   \REM{ kobolder, ödlemän, djupingar }
6.  Odöda        \REM{ lägre, vampyr, lich }
7.  Fauna        \REM{ råttor, jättespindlar, vargar, grizzlybjörn, mörkmantlar, jätteorm, ointelligent fantastisk fauna }
8.  Monster      \REM{ drake, mantikora, varulvar, troll, intelligenta eller semiintelligenta }
9.  Neutral      \REM{ dvärgar, alver, kentaurer, arme, släkt, organisation, sekt }
10. Icke-jordisk \REM{ Great Old Ones, Outer Gods, demon, `extra-dimensionella besökare, mi-go }

}


# Platsgenerator

 \D10 Plats        Fraktion     Funktion       Uppdrag        
----- ------------ ------------ -------------- ---------------
  1   grotta       banditer     högkvarter     frita          
  2   ruin         kultister    samhälle       döda           
  3   grav         mystiker     vaktpost       fördriva       
  4   torn         vättefolk    militärläger   rekognosera    
  5   fort         reptilfolk   förtrupp       stjäla         
  6   helgedom     odöda        utvinning      förstöra       
  7   fängelse     fauna        prospektering  stoppa         
  8   (spök)hus    monster      helgedom       passera        
  9   samhälle     neutral      gömställe      eskortera      
 10   ickejordisk  ickejordisk  fängelse       förhandla      

