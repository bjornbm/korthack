För de flesta \SLP{} är det inte värt besväret att hålla reda på vilka sorters markeringar deras \resurserx{} fått eller att låta dem vila. Ett undantag är bossar som kan ta taktiska vilor, exempelvis medan deras underhuggare uppehåller \RP{}.

De flesta \SLP{} bör inte få använda \FP{} eller \IP{} och det kan vara enklare att till exempel låta en boss alltid göra två attacker på sin tur och/eller ha fördel i vissa situationer än att bokföra \FP{}/\IP{} för den.
Det ger också \RP{} ett mönster att lista ut och arbeta mot.

