\REM{

# Faktioner

1.  Vättar
2.  Odöda
4.  Rövare
3.  Äventyrare
5.  En trollkarl (med underhuggare)
6.  En stor best (mundan eller magisk)
7.  Orker (vargpack, ulvryttare, elitstyrkor/chocktrupper)
8.  Troll
9.  En demon
10. Kultister
11. Djupingar
12. Dvärgar (grenadjärer, musköter, chocktrupper, ingenjörer)
13. Kobolder
14. Lokal fauna
15. En minotaur
16. Kentaurer
17. Varulvar
18. Ödlemän ("vanlig" med spjut/harpun, shaman, hövding)
19. En drake
20. Vildar
xx. Råttmänn ("vanlig" med kniv och slunga, shaman, hövding)


# Faktioner

1.  Banditer/pirater
2.  Kultister
3.  Trollkarl
4.  Vättar
5.  Orker
6.  Ödlemän/kobolder/djupingar
7.  Råttmän/varulvar/kentaurer
9.  Fauna (samhälle eller individ)
10. Mytologiskt monster
11. Odöda
12. Kaos/demoner/mythos


}


Slå en \D20 när 6 timmar förflyter i vildmark eller en timme i bebodda områden.
I ett ”hett” område, slå en \D12.
\Rp{} stöter på…

|      |                                               |
|:----:|-----------------------------------------------|
|  1–2 | \Monster{} från den lokala faktionen          |
|   3  | \Monsterx{} från en grannfaktion              |
|   4  | Fauna eller äventyrare                        |
|  5–6 | Spår av \monster{} från den lokala faktionen  |
|   7  | Spår av \monster{} från en grannfaktion       |
|   8  | Spår av fauna eller äventyrare                |
|  ≥9  | Ingenting                                     |

\REM{
| \D20 | (spår)  | \Rp{} stöter ihop med (spår av)…          |
|:----:|:-------:|-------------------------------------------|
|  1–2 | (11–12) | \monsterx{} från den lokala faktionen     |
|   3  | (13)    | \monster{} från en grannfaktion           |
|   4  | (14)    | fauna eller äventyrare                    |
| 5–10 | (15–20) | ingenting                                 |
}

Spår kan vara fotavtryck, ljud, lukt, matrester, kasserad utrustning, spillning, kroppsvätskor, päls med mera.


# Monstergenerator

För faktioner bestående av mer än en individ, bestäm monstrens antal genom att slå en \D8.
I ett ”hett” område, slå en \D6.

------ ----------------------------------------------------------------------
   1   Här \ex{i princip oslagbar för \RP{}, +2 nivåer på reaktionstabellen}
 2-–3  Stor styrka \ex{stridsförband, +1 nivå på reaktionstabellen}
 4-–5  Liten styrka \ex{spejare, lönnmördare}
  ≥6   Ensam \ex{budbärare, civilist, \minus{2} nivåer på reaktionstabellen}
------ ----------------------------------------------------------------------

Bestäm sedan \xr{vad monstren sysslar med}{monstersysslor} när \RP{} stöter på dem samt \xr{hur de reagerar}{reaktionstabell} på \RP{}s närvaro.


# Vad sysslar monstren med? { #monstersysslor }

  \D20   Monstren…
-------- -------------------------------------------------------------------
    1    är på väg hem efter en strid \ex{minska resurser}
    2    slåss med ett annat monster \ex{slå fram ett till monster}
    3    är på väg hem med en fånge \ex{slå fram en fånge}
  4--5   är på väg hem med skatter \ex{slå fram skatt som i lyan}
  6--8   är förbipasserande på väg någonstans \ex{har en egen agenda}
  9--12  försvarar sitt territorium \ex{förband på jakt efter inkräktare}
 13--15  är på jakt efter mat \ex{tysta och på sin vakt}
 16--17  jagar ett annat monster \ex{slå fram ett svagare monster}
   18    flyr från ett annat monster \ex{slå fram ett starkare monster}
   19    bygger en ny lya \ex{gräver ett hål, sätter upp läger, högljudda}
   20    letar efter en plats att sova av sig festrus \ex{ouppmärksama}

\REM{3, 4, 9, 10 funkar ej riktigt med slumpmötestabellen… välj från annan faktion?}

\cred{ 1D8 – What are those wandering monsters up to? http://1d8.blogspot.se/2011/04/what-are-those-wandering-monsters-up-to.html }


# Reaktionstabell

  \D20   Monstren reagerar på \RP{} genom att (försöka)…
-------- ---------------------------------------------------------
    1    kapitulera/underkasta sig och erbjuda \RP{} sin loyalitet
    2    ge \RP{} ett föremål, information, eller annan hjälp
    3    genomföra omsesidigt gynnsam byteshandel med \RP{}
  4--5   missta \RP{} för vänner/allierade
  6--8   avvakta och låta \RP{} handla först
  9--12  retirera till en säkrare plats
 13--15  begära att \RP{} retirerar (lägg annars till 1\D6)
 16--17  kalla på förstärkningar och avvakta
   18    lura \RP{} enligt 2–4, sedan tillfångata dem
   19    tillfångata \RP{}
   20    döda/äta \RP{}

\cred{ http://dngnsndrgns.blogspot.se/2017/01/the-black-hack-creature-reactions.html }

