Svara på dessa frågor innan du slår tärning:

1.  Är ditt _mål_ och din _metod_ tydliga?

2.  Har du \fördelx{} eller \nackdel{}?

3.  Har \xr{övning gett färdighet}{övning-ger-färdighet}?

4.  Vill du \satsax{}?

5.  Vill någon \hjälpax{} eller \hindra{} dig?
