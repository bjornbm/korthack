Vättar är korta och krumma humanoider avlade i underjordens mörker. De är seniga och starka och med sin grymma lynne \REM{vicious streak?} en vuxen människas jämlike i strid.

Vättarnas ambition och företagsamhet "is rivaled only by" människans. Endast deras oförmåga att skapa långvariga allianser mellan sina klaner begränsar deras framfart ovan jord.

De vätteklaner som har ambitioner ovan jord avlar och föder upp vargar. De största vargarna de lyckas avla fram använder deras spejare som riddjur. Vargar som inte är stora nog att rida på använder de som attackdjur.

*   Vätte: nivå 0 (\GE{} 1, \KP{} 1)
*   Varg:  nivå 0 (\GE{} 1, \KP{} 1), klor (\kortvapen{}, \skada{} 2)

Vargar utan ryttare eller drivare attackerar närmsta fiende eller flyr (moralslag).


Fotsoldat
---------

1.  improviserat vapen
2.  kort vapen
3.  kort vapen, sköld
4.  långt vapen
5.  kastvapen, kort vapen
6.  skjutvapen

Spejare
-------

1.  kort vapen, sköld, varg
2.  långt vapen, varg
3.  kastvapen, kort vapen, varg
4.  skjutvapen, varg
5.  skjutvapen, kort vapen, varg


Vargdrivare
-----------

En vargdrivare kan dirigera sina vargar att attacker valda fiender, eller att försvara henom. Eventuellt andra trick som att apportera, söka, eller spela död.

1.  improviserat vapen, visselpipa, \D4 vargar
2.  kort vapen, visselpipa, \D4 vargar
3.  långt vapen, visselpipa, \D4 vargar
4.  sköld, visselpipa, \D4 vargar

Stormtrupp
----------

1.  kort vapen, sköld, rustning
2.  kort vapen, sköld, rustning
3.  långt vapen, rustning
4.  långt vapen, rustning
5.  kastvapen, kort vapen, sköld, rustning
6.  skjutvapen, kort vapen, rustning

Shaman
------

När vättar har en shaman med sig behöver de endast \provasinmoral{} när de angrips av magi, för de litar på att shamanens krafter (verkliga eller inbillade) garanterar deras seger mot mundana fiender.
Det gäller även shamanen själv.
De måste dock genast \provasinmoral{} om en av deras shamaner blir dödade (eller flyr).

1.  --
2.  improviserat vapen
3.  sköld
4.  mystisk kraft
5.  improviserat vapen, mystisk kraft
6.  sköld, mystisk kraft

Krafter (markera \KP{} för varje användning):

1.  mörker (släcker ljuskällor eller gör dagljus till \xr{svagt ljus}{belysning} i ett \omrade{})
2.  väck död (\GE{} 1(\UTS?))
3.  skräck (\skada{} 1(\UTS))
4.  paralysera (\skada{} 1(\HVD?))
5.  blixtnedslag (endast utomhus, \sHVD{} i \skada{})
6.  insektssvärm (\skada{} 1 per fiende (eller \UTS{} fiender, eller per fiende i ett \omrade{}?), \rustning{}/\skold{} hjälper inte.)


### Paralysera

När du blir paralyserad tappar du det du håller i och faller ihop, oförmögen att aktivera dina muskler.

\Prova{\KRP} varje gång det blir ditt \drag{}.
Om du lyckas utför du \KRP{} \skada{} mot paralyseringen.


### Skräck

När du är skräckslagen kan du inte närma dig eller attackera den/det skrämmande och har \nackdel{} på alla \termm{grundegenskapsslag}.
En skräckslagen \SLP{} flyr från den/det skrämmande.

\Prova{\UTS} varje gång det blir ditt \drag{}.
Om du lyckas utför du \UTS{} \skada{} mot skräcken.
