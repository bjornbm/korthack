Bestäm max antal rum (eller slå 3\D4).
Om max antal rum uppnåtts när ett rum utforskas så är det bossrummet
(kvarvarande outforskade dörrar kan leda till bossrummet eller vara bakvägar ut),
annars slås \D4 minus antalet outforskade dörrar på kartan:

*   Om resultatet <1 så har rummet två dörrar varav den ena loopar tillbaka till en annan outforskad dörr.
*   Om resultatet >1 och antalet outforskade dörrar är större än antalet outforskade rum så är det bossrummet.
*   Annars är antalet dörrar i rummet lika med resultatet.
\REM{*   Annars är antalet dörrar i rummet lika med det lägre av resultatet och antalet outforskade rum plus 1.}

Om det blir slut på outforskade dörrar innan max antal rum uppnåtts så lägg till en hemlig dörr i ett av rummen.

\cred{Rocket Dungeons}

