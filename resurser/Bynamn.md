Hitta ortsnamn på <https://sv.wikipedia.org/wiki/Lista_%C3%B6ver_Sveriges_t%C3%A4torter>. Sortera efter folkmängd, eller en kommun annat än Örebro.


# Första led

Häll
Val
Vall
Tall
Gran
Troll
Vind
Får
Get
Gro
Kvarn
Tjäll
Al
Ask
Ek
Kol
Malm
Brun
Svart
Grå
Hal
Kall
Sten
Block


# Andra led

mon
mora
dal(a)
fåra
träsk
sjö(n)
bro
ås(en)
myren
klyft(a)
berg(a)
damm(en)
kvarn(en)
by(n)
tun(et)
gård(a)
fäste
brottet
roten
tjärn(en)
häll
vall(en)
hall(a)
