Vad var ditt yrke innan du gav dig av?
Ditt yrke kan, om det är relevant, ge dig \fordelx{} en gång per spelmöte.
\
\columnTwo

1.  bonde
2.  dräng eller piga
3.  springschas
4.  värdshusskänk
5.  skräddare
6.  skomakare
7.  jägare
8.  utkastare
9.  handlare
10. herde

\next{\columnTwo}

11. smed
12. slaktare
13. tiggare
14. tempeltjänare
15. tjänare
16. bagare
17. fiskare
18. skatteindrivare
19. vakt
20. lärare

\columnend
