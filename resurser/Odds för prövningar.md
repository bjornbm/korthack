Chans att \lyckas{} (\exceptionelltbra{}) med en \prövningx{}:

|          |    \Dålig  |   \Hyfsad  |   \Bra     | \Mktbra    |
|----------|:----------:|:----------:|:----------:|:----------:|
| \Nackdel |   0 %      |  25 %      |  40 %      |  50 (8) %  |
| ---      |  33 %      |  50 %      |  60 (10) % |  67 (25) % |
| \Fördel  |  67 %      |  75 (13) % |  80 (30) % |  83 (42) % |

Risk att \misslyckas{} (\kapitalt{}) med en \prövning{}:

|         |    \Dålig  |   \Hyfsad  |   \Bra     | \Mktbra    |
|---------|:----------:|:----------:|:----------:|:----------:|
|\Nackdel | 100 (50) % |  75 (38) % |  60 (30) % |  50 (25) % |
|---      |  67 (17) % |  50 (13) % |  40 (10) % |  33 (8) %  |
|\Fördel  |  33 %      |  25 %      |  20 %      |  17 %      |


\REM{ Med tärningen:
# Odds

Chans att lyckas (exceptionellt bra) med en \prövning{}:

           \Dålig{} (\D6)   \Hyfsad{} (\D8)   \Bra{} (\D10)   \Mktbra{} (\D12)
--------- ---------------- ----------------- --------------- ------------------
\Nackdel    0 %             25 %              40 %            50 (8) %
---        33 %             50 %              60 (10) %       67 (25) %
\Fördel    67 %             75 (13) %         80 (30) %       75 (42) %

Risk att misslyckas (kapitalt) med en \prövningx{}:

           \Dålig{} (\D6)   \Hyfsad{} (\D8)   \Bra{} (\D10)   \Mktbra{} (\D12)
--------- ---------------- ----------------- --------------- ------------------
\Nackdel   100 (50) %       75 (38) %          60 (30) %        50 (25) %
---         67 (17) %       50 (13) %          40 (10) %        33 (8) %
\Fördel     33 %            25 %               20 %             17 %
}
