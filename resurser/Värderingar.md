Välj något som din \RP{} värderar, beundrar hos andra och själv strävar mot.
Kommer hen att tvingas agera mot sin värdering?
\
\columnTwo

1.  andlighet
1.  barmhärtighet
1.  disciplin
1.  frihet
1.  försiktighet
1.  generositet
1.  heder
1.  humor
1.  lojalitet
1.  medkänsla

\next{\columnTwo}

11. mod
1.  optimism
1.  professionalism
1.  pålitlighet
1.  rättvisa
1.  samarbete
1.  visdom
1.  ärlighet
1.  återhållsamhet
1.  ödmjukhet

\columnend

\REM{
ledarskap
tillit
försiktighet
förlåtande/försonlighet
kunskap
tillförsikt (optimism)
lycka
påhittighet
lärande
ihärdighet?
självbehärskning
}
