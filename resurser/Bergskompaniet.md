Armé
Division/legion (5000?)
Bataljon/kohort (500?)
Kompani (50–300)
Pluton/korpralskap (4 rotar? 25 man?)
Rote (2–16)


General/fältmarskalk/
Fältöverste
Överste
Kapten
Löjtnant
Sergeant
Korpral
Menig


Kvartersmästare (mat, sovplatser, lägerutrustning, sjukvård …)
Rustmästare (vapen, rustningar, ammunition …)

<https://sv.wikipedia.org/wiki/Underofficer#Sverige>:

> Av underofficerare hade infanteriet i äldre tider (1600-, 1700- och början av 1800-talet) vid varje kompani en fältväbel, en sergeant, en förare, en furir, en rustmästare och en mönsterskrivare; vid kavalleriet hade varje skvadron en kvartermästare och en mönsterskrivare

<https://sv.wikipedia.org/wiki/Korpral#Sverige_och_Finland_under_1600-_och_tidiga_1700-talet>:

> I den karolinska armén förde korpralen befäl över ett korpralskap på 24 man vilket var indelat i fyra rotar med 6 man vardera.


<https://sv.wikipedia.org/wiki/Kompani>:

Ett kompani är ofta specialiserat för en viss uppgift.

Chefen för ett kompani är vanligen major eller kapten. Som biträde finns en kompaniadjutant/kompanikvartermästare (ett befäl som biträder kompanichefen med expedition, materielförvaltning, kassatjänst med mera).

Ett kompani är den minsta arméenhet som i administrativt och taktiskt hänseende är delvis självständigt.


# Intressanta karaktärer i kompaniet

Kompanichef (kapten)
Löjtnanter
Sergeanter
Korpraler

- Skattmästare (betalar löner)
- Mönsterskrivare (för bok över soldater)
- Rustmästare
  - Hantverkare (vässa vapen, reparera rustningar …)
  - Stallhållare(?)
- Kvartersmästare
  - Kock
  - Barberare/kirurg
  - Sjuksystrar
- Kaplan (välsignar)
- Budbärare

