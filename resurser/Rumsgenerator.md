Välj/slumpa storlek samt ett/flera ytskikt och utmärkande drag.

 \D10 Storlek         Ytskikt         Drag          Drag
----- --------------- --------------- ------------- ------------
    1 trång gång      jord/rötter     klyfta        staty(er)
    2 bred gång       naturlig sten   brunn         altare
    3 vindlande gång  uthuggen        fontän        eldstad
    4 trappa          huggen sten     trappa        grav(ar)
    5 spiraltrappa    tegel           galleri       hål i tak
    6 kyffe           puts            arkad         hål i golv
    7 litet rum       trä             pelare        speglar
    8 stort rum       textilier       nisch(er)     takkrona
    9 liten sal       marmor          kupoltak      fresker
   10 stor sal        mosaik/fresk    bassäng       barrikad


# Rumsfunktioner {#rumsfunktion}

Välj/slumpa en funktion beroende på vem rummet nyttjas eller nyttjats av.
Tänk på att ett rum kan ha flera funktioner.

\REM{ Minst sofistikerade funktioner till vänster, funkar även för bestars lyor. }

--- -------------- -------------- -------------- -------------
  1 soprum         fängelse       offerplats     bibliotek
  2 latrin         vaktrum        skyddsrum      boskap
  3 sovrum/-sal    barrikad       träning        utbildning
  4 vattenhål      kök            skattkammare   tronrum
  5 förråd         tortyr         stall          studier
  6 matsal         produktion     arena          mystik
  7 fälla          odling         underhållning  verkstad
  8 tvätt/bad      gruva          bostad         smedja
  9 genomfart      helgedom       mottagning     ledning
 10 flyktväg       gravplats      utkik          vårdsal
--- -------------- -------------- -------------- -------------

\REM{

Funktioner:

-   bostad (sovsal, sovrum)
-   mat (kök, mässhall)
-   förråd (verktyg, mat)
-   helgedom (altare, kyrka, relik, staty)
-   grav
-   underhållning (spelrum, arena, harem)
-   träning (skjutbana, arena, hinderbana)
-   latrin/sopor
-   gruva
-   tronsal
-   vatten (brunn, bad)
-   verkstad (smedja, snickeri)
-   säkerhet (vaktrum, portrum, barrikad)
-   fängelse (cell, oubliette)
-   djur (stall, kennel, boskap)
-   skattkammare

}
