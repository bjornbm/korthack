\REM{# Fiender {#fiendeklasser}

\FANTASY{ exemplen }

\REM{ ohyra, minion, väktare, löjtnant, boss, über }

             |  \Skada   |  \KP  |
-------------|:---------:|:-----:|--------------------------------------
Irriterande  |     1     |   1   | \ex{råtta, stor insekt…}
Ynklig       |     2     |   2   | \ex{civilist, hund, liten oknytt…}
Svag         |     2     |   4   | \ex{bråkstake, kobold, kamphund…}
Duglig       |     3     |   6   | \ex{soldat, bandit, vätte, varg…}
Utmanande    |     4     |   8   | \ex{riddare, illvätte, björn, (var)ulv…}
Stark        |     5     |  12   | \ex{rese, grizzly, minotaur, mumie…}
Mäktig       |     6     |  20   | \ex{hjälte, troll, vampyr, demon…}
Legendarisk  |     7+    |  40+  | \ex{jätte, drake, hydra, halvgud…}

\REM{\Verkan{} är för en fiendes kraftfullaste attack(er).}
Rustning\sec{rustning} och specialförmågor gör fiender svårare.
\Rp{} kan fördela sin \verkanx{} på flera irriterande/ynkliga fiender i en flock.

}

\FANTASY{ exemplen }

\REM{ ohyra, minion, väktare, löjtnant, boss, über }

             \REM{| Nivå }| \Ge | \Kp|
-------------\REM{|:----:}|:---:|:---:|--------------------------------------
Ynklig       \REM{|  -   }|  1  |  1  | \ex{hund, stor råtta/insekt…}
Svag         \REM{|  0   }|  1  |  1  | \ex{soldat, bandit, vätte, varg…}
Duglig       \REM{|  1   }|  2  |  3  | \ex{veteran, riddare, illvätte, ulv…}
Utmanande    \REM{|  2   }|  3  |  5  | \ex{rese, björn, varulv…}
Stark        \REM{|  3   }|  4  |  7  | \ex{lejon, minotaur, mumie…}
Mäktig       \REM{|  4   }|  5  |  9  | \ex{troll, vampyr, hydra, demon…}
Legendarisk  \REM{|  5+  }|  6+ | 11+ | \ex{drake, halvgud, demonfurste…}

Hövdingar/ledare är en klass svårare, hjältar två klasser svårare.
\Ge{} gäller en fiendes bästa eller mest relevanta \GE{}.
Utrustning, immuniteter, magi och andra förmågor gör fiender svårare.


\cred{ Se "The order of might" på Torchbearers "GM sheet_r4.pdf". Förvisso upptäckt i efterhand. }

