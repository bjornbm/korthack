Furstendömet (landet/stadsstaten) FFF1 ligger i utkanten av civilisations utbreding. Det består av den befästa staden SSS1 (5 000 invånare) omgiven av ett antar mindre byar. I söder gränsar den mot FFF2. I norr gränsar den mot vildmark och MMM1-bergen vildmarken. I öster finns barbarernas länder och i väster HHH1-havet.

Fursten som ärvt FFF1 är måttligt nöjd med sin lott och har ambitionen att utöka sina domäner. En utökning norrut bortom byn BBB1 borde i teorin vara enkel; bara en fråga om att rensa upp den handful vättar gör att nybyggare inte vågar upprätta nya bosättningar. För tillfället är dock furstens armé upplåst i det segdragna kriget mot hens kusin i FFF2.

Till råga på allt rapporterar guvernören i BBB1 att vättarna har ökat sin aktivitet, och att det bristfälligt tränade och utrustade medborgargardet i BBB1 är hårt pressat. Invånarna i BBB1 uttrycker missnöje med avsaknaden av väpnad hjälp från huvudstaden.

För att rädda upp den eskalerande situationen in norr med minimal investering och risk erbjuder fursten stridsföra män och kvinnor (som inte är skyldiga att tjäna i hens arme) följande:

1.  liberala uppehållstillstånd (för ickemedborgare), vapenbärarlicenser och skattbärgarrättigheter giltiga i BBB1 socken och därtill angränsande vildmarker,
2.  \sm{5} för var vätte som dräps (den dräptes vänstra öra måste presenteras för guvernören för att denne ska betala ut belöningen),
3.  begränad amnesti för eventuella tidigare brott begågna i furstendömet eller annorstädes.

Därigenom hoppas han att ambitiösa/modiga/dåraktiga/desperata äventyrare/vagabonder/våldsmän ska lockas till BBB1 för att stärka medborgargardets led och lösa vätte-problemet åt honom. Motkraven är att:


1.  registrera sig som medlem i det för ändamålet upprättade äventyrargillet,
2.  betala ett tionde av det sammalagda värdet av bärgade skatter till guvernören i BBB1,
3.  inte begå brott eller övergrepp mot medborgarna i BBB1 socken.


BBB1
====

- 500 invånare
- bönder, skogsbrukare, jägare, bergsmän (fåtal/småskalig brytning)
- medborgargarde
    - i bästa fall 50 civila med mestadels improviserade vapen
- guvernör
    - skattmästare
    - stall
    - 7 soldater
- byråd
- två värdshus
- diversehandel
- smed, pilbågsmakare(?),
- skräddare, sadelmakare, krukmakare
- timmerman, möbelsnickare
- bagari, rökeri, bryggeri, slakteri
- apotekare, kirurg, barnmorska
- tempel, kyrka, kyrkogård
- lärd man, spågumma(?)

Nomadfolket
-----------

Zigenaraktikt folk som reser runt med sina vagnar. Färgglada. Dansar, sjunger, berättar historier runt lägerelden. Spågumma. Har eldteknologi (eller är det magi?):

- rökbomber
- tomtebloss/flares
- graneter
- brandbomber (molotov cocktails, explosiv olja)
- fyrverkerier
- smällare

Hooks:

- barn/flickor kidnappade av vättar?
- berätta historia om något de sett?
- har en gud som kan hjälpa \RP{} med magi?
- spågumman spår något om \RP{}.


Shopping
========

När \RP{} vill handla en vara går de till lämplig butik och slår \D6 för att se om varan finns i lager.
Antal varor i lager är $2^{\D6 - \textrm{prisklass{}$.
I praktiken finns ett exemplar av varan om man slå lika med prisklassen.
För varje steg högre man slår fördubblas antalet av varan.

 Prisklass          Pris     Finns på
----------- ------------- -------------
1           \sm{1}            1+
2           \sm{5}            2+
3           \sm{10}           3+
4           \sm{20}           4+
5           \sm{50}           5+
6           \sm{100}          6+
7           \sm{200}          7+
8           \sm{500}          8+
9           \sm{1000}         9+

Slår man ett steg lägre än prisklassen så är köpmannen villig att skramla fram varan men det tar \timmarx{} och varan kostar dubbelt det vanliga.

Om varan de söker inte är alltför exotisk kan \RP{} försöka hitta en ägare bland byborna och övertala denne att sälja genom en \prövning{övertala, timmar}:

*   \K{}  ingen vill sälja
*   \M{}  dubbla priset
*   \L{}  nypris för begagnad vara
*   \E{}  halva priset (hen behövde den ändå inte)

För att sälja en vara i byn måste man slå över prisklassen, per exemplar.
Då får man genast halva priset från prislistan.
Slår man under priset erbjuder köpmannen att försöka hitta en köpare i SSS1 inom \D4 veckor.


Inaktiva \RP{}
==============

\RP{} som inte deltar i en expedition kan välja mellan att (en av):

1.  Träna. Detta ger 1 \EP{}, men bara till \RP{} är en \EP{} ifrån att öka sin nivå. Den sista \EP{} som krävs för att öka i nivå måste komma från äventyrande.
2.  Forska i byns bibliotek
3.  Göra ett ärende till SSS1 för att (en av):
    -   köpa något som inte går att finna i BBB1,
    -   sälja något som är osäljbart i BBB1,
    -   söka en \handlangare{} med en specifik profil eller specialiter,
    -   forska i stadens bibliotek.
4.  Skapa något hantverk.
5.  Förvärvsarbeta (\sm{\D8}).
6.  Söka förföra eller schmoosa någon.
7.  Validera eller jaga nya rykten.


Religion
========

Sol
---


Kända raser
===========

Nedan är vad de flesta \RP{} och andra vuxna människor i FFF1 tror sig veta om andra folkslag.


Vättar
------

Vättar är ett ondskefullt folkslag som terroriserar civilisationens utkanter och begränsar människans utbredning. De kryper ner från de branta bergen och ut ur de mörka skogarna för att göra räder mot bosättningar och karavaner.

Trots att vättar är småväxta och krumma så är de med sin hänsynslösa grymhet en vuxen människas jämnlike i strid.


Illvättar
---------

Illvättar är besläktade med \vättar{} men är stora och starka som den starkaste människa. De må vara mindre utbredda än vättarna men när de genomför räder sker det med en skräckinjagande effektivitet och brutalitet.


Dvärgar
-------

Dvärgarna är en uråldrig och en gång mäktig ras, med enastående ingenjörskunskaper och arkitektur. De var vättarnas traditionella fiender, i ständig kamp om underjordens och utrymmen och rikedomar.

För inte många mansåldrar sedan fanns det diplomatiska relationer och handel mellan människor och dvärgar, men dvärgarna bröt kontakten och drog sig undan ner i bergen.
Kanske är de nu utdöda.
Inga kända dvärgsamhällen finns kvar men dvärgarna har lämnat många ruiner, underjordiska städer och andra komplex efter sig.
Dessa är numer i bästa fall tomma, i värsta fall övertagna av \vättar{} eller något ännu värre.


Alver
-----

Det finns legender om alver, ett ädelt och långlivat släkte, men det finns inga eller otvetydiga vittnesmål eller spår av deras existens.


Odöda
-----

Det finns dokumenterade fall av bosättningar som angripits av vandrande skelett. Många menar att det också förekommer spöken och gengångare av andra slag men det är omtvistat vad som är sanning och vad som är sägen. Det finns en stor fruktan för nekromantiker, men de sista kända nekromantikerna besegrades av Sols förkämpar för hundratals år sedan.


Gamla civilisationer
--------------------

Spår av gamla civilisationer finns överallt; ruiner, kryptor, monument, artefakter. Hur långt de sträckte sig, hur länge de varade, och varför de föll är höljt i dunkel.
