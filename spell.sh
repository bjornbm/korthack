#!/bin/bash

set -o nounset
set -o errexit
#set -o verbose

HOME_DIR=$(dirname "$0")

declare -a files

if [ "$#" -ge 1 ]
  then
    files=("$@")
  else
    files=( \
      "$HOME_DIR"/*.gpp \
      "$HOME_DIR"/*.md  \
      )
  fi

for file in "${files[@]}"
  do
    aspell --home-dir="$HOME_DIR" check "$file"
  done
