Varifrån kommer du?

1. Tyrrne,
\hfill{}2.\enspace{}en annan del av Riket,
\hfill{}3.\enspace{}barbarländerna.\hfill\p{}

Varför riskerar du liv och lem som äventyrare vid världens ände?

1.  Du vill uppleva äventyr och spänning. \REM{Tror du ja!}
2.  Du är ute efter snabba och lätta pengar. \REM{Lycka till!}
3.  Du söker sanning och mening. \REM{Komer de gamla gudarna svara dig?}
4.  Du vill bevisa något för någon. \REM{För vem och varför?}
5.  Du är på flykt undan ditt förflutna. \REM{Från vad? Ett minne?}
6.  Du är här för amnestin. \REM{Vad har du gjort?}
7.  Du är inte välkommen någon annanstans. \REM{Varför har du förvisats?}
8.  Du duger inte till annat. \REM{Gudarna vet att du har försökt.}
