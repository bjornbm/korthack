En \intro{rustning} ger dig 2 extra \KP{} i \striderx{}.
Du \xr{förlorar}{kroppspoäng} den innan dina egna \KP{} och återfår den genom att se över rustningen i några minuter.
En \tung{} rustning ger dig 4 extra \KP{} och en \mkttung{} rustning ger dig 6 extra \KP{}.
Du kan inte simma, smyga eller sova iklädd \tung{} rustning.

Med en \intro{sköld} kan du en gång per \rundax{} minska \skadan{} från en \attack{} mot dig med 1.
En \tung{} sköld minskar \skadan{} med 2.
\REM{Du kan dessutom när som helst undgå all \skada{} från en \attack{} genom att låta din sköld förstöras.}

En \hjalm{} minskar risken att bli \xr{dödligt sårad}{kroppspoäng}.
