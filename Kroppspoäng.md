\REM{
Du har \KRP{} \intro{kroppspoäng} (\introKP{}) som representerar din vigör.
För varje \intro{skada} du får markerar du en \introKP{}.
Markera först alla \introKP{} med \checkS{} \ex{skakad}, sedan med \checkH{} \ex{sliten}, och sist med \checkD{} \ex{sårad}.
Du dör om du får mer \intro{skada} än vad du kan markera på dina \introKP{}.

Varje gång du markerar en eller flera \introKP{} med \checkD{} måste du \slåförx{} \KRP{} för att inte förlora medvetandet i en timme.
Om du bär en \intro{hjälm} så får du \fördel{} på slaget.

Markeringarna försvinner när du \intro{vilar}.
Radera alla \checkS{} efter några sekunder, \checkH{} efter några timmar, och \checkD{} efter några dagar av ostörd vila.


# Kroppspoäng 2

Du har 2\ggr{}\KRP{}+2 \intro{kroppspoäng} (\introKP{}) som representerar din vigör.

För varje \intro{skada} du får förlorar du en \introKP{}.
När dina \introKP{} tar slut \ex{och varje gång du skadas därefter} måste du \slåförx{} \KRP{}.
Om du misslyckas så dör du,
annars förlorar du bara medvetandet.
\REM{En \intro{hjälm} ger dig \fördel{} på slaget.
}Om du bär en \intro{hjälm} så får du \fördel{} på slaget.

\REM{
\Slå{} \KRP{} när du dricker vatten och plåstrar om dig efter en \stridx{}.
Du återfår så många av de \introKP{} du _förlorade under \striden{}_.

\Slå{} \KRP{} när du får en ordentlig måltid och dygnsvila.
Du återfår så många av de \introKP{} du _förlorat under dagen_.
}

Med lite vatten och omplåstring så kan du återfå \introKP{} du nyss förlorat i \stridx{}.
\Slå{} \KRP{} för att se hur många \introKP{} du återfår.

Med en ordentlig måltid och dygnsvila så kan du återfå \introKP{} förlorade under dagen.
\Slå{} \KRP{} för att se hur många \introKP{} du återfår.

Övriga \introKP{} måste du vila i dagar eller veckor för att återfå.

\Slå{} \KRP{} för att se hur många \introKP{} du återfår när du:

1.  Plåstra om dig och drick för att återfå \introKP{} du nyss förlorat i \stridx{}.
2.  Ät och sov för att återfå \introKP{} du förlorat under dagen.
3.  Vila och bli omvårdad i dagar för att återfå övriga \introKP{}.
1.  Vatten och omplåstring ger tillbaka \introKP{} du nyss förlorat i \stridx{}.
2.  En ordentlig måltid och dygnsvila ger tillbaka \introKP{} du förlorat under dagen.
3.  Vila och omvårdad i dagar ger tillbaka övriga \introKP{}.


# Kroppspoäng 3
}

Du har \HVD+\UTS+2\ggr\KRP{} \intro{kroppspoäng} (\introKP{}) som representerar din vigör.
För varje \intro{skada} du får förlorar du en \introKP{}.
De återfås genom:

1.  vatten och bandagering för \introKP{} _nyligen förlorade i \stridx{}_,
2.  mat och dygnsvila för \introKP{} _förlorade under dagen_,
3.  en veckas vila och omvårdnad för resterande \introKP{}.

Vid varje tillfälle ovan kan du återfå upp till \sKRP{} \introKP.

Om dina \introKP{} tar slut \ex{och varje gång du skadas därefter} måste du \provax{\KRP}.
Om du misslyckas så dör du,
annars förlorar du bara medvetandet i en timme.
En \intro{hjälm} ger dig \fordel{} på \slaget{}.
\REM{Om du bär en \intro{hjälm} så får du \fordel{} på \slaget{}.}
