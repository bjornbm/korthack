\FANTASY

\
\columnTwoS

\Vapenx{}                 \hfill  \sm{20}\
\Tungt{} \vapenx{}        \hfill  \sm{50}\
\Skoldx{}                 \hfill  \sm{20}\
\Hjalmx{}                 \hfill  \sm{20}\
\Rustningx{}              \hfill  \sm{50}\
\Tungx{} \rustning{}      \hfill \sm{200}\
Lägerutrustning           \hfill  \sm{10}\
Klätterutrustning         \hfill  \sm{10}\
\Forbrukningsvarorx{}     \hfill   \sm{5}\
Förbrukningsvara \ex{en}  \hfill   \sm{1}\
Dekokt \en{en flaska/dos} \hfill   \sm{10}\

\next{\columnTwoS}

Småsak                    \hfill   \sm{2}\
Verktyg                   \hfill   \sm{5}\
Specialistverktyg         \hfill  \sm{10}\
Lyxartikel                \hfill \sm{20+}\
Värdshusmåltid            \hfill   \sm{1}\
Natthärbärge \ex{1 natt}  \hfill  \sm{1+}\
Specialisttjänst          \hfill  \sm{5+}\
Arbetsdjur                \hfill \sm{10+}\
Riddjur                   \hfill \sm{20+}\
Tjänare/bärare \ex{1 dag} \hfill   \sm{1}\
Vägvisare      \ex{1 dag} \hfill   \sm{2}\
Hyrsvärd       \ex{1 dag} \hfill   \sm{5}\
\REM{Specialist     \ex{1 dag} \hfill  \sm{10}\}

\columnend


\REM{Mer idéer:

Färdkost \ex{1 dag}       \hfill   \sm{1}\  % Ingår i förbrukningsvaror
Bandage                   \hfill   \sm{1}\  % Ingår i förbrukningsvaror

\Kastvapenx{} \ex{ett}    \hfill  \sm{10}\  % Ingår i vapen som "flera"
Vagn/kärra/båt \sm{50+}
Bostad/byggnad \sm{100+}
Sällskapsdjur?
Resor (droska, färja, …)
Religiosa/mystiska tjänster
Läkande örter/omslag
Akademiska tjänster?
Lärdes tjänster?
"Tjänster" allmänt?
Stadstull/grindtull?

Flammolja \ex{flaska}       \hfill \sm{10}\
Vigvatten \ex{flaska}       \hfill \sm{10}\
Läkande omslag              \hfill \sm{10}\
Granat                      \hfill \sm{10}\
Rökbomb                     \hfill \sm{10}\
Flashbang                   \hfill \sm{10}\
}
