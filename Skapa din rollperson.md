\REM{
Skapa en ny \rollpersonx{} (\RP{}) enligt nedan.
Du kan göra valen själv eller slumpa dem med hjälp av lämpliga tärningar.

1.  Vem är du? Välj ett kön och hitta på eller välj din \xr{bakgrund}{din-bakgrund} och ditt \xrr{namn}\sec{barbarnamn}.

2.  Välj din bästa \grundegenskapx{} (\GE{}).
Du har 2 i den och 1 i de andra.

\REM{1.  Välj en \grundegenskapx{} (\GE{}) att ha 2 i.
Du har 1 i de andra.}

3.  Välj din \xrr{startutrustning}.

4.  Anställ \hantlangarex{} och köp ytterligare \xr{utrustning}{prislista} baserat på era planer.
    Ni bör åtminstone ta med er mat, vatten, lägerutrustning och \belysningx{} om ni ska långt.

# Skapa din rollperson

Skapa en \RP{} med hjälp av instruktionerna nedan.
Du kan göra valen själv eller slumpa dem med hjälp av lämpliga tärningar.

1.  Välj en \xr{arketyp}{arketyper} för din \RP{}.

2.  Välj din \xrr{startutrustning}.

3.  Om du vill göra din \RP{} mer färgstark så kan du välja en \xr{värdering}{värderingar}, en \xr{svaghet}{svagheter} och/eller en \xr{drivkraft}{drivkrafter}\REM{ att spela på}.

4.  Bestäm ditt kön och välj eller hitta på ett \xrr{namn}.

5.  Anställ \hantlangarex{} och \xr{köp}{prislista} ytterligare \xrr{utrustning} baserat på era planer.
    Ni bör ha med er mat, vatten, lägerutrustning och \belysningx{} om ni ska långt.


\hyphenation{band-age}

Skapa en \RP{} enligt instruktionerna nedan.
Gör valen själv eller slumpa dem med hjälp av tärningar.

1.  Välj din \xrr{bakgrund}. \REM{och vad du fått med dig därifrån.}

2.  Välj din \xrr{startutrustning}.

3.  Välj ett \xrr{namn}.

4.  Hyr in \xr{tillfällig hjälp}{hyrd-hjälp} och \xr{köp}{prislista} ytterligare \xrr{utrustning} baserat på era planer.
    Ni bör ha med er vatten, bandage, \belysningx{}, mat och lägerutrustning på expeditioner.
}

\hyphenation{band-age}

Du har tröttnat på ditt obetydliga liv i din obetydliga hemby och gett dig av i jakt på äventyr och spänning.
Skapa din \RP{} enligt instruktionerna nedan.
\REM{Gör valen själv eller låt slumpen välja åt dig.}

1.  Välj en \grundegenskapx{} (\GE{}) som utmärker dig.
    Du har 2 i den och 1 i de andra.
2.  Välj din \xrr{startutrustning}.
3.  Välj ett \xrr{namn}.
4.  Hyr \xr{tillfällig hjälp}{hyrd-hjälp} och \xr{köp}{prislista} ytterligare \xrr{utrustning} baserat på era planer.
    Ni bör ha med er vatten, bandage, \belysningx{}, mat och lägerutrustning på expeditioner.

