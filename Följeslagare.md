\REM{
Utöver eventuella anställda och underordnade så kan du ha upp till \UTS{} trogna \intro{följeslagare} som är villiga att göra och utsätta sig för det mesta du ber dem om.
De handlar på ditt \dragx{},
\slarformoralx{} med \fordel{}
och får \EP{} \xr{som du}{grundegenskaper}.
}

Du kan övertala upp till \UTS{} \SLP{} att bli dina trogna \intro{följeslagare},
villiga att göra och utsätta sig för det mesta för din skull.
De handlar på ditt \dragx{},
\slarformoralx{} med \fordel{}
och får \EP{} \xr{som du}{grundegenskaper}.

Du börjar spelet med en \foljeslagare{}.

\REM{Om din \RP{} dör kan du om du vill fortsätta spela med din \foljeslagare istället.}
