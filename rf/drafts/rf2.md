\REM{ Nivå och EP }

\define{rfnivå}{\minipagebegin{t}{0.24\textwidth}
\Large
Nivå:\scriptsize

(öka efter expedition)
\minipageend}

\define{rferf}{\minipagebegin{b}{0.46\textwidth}
\normalsize
\Ep{}:
\hfill
\checkEPs{}  
\scriptsize
Erfarenhetspoäng, \trippelt{nivå} ökar din nivå
\minipageend}


\REM{ Grundegenskaper }


\define{Halfwidth}{\minipagebegin{#1}{0.46\textwidth}#2\minipageend}


\define{gekey}{\minipagebegin{c}{0.08\textwidth}
\raggedleft
\scriptsize  1: \D4\p{0}\tiny

\scriptsize  2: \D6\p{0}\tiny

\scriptsize  3: \D8\p{0}\tiny

\scriptsize  4: \D10\tiny

\scriptsize ≥5: \D12\tiny
\minipageend}


\define{gex}{\pbox{0.2\textwidth}{
\LARGE
\textsc{#1}: \\
\scriptsize
\centering
(#2)
}}


\REM{ Resurser }

\define{resurs}{\Large\minipagebegin{b}{0.22\textwidth}
#1:
\minipageend
\checkRF{}  
\scriptsize
#2}


\define{rfkp1}{\Halfwidth{b}{
\Large
\Kp:
\checkRF\checkGråKP

\scriptsize
Kroppspoäng=\nivå+\KRP, \REM{\checkO{}\ra}
\checkS{}\ra
\checkH{}\ra
\checkD{}
}}


\define{rfkp2}{\Halfwidth{b}{
\scriptsize
\checkS{} _(skakad)_ tas bort efter *ögonblick* av vila

\checkH{} _(sliten)_ tas bort efter *timmar* av vila

\checkD{} _(sårad)_ tas bort efter *dagar* av vila
}}


\REM{ Skills }

\define{rfspråk}{\minipagebegin{b}{0.46\textwidth}
\normalsize
Språk:  
\scriptsize
(\HVD{} stycken)
\minipageend}



\REM{ Rollformuläret }


\Large
Namn:
\hfill
\rfnivå

\REM{\vspace{-1ex}}
\normalsize
Grundegenskaper (\GE)  
\scriptsize
1: \D4,
2: \D6,
3: \D8,
4: \D10,
≥5: \D12

\gex{Krp}{kropp}

\gex{Hvd}{huvud}

\gex{Uts}{utstrålning}

\REM{\hfill \gekey}

\REM{
\normalsize
Bakgrund:
\vfill
Språk:}

\vfill

\rfkp1
\hfill
\rfkp2

\vspace{5pt}


\newpage


\define{rfutrustning}{\minipagebegin{t}{0.46\textwidth}
\normalsize
Utrustning:
\hfill
\scriptsize
(max \KRP{} \tunga{} föremål)
\minipageend}


\define{rfhantlangare}{\minipagebegin{b}{0.46\textwidth}
\vspace{0pt}
\normalsize
Hantlangare:
\hfill
\scriptsize
(max \UTS{} stycken)
\minipageend}


\define{rfljus}{\minipagebegin{t}{0.46\textwidth}
\normalsize
Ljuskällor:
\hfill
\scriptsize
(ny \checkO{}\ra{}\checkS\ra\checkH\ra\checkD{} slut)

\hfill\checkGrå

\hfill\checkGrå
\minipageend}


\define{dagsransoner}{\minipagebegin{b}{0.46\textwidth}
\normalsize
Mat:\hfill\checkDR

Dryck:\hfill\checkDR

\scriptsize
 \hfill
(dagsransoner)
\minipageend}


\define{rfsupplies}{\minipagebegin{t}{0.46\textwidth}
\scriptsize
\raggedright
_enkla kläder,
skor,
bälte,
filt,
ryggsäck,
vattenskinn (2 dagar),
elddon,
kokkärl,
slev,
kniv_
\minipageend}


\define{rfsilver}{\Halfwidth{b}{
\normalsize
\Silver{}:
\hfill
\sm{}
}}

\define{Dotfill}{\Halfwidth{b}{
\scriptsize
#1 \dotfill
}}

\normalsize

\rfutrustning
\hfill
\rfhantlangare{}
\REM{\p{x}\rfsupplies}

\normalsize

\vfill\Dotfill{1}
\vfill\Dotfill{2}
\vfill\Dotfill{3}
\vfill\Dotfill{4}
\vfill\Dotfill{5}
\vfill\Dotfill{6}
\vfill\Dotfill{7}
\vfill\Dotfill{8}

\rfsilver

\REM{

\hfill
\rfljus

\scriptsize
$\star$ lättåtkomlig (max 2)
\hfill
\dagsransoner
}

\vspace{5pt}


\pagebreak

\twocolumn

\Large Namn:  
\scriptsize (och bakgrund)

\normalsize Grundegenskaper (\GE)  
\scriptsize
1: \D4,
2: \D6,
3: \D8,
4: \D10,
≥5: \D12

\gex{Krp}{kropp}\hfill
\gex{Hvd}{huvud}\hfill
\gex{Uts}{utstrålning}\hfill



\pagebreak

\hfill\rfnivå

\normalsize Utrustning:\hfill\scriptsize(max \KRP{} \tunga{} föremål)

\scriptsize
\vfill{}1 \dotfill
\vfill{}2 \dotfill
\vfill{}3 \dotfill
\vfill{}4 \dotfill
\vfill{}5 \dotfill
\vfill{}6 \dotfill
\vfill{}7 \dotfill
\vfill{}8 \dotfill

\REM{\normalsize Hantlangare:\hfill\scriptsize(max \UTS{} stycken)}

\normalsize \Silver{}:\hfill \sm{}

\vspace{5pt}

