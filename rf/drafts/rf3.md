\REM{ Nivå och EP }

\define{rfnivå}{\minipagebegin{t}{0.24\textwidth}
\Large
Nivå:\scriptsize

(öka efter expedition)
\minipageend}

\define{rferf}{\minipagebegin{b}{0.46\textwidth}
\normalsize
\Ep{}:
\hfill
\checkEPs{}  
\scriptsize
Erfarenhetspoäng, \trippelt{nivå} ökar din nivå
\minipageend}


\REM{ Grundegenskaper }


\define{Halfwidth}{\minipagebegin{#1}{0.46\textwidth}#2\minipageend}


\define{gekey}{\minipagebegin{c}{0.08\textwidth}
\raggedleft
\scriptsize  1: \D4\p{0}\tiny

\scriptsize  2: \D6\p{0}\tiny

\scriptsize  3: \D8\p{0}\tiny

\scriptsize  4: \D10\tiny

\scriptsize ≥5: \D12\tiny
\minipageend}


\define{gex}{\pbox{0.2\textwidth}{
\LARGE
\textsc{#1}: \\
\scriptsize
\centering
(#2)
}}


\REM{ Resurser }

\define{resurs}{\Large\minipagebegin{b}{0.22\textwidth}
#1:
\minipageend
\checkRF{}  
\scriptsize
#2}


\define{rfkp1}{\Halfwidth{b}{
\Large
\Kp:
\checkRF\checkGråKP

\scriptsize
Kroppspoäng=\nivå+\KRP, \REM{\checkO{}\ra}
\checkS{}\ra
\checkH{}\ra
\checkD{}
}}


\define{rfkp2}{\Halfwidth{b}{
\scriptsize
\checkS{} _(skakad)_ tas bort efter \ögonblick{} av vila

\checkH{} _(sliten)_ tas bort efter \timmar{} av vila

\checkD{} _(sårad)_ tas bort efter \dagar{} av vila
}}


\REM{ Skills }

\define{rfspråk}{\minipagebegin{b}{0.46\textwidth}
\normalsize
Språk:  
\scriptsize
(\HVD{} stycken)
\minipageend}



\define{rfutrustning}{\minipagebegin{t}{0.46\textwidth}
\normalsize
Utrustning:
\hfill
\scriptsize
(max \KRP{} \tunga{} föremål)
\minipageend}


\define{rfhantlangare}{\minipagebegin{b}{0.46\textwidth}
\vspace{0pt}
\normalsize
Hantlangare:
\hfill
\scriptsize
(max \UTS{} stycken)
\minipageend}


\define{rfljus}{\minipagebegin{t}{0.46\textwidth}
\normalsize
Ljuskällor:
\hfill
\scriptsize
(ny \checkO{}\ra{}\checkS\ra\checkH\ra\checkD{} slut)

\hfill\checkGrå

\hfill\checkGrå
\minipageend}


\define{dagsransoner}{\minipagebegin{b}{0.46\textwidth}
\normalsize
Mat:\hfill\checkDR

Dryck:\hfill\checkDR

\scriptsize
 \hfill
(dagsransoner)
\minipageend}


\define{rfsupplies}{\minipagebegin{t}{0.46\textwidth}
\scriptsize
\raggedright
_enkla kläder,
skor,
bälte,
filt,
ryggsäck,
vattenskinn (2 dagar),
elddon,
kokkärl,
slev,
kniv_
\minipageend}


\define{rfsilver}{\Halfwidth{b}{
\normalsize
\Silver{}:
\hfill
\sm{}
}}

\define{Dotfill}{\Halfwidth{b}{
\scriptsize
#1 \dotfill
}}



\twocolumn

\Large Namn:

\normalsize Grundegenskaper (\GE)\REM{  
\scriptsize
(slå för; 1: \D4,
2: \D6,
3: \D8,
4: \D10,
5+: \D12)}

\REM{}

\define{gex}{\minipagebegin{t}{0.30\columnwidth}
\centering
\Large\textsc{#1}

\normalsize\p{6}

\hrulefill

\scriptsize (#2)
\minipageend}

\define{gex2}{\pbox{0.325\columnwidth}{
\Large
\textsc{#1}:\hfill{} \\
\scriptsize
\centering
(#2)
}}


\gex{Krp}{kropp}\hfill
\gex{Hvd}{huvud}\hfill
\gex{Uts}{utstrålning}


\REM{\Large\textsc{Krp:\hrulefill{}Hvd:\hrulefill{}Uts:\hrulefill}}


\REM{
\scriptsize
Slå för 1: \D4,
2: \D6,
3: \D8,
4: \D10,
≥5: \D12.
}
\REM{\normalsize Språk, mystik, med mera:}


\vfill
\Newcommand{\checkGKP}{\textcolor{lightgray}{\checkO\checkO\checkO\checkO\checkO\checkO\checkO\checkO}}
\normalsize Kroppspoäng (\KP)
\scriptsize (\checkO{}\ra
\checkS{}\ra
\checkH{}\ra
\checkD{})  
\Large\checkRF\checkGKP{}



\vspace{5pt}
\newpage




\hfill\Large Nivå: \p{666}

\normalsize Utrustning\hfill\scriptsize(max \KRP{} \tunga{} föremål)\normalsize

\define{Dotfill}{}
\scriptsize
\vfill{}1 \dotfill
\vfill{}2 \dotfill
\vfill{}3 \dotfill
\vfill{}4 \dotfill
\vfill{}5 \dotfill
\vfill{}6 \dotfill
\vfill{}7 \dotfill
\vfill{}8 \dotfill

\normalsize \Silver{}:\hfill \sm{}


\vspace{5pt}
\newpage

\define{Hfill}{\hfill\p{}}


\define{gex}{\minipagebegin{t}{0.30\columnwidth}
\centering
\Large\textsc{#1}

\normalsize\p{6}

\hrulefill

\scriptsize (#2)
\minipageend}

\define{gex2}{\pbox{0.33\columnwidth}{
\large
\textsc{#1}: \p{66} \\
\scriptsize (#2)
}}

\define{Dotfill}{\vfill{}\scriptsize#1 \tiny\dotfill}


\REM{# NYTT FORMULÄR ################################################################}

\Large Namn:

\normalsize Grundegenskaper (\GE)\REM{  
\scriptsize
\Ge-sla
1\ra\D4,
2\ra\D6,
3\ra\D8,
4\ra\D10,
5+\ra \D12}\REM{  
\scriptsize
\Ge-slag
1: \D4,
2: \D6,
3: \D8,
4: \D10,
5+: \D{12}}

\gex2{Krp}{kropp}\Hfill
\gex2{Hvd}{huvud}\Hfill
\gex2{Uts}{utstrålning}

\REM{
\scriptsize\Ge
1\ra\D4,\Hfill
2\ra\D6,\Hfill
3\ra\D8,\Hfill
4\ra\D10,\Hfill
5+\ra \D12
}


\vfill\Large\Kp: \Hfill\checkRF\checkGråKP{}  
\scriptsize Kroppspoäng=\nivå+\KRP, \checkO{}\ra\checkS{}\ra\checkH{}\ra\checkD{}


\vspace{5pt}
\newpage




\Hfill\Large Nivå: \p{666}

\normalsize Utrustning\Hfill\scriptsize(max \KRP{} \tunga{} föremål)
\normalsize

\scriptsize

\Dotfill{1}
\Dotfill{2}
\Dotfill{3}
\Dotfill{4}
\Dotfill{5}
\Dotfill{6}
\Dotfill{7}
\Dotfill{8}

\normalsize \Silver{}:\Hfill \sm{}

\vspace{5pt}
\pagebreak


\REM{# NYTT FORMULÄR ################################################################}

\Large Namn:

\normalsize Grundegenskaper (\GE)\REM{  
\scriptsize
\Ge-sla
1\ra\D4,
2\ra\D6,
3\ra\D8,
4\ra\D10,
5+\ra \D12}\REM{  
\scriptsize
\Ge-slag
1: \D4,
2: \D6,
3: \D8,
4: \D10,
5+: \D{12}}

\gex2{Krp}{kropp}\Hfill
\gex2{Hvd}{huvud}\Hfill
\gex2{Uts}{utstrålning}

\scriptsize\Ge
1\ra\D4,\Hfill
2\ra\D6,\Hfill
3\ra\D8,\Hfill
4\ra\D10,\Hfill
5+\ra \D12



\vfill\Large\Kp: \Hfill\checkRF\checkGråKP{}  
\scriptsize Kroppspoäng=\nivå+\KRP, \checkO{}\ra\checkS{}\ra\checkH{}\ra\checkD{}


\vspace{5pt}
\newpage




\Hfill\Large Nivå: \p{666}

\normalsize Utrustning\Hfill\scriptsize(max \KRP{} \tunga{} föremål)
\normalsize

\scriptsize

\Dotfill{1}
\Dotfill{2}
\Dotfill{3}
\Dotfill{4}
\Dotfill{5}
\Dotfill{6}
\Dotfill{7}
\Dotfill{8}

\normalsize \Silver{}:\Hfill \sm{}

\vspace{5pt}
\pagebreak


\REM{# NYTT FORMULÄR ################################################################}

\Large Namn:

\vspace{0.5ex}
\normalsize Grundegenskaper (\GE)\REM{  
\scriptsize
\Ge-sla
1\ra\D4,
2\ra\D6,
3\ra\D8,
4\ra\D10,
5+\ra \D12}  
\scriptsize
\Ge-slag\Hfill
1: \D4,\Hfill
2: \D6,\Hfill
3: \D8,\Hfill
4: \D10,\Hfill
5+: \D12

\gex2{Krp}{kropp}\Hfill
\gex2{Hvd}{huvud}\Hfill
\gex2{Uts}{utstrålning}

\REM{\scriptsize\Ge\Hfill
1\ra\D4,\Hfill
2\ra\D6,\Hfill
3\ra\D8,\Hfill
4\ra\D10,\Hfill
5+\ra \D12}



\vfill\normalsize\Kp:\Large\checkRF\checkGKP{}  
\scriptsize Kroppspoäng=\nivå+\KRP, \checkO{}\ra\checkS{}\ra\checkH{}\ra\checkD{}

\vspace{5pt}
\newpage




\Hfill\Large Nivå: \p{666}

\vspace{0.5ex}
\normalsize Utrustning\Hfill\scriptsize(max \KRP{} \tunga{} föremål)

\scriptsize

\Dotfill{1}
\Dotfill{2}
\Dotfill{3}
\Dotfill{4}
\Dotfill{5}
\Dotfill{6}
\Dotfill{7}
\Dotfill{8}

\normalsize \Silver{}:\Hfill \sm{}

\vspace{5pt}

