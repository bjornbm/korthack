\newgeometry{
  left=7mm,
  right=7mm,
  top=7mm,
  bottom=5mm
  }

\define{Hfill}{\hfill{}}


\define{Third}{\minipagebegin{[t][13mm]}{0.29\columnwidth}
  \centering
  #1
  \minipageend{}}


\define{gex}{\Third{
  \Large\textsc{#1}
  \vfill
  \scriptsize(#2)}}


\define{\geslag}{\scriptsize
  \GE-slag\Hfill
  1: \D4,\Hfill
  2: \D6,\Hfill
  3: \D8,\Hfill
  4: \D10,\Hfill
  ≥5: \D12
  \normalsize
}

\define{grundegenskaper}{
  \normalsize Grundegenskaper (\GE)\smallskip
  \
  \gex{krp}{kropp}\Hfill
  \gex{hvd}{huvud}\Hfill
  \gex{uts}{utstrålning}\smallskip
}

\define{\erfarenhet}{\scriptsize
  Erfarenhet
  \hfill
  \normalsize
  \fisheye 
  \mdlgwhtcircle
  \fisheye
  \mdlgwhtcircle
  \mdlgwhtcircle
  \fisheye
  \mdlgwhtcircle
  \mdlgwhtcircle
  \mdlgwhtcircle
  \fisheye
  \mdlgwhtcircle
  \mdlgwhtcircle
  \mdlgwhtcircle
  \mdlgwhtcircle
  \fisheye
  \scriptsize
  …
  \normalsize
}

\Newcommand{\OOOOO}{\checkO\hfill\checkO\hfill\checkO\hfill\checkO\hfill\checkO}
\Newcommand{\OOOOo}{\checkO\hfill\checkO\hfill\checkO\hfill\checkO\hfill%
  \textcolor{lightgray}{\checkO}}
\Newcommand{\ooooo}{\textcolor{lightgray}{%
  \checkO\hfill\checkO\hfill\checkO\hfill\checkO\hfill\checkO}}
\Newcommand{  \hfilll}{\hfill\hfill}
\Newcommand{ \hfillll}{\hfill\hfill\hfill}
\Newcommand{\hfilllll}{\hfill\hfill\hfill\hfill}
\Newcommand{  \checkKP}{\OOOOo{}\hfilllll%
  \textcolor{lightgray}{\OOOOO{}\hfilllll%
                        \OOOOO{}\hfilllll%
                        \OOOOO}}

\define{Dotfill}{\vfill\scriptsize#1 \tiny\dotfill{}}

\define{utrustning}{

  \normalsize Utrustning
  \Hfill
  \scriptsize (max \KRP{} \tunga{} föremål)
  \Dotfill{1}
  \Dotfill{2}
  \Dotfill{3}
  \Dotfill{4}
  \Dotfill{5}
  \Dotfill{6}
  \Dotfill{7}
  \Dotfill{8}}

\REM{# FORMULÄR ################################################################}

\p{
\Large Namn:
\Hfill
\normalsize \smsymbol
}

\minipagebegin{[t]}{0.5\textwidth - 1ex}

\grundegenskaper

\erfarenhet

\next\minipagebegin{[t][0.85\textheight]}{0.5\textwidth - 1ex}

\utrustning

\Large\hfill\smsymbol\large

\columnend
