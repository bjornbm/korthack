\REM{Ibland kommer du att behöva göra \intro{grundegenskapsslag} (\GE-\intro{slag}).
Då slår du en tärning som bestäms av värdet på en av dina \GE{}:

\REM{Ibland kommer du att behöva göra ett \intro{grundegenskapsslag} (\GE-\intro{slag}) för att se hur väl du hanterar en situation.
Då slår du en tärning som bestäms av värdet på din \REM{för situationen} mest relevanta \GE{}:}

\hfill{} 1: \D4,
\hfill{} 2: \D6,
\hfill{} 3: \D8,
\hfill{} 4: \D10,
\hfill{}≥5: \D12.
\hfill{}

Om omständigheter ger dig betydande \intro{fördel} så får du +1 på tärningens resultat.
Betydande \intro{nackdel} ger dig \minus{1}.
Dessa är inte kumulativa och \intro{nackdel} trumfar \intro{fördel}.

\REM{Utrustning såsom \vapenx{} kan ge ytterligare modifikationer.}

Till exempel så gör du ett \KRP-\intro{slag} varje gång du attackerar någon för att se hur mycket \skadax{} hen får.
I andra sammanhang kan ett \GE-\intro{slag} avgöra om du lyckas med något svårt; då måste du slå ≥4 för att lyckas.

\REM{När du attackerar någon gör du ett \GE-slag (vanligtvis ett \KRP-slag); resultatet är den \skadax{} din fiende får.
I andra fall gör du ett \GE-slag för att se om du lyckas med något \svårt{}; då slår du för din mest relevanta \GE{} och måste få 4 eller högre för att lyckas.

När du försöker göra någonting \svårtx{} gör du ett \GE-slag för din mest relevanta \GE{}; om du slår 4 eller högre så lyckas du.}


# Grundegenskapsslag 2

\REM{Ibland kommer du att behöva slå för en \GE-\intro{slag}).
Då slår du en tärning som bestäms av värdet på en av dina \GE{}:}

Ibland kommer du att behöva \intro{slå för} en \GE för att se hur väl du hanterar en situation.
Då slår du en tärning som bestäms av värdet på din för situationen mest relevanta \GE{}:

\hfill{} 1: \D4,
\hfill{} 2: \D6,
\hfill{} 3: \D8,
\hfill{} 4: \D10,
\hfill{}≥5: \D12.
\hfill{}

Resultatet är ett mått på hur bra det går.
Om du bara behöver veta huruvida du lyckas så gör du det på ett resultat ≥4.
\REM{
Om du bara behöver veta huruvida du lyckas så betyder ett resultat ≥4 att du lyckas.
}

\REM{
Om omständigheter ger dig betydande \intro{fördel} så får du +1 på tärningens resultat.
Betydande \intro{nackdel} ger dig \minus{1}.
Dessa är inte kumulativa och \intro{nackdel} trumfar \intro{fördel}.
}

Om omständigheter ger dig betydande \intro{fördel} eller \intro{nackdel} så slår du två tärningar och använder det högsta respektive lägsta resultatet.
\intro{Nackdel} trumfar \intro{fördel}.

\REM{Utrustning såsom \vapenx{} kan ge ytterligare modifikationer.}


# Grundegenskapsslag 2

Ibland kommer du att behöva \intro{slå för} en \GE för att se huruvida du lyckas med något.
För att lyckas måste du slå ≥4 med en tärning som bestäms av värdet på din \REM{för situationen} mest relevanta \GE{}:
\REM{Då lyckas du om du slår ≥4 med en tärning som bestäms av värdet på din för situationen mest relevanta \GE{}:}

\hfill{} 1: \D4,
\hfill{} 2: \D6,
\hfill{} 3: \D8,
\hfill{} 4: \D10,
\hfill{}≥5: \D12.
\hfill{}

\REM{
Om du istället bara vill veta hur väl du gör något så \intro{slår} du en \GE{}.
Då slår du en tärning på samma sätt men använder resultatet som ett mått på hur bra det går.
}
\REM{
Om du istället bara vill veta _hur väl_ du gör något så slår du en tärning på samma sätt men använder resultatet som ett mått på hur bra det går. Det kallas att \intro{slå} en \GE{}.
}

Om du istället bara behöver ha ett mått på _hur väl_ du gör något så använder du tärningens resultatet. Det kallas att \intro{slå} en \GE{}.

Om omständigheter ger dig betydande \intro{fördel} eller \intro{nackdel} så slår du två tärningar och använder det högsta respektive lägsta resultatet.
\intro{Nackdel} trumfar \intro{fördel}.

# Grundegenskapsslag 3

För att avgöra hur väl du hanterar en situation kan du behöva slå en tärning som bestäms av värdet på din \REM{för situationen} mest relevanta \GE{}:

\hfill{} 1: \D4,
\hfill{} 2: \D6,
\hfill{} 3: \D8,
\hfill{} 4: \D10,
\hfill{}≥5: \D12.
\hfill{}

\REM{Resultatet (skrivs [\GE]) är ett mått på hur bra det går.
Om du bara behöver veta om du lyckas eller misslyckas så lyckas du om resultatet är ≥4.
}

Tärningens resultat är ett mått på hur bra det går och betecknas \sGE.
Om du bara vill veta huruvida du lyckas med något säger man att du \intro{prövar} en \GE och  du lyckas om \sGE{}≥4.

\REM{
Det kallas att göra ett \intro{\GE-slag} och resultatet \ex{skrivs [\GE]} är ett mått på hur bra det går.

Om du bara vill veta om du lyckas med något säger man att du \intro{prövar} en \GE och  du lyckas om [\GE]≥4.

Att göra ett \GE-slag för att avgöra om du lyckas eller misslyckas med något kallas att \intro{pröva} en \GE. Då lyckas du om [\GE]≥4.

Om du bara slår för att se om du lyckas eller misslyckas med något kallas det att du \intro{prövar} en \GE.
Då lyckas du om [\GE]≥4.

Ofta kommer du att behöva ett resultat ≥4 för att lyckas med något.
}

Om omständigheter ger dig betydande \intro{fördel} eller \intro{nackdel} så slår du två tärningar och använder det högsta respektive lägsta resultatet.
\intro{Nackdel} trumfar \intro{fördel}.


# Grundegenskapsslag 4
}

För att avgöra om du lyckas med något kan du behöva \intro{slå för} en \GE{}.
Då slår du en \REM{6-sidig} tärning och lägger till din mest relevanta \GE{}.
Om resultatet blir ≥6 så \intro{lyckas} du\REM{ och om resultatet blir ≥8 så lyckas du \intro{exceptionellt bra}},
annars \intro{misslyckas} du.

\REM{
För att avgöra huruvida du lyckas med något kan du behöva \intro{slå för} en \GE{}.
Då slår du en tärning och lägger till din för situationen mest relevanta \GE{}.
Om resultatet blir ≥6 så \intro{lyckas} du.
}

\REM{
* Om resultatet blir ≤5 så \intro{misslyckas} du.
* Om resultatet blir ≥6 så \intro{lyckas} du.
* Om resultatet blir ≥8 så \intro{lyckas} du \intro{exceptionellt bra}.
}

\REM{Du lyckas dock alltid om du slår en sexa, och misslyckas alltid om du slår en etta.}

Om du försöker igen med något som du misslyckades med på ditt förra \drag{} så får du +1 på resultatet, kumulativt tills du lyckas.

Om omständigheter ger dig betydande \intro{fördel} så får du +2 på resultatet.
Betydande \intro{nackdel} ger dig \minus{2}.
Dessa kumuleras inte och \keyI{nackdel} trumfar \keyI{fördel}.

\REM{
Genom att använda ditt \dragx{} till att hjälpa eller motarbeta någon kan du ge henom \intro{fördel} respektive \intro{nackdel} på hens \drag{}.

Genom att på ditt \dragx{} hjälpa eller motarbeta någon \ex{förklara hur du gör!} kan du ge henom \intro{fördel} eller \intro{nackdel} på hens \drag{}.
}
