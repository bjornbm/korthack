I detta rollspel antar du och de flesta av dina medspelare rollen av varsin karaktär, en så kallad \introA{rollperson}{rp}, i en fantasivärld.
En av er, \introA{spelledaren}{sl}, antar rollen av alla andra karaktärer\REM{ i världen}, så kallade \introA{spelledarpersoner}{slp}, samt världen i övrigt.
\introSl{} beskriver världen och de situationer \RP{} befinner sig i.
Du beskriver hur du \REM{din \introRP{}} önskar agera i dessa situationer.

Vad du/din \RP{} kan göra styrs till viss del av ett regelverk och,
om det inte är givet huruvida du bör \lyckasx{} med det du företar dig, 
av \xr{tärningar}{tärningsslag}.
När regelverket är otillräckligt eller dess tillämpning oklart avgör \introSL{} hur situationen ska hanteras.

\REM{
När regelverket är otillräckligt eller \REM{dess tillämpning oklart} otydligt avgör \introSL{} hur situationen ska hanteras.
}

\REM{
Det här spelet innehåller ingen grundlig introduktion till rollspel utan det förutsätts att \introSL{} har tidigare erfarenhet av rollspel och fantasy och kan introducera nybörjarspelare till hobbyn samt extrapolera informationen som ges här efter behov.
}

\REM{
Varför jag skriver:

Jag skriver för en erfaren spelledare som vill ha ett tajt pch snabbt system som erbjuder intressanta spelarana intressanta taktiska val och resurshantering.
}

