#### ”Mothershippish”

\
\small
*Björn Buckwalter, 2020-01-28*
\normalsize

\small
\noindent
*Delvis © 2018 Tuesday Knight Games*
\normalsize
\
\

\REM{
\begin{centering}

For your eyes only.

\textbf{DO NOT DISTRIBUTE}

\end{centering}
}


#### Din rollperson

Skapa en rollperson med hjälp av den här sidan
\ex{efterföljande sidor \REM{samt \Mothership{}} är frivillig läsning}.
Markera dina val här på sidan eller skriv ner dem på annorstädes.
\REM{Markera dina val här på sidan eller skriv ner dem på ett annat papper.}

1. Välj en klass och hitta på ett namn.
2. Välj ett utrustningspaket.
3. Välj en specialisering.
4. Slumpa dina krediter (5\D10\times{}10).
4. Du börjar med 2 Stress och 0 Resolve.



#### Klasser

\Include{Classes}


#### Utrustningspaket

\Include{Loadouts}



\break


\card{Introduktion}


\card{Grundegenskaper}
\noindent
\Include{Tärningsslag}




#### Specialiseringar

När du gör något du har en \intro{specialisering} i får du alltid en \traff{}.
Det innebär att du ofta \lyckasx{} automatiskt och bara behöver slå tärningar om det är viktigt att få flera \traffar{}\REM{då lägger du till en \traff{} till resultatet}.
Se \Mothership{} sidan 5 för exempel på specialiseringar.


#### Saves

You make \intro{Saves} when something bad might happen to you and you need to find out whether you resist it.

* \intro{Sanity} (\slafor{\HVD}) to explain away logical inconsistencies in the universe, rationalize and make sense out of chaos, detect illusions and mimicry and think quickly under pressure.
* \intro{Fear} (\slafor{\UTS}) to cope with emotional trauma, and covers not only fear, but also loneliness, depression or any other emotional surge.
* \intro{Body} (\slafor{\KRP}) for reflexes and to resist hunger, disease or any other organism that might attempt to invade your insides.


\card{Försvarspoäng}


\break


\card{Turordning}

\NOcard{Ledare}

\card{Strider}


#### Stress

Gain Stress whenever you:

* Fail a Save
* Lose an \FP{} or get knocked unconscious
* Go without rest, food or water
* Are near a Scientist failing a Sanity save
* Are near an Officer failing a Fear save
* Are near a Marine who panics
* Are in a ship taking a hit
* Experience a near miss with an asteroid and other massive object

\noindent
Minska Stress genom att vila i minst sex timmar
(\slafor{\UTS} och minska med antalet \traffar{}).


#### Panik

Make a Panic check whenever you:

* First encounter a horrific creature
* Lose more than 1⁄2 your \FP{} in one hit
* See a crewmember die
* See more than one crewmember Panic
* When all hope seems lost \REM{and death seems certain}
* Your ship loses more than 1⁄2 its Hull

\noindent
Slå 2\D{10}.
Om resultatet är högre än din stress så minskar din Stress med ett.
Annars får du panik och slår 2\D{10}+Stress\minus{}\intro{Resolve} på *Panic Effect Table* (\Mothership{} sidan 26).


\NOcard{Stridmoral} \REM{ Hanteras med stress }


\card{Utrustning}


#### Erfarenhet

Efter varje spelmöte du överlever får du ett \introA{erfarenhetspoäng}{ep} och får välja mellan att

1.  få en Resolve (upp till maximalt 5),
2.  bli av med en fobi eller ett beroende,
3.  bli av med all din Stress.

\noindent
Du kan öka en \GE{} med ett genom att spendera lika många \EP{} som dess nuvarande värde, eller
lära dig en ny specialisering för en \EP{}.



\includepdf{../Equipment.pdf}
