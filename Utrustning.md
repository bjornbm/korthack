Utöver dina kläder, en väska och dina pengar \REM{\introA{silvermynt}{\smsymbol}} så kan du bära upp till åtta saker varav maximalt \KRP{} får vara \intro{tunga}.
Bär du mer än så kan du inte göra någonting atletiskt \ex{springa, strida, klättra …}.

\hyphenation{läger-utrustning}

Föremål som hör ihop \ex{\SCIFI{vapen och hölster, vaccsuit med tillbehör}
\FANTASY{svärd och skida, pilbåge och pilar, lägerutrustning\REM{inkluderar tält, filtar, kokkärl …}} …}
samt \FANTASY{kastvapen och}
förbrukningsvaror av samma slag räknas som en sak.
\FANTASY{Vidare så räcker det att en av er bär med sig mat, vatten, \facklorx{} och andra förbrukningsvaror för att det ska räcka till ordinarie användning för er alla.
Det gäller också för läger-\REM{utrustning} och klätterutrustning\REM{inkluderar rep, änterhakar, hammare, pitonger…}.

En lykta eller fackla ger ljus till tre personer inklusive bäraren. Vaxljus och oljelampor ger ljus till två \REM{personer} men måste bäras försiktigt.
}

\SCIFI{
\REM{En ficklampa eller pannlampa ger ljus till två personer inklusive bäraren.}
\REM{En *flare* lyser upp allt inom \narax{} avstånd.}

\REM{ I Försvarspoäng.md av layout-skäl
En *Vaccsuit* (\tungx{}) eller en *Standard Battle Dress* ger dig en extra \FP{}. En *Advanced Battle Dress* (\tungx{}) ger dig två. \FP{} från dräkter återfås omedelbart efter \strider{}.
}

Med ett \intro{automatvapen} slår du dubbelt så många tärningar men måste avvara en \traffx{} om du vill undvika att tömma magget.

\intro{\Tungax{} vapen}\ex{pulse rifle, flame thrower …} låter dig räkna varje \traff{} dubbelt.

\intro{Kastvapen} kan användas på \keyI{nära} avstånd.
En *frag grenade* skadar alla inom 10 meter när den exploderar (\slafor{4} för var och en).


Med ett \intro{improviserat vapen} får du varken \foljaupp{} \REM{\attacker{}} eller göra \motattacker{}.
Om du är \intro{obeväpnad} gäller detsamma samt att väpnade närstrids\attacker{} mot dig får \fordel{}.
\REM{
I närkamp räknas \skjutvapen{} som improviserade.
}

}


\cred{
  OSH för (mkt) tunga föremål.
  https://blog.thesconesalone.com/2018/07/a-simple-resource-management-system-for.html för 8 föremål.
}
