#!/bin/bash

set -o nounset
set -o errexit
#set -o verbose

date=$(date +%Y-%m-%d)

function pocketmod {
  cwd=$(pwd)
  cd "$HOME/repos/pocketmod"
  ./pocketmod.sh "$cwd/out/$date stack 1L.pdf" flipbook
  cp booklet.pdf "$cwd/out/$date booklet $1.pdf"
  cd "$cwd"
  #open "out/$date booklet $1.pdf"
}

./make.sh cards stack 1L 1-8
pocketmod 1

./make.sh cards stack 1L 9-16
pocketmod 2

pdftk "out/$date booklet 1.pdf" "out/$date booklet 2.pdf" output "out/$date booklets.pdf"
open "out/$date booklets.pdf"
