\REM{
\hyphenation{gren-ade}
\hyphenation{re-breath-er}
\hyphenation{camp-ing}
}

A.  \intro{Excavation}:
    Crowbar, Hand Welder, Laser Cutter \ex{\tungx}, Body Cam, Bioscanner, Infrared Goggles, Lockpick Set, Vaccsuit (Oxygen Tank, Mag-Boots, Short-range Comms) \ex{\tungx}.

B.  \intro{Exploration}:
    Vibechete, Rigging Gun, Flare Gun, First Aid Kit, Vaccsuit (Long-range Comms, Oxygen Tank) \ex{\tungx{}}, Survey Kit, Water Filter, Locator, Rebreather, Binoculars, Flashlight, Camping Gear, MREs\times{}7.

C.  \intro{Extermination}:
    SMG (3 magazines), Frag Grenade\times{}6, Standard Battle Dress (Heads-up Display, Body Cam, Short-range Comms), Stimpak\times{}6, Electronic Tool Kit.

D.  \intro{Examination}:
    Scalpel, Tranq Pistol, Stun Baton, Hazard Suit, Medscanner, Automed\times{}6, Pain Pills\times{}6, Stimpak\times{}6, Cybernetic Diagnostic Scanner.

\noindent
Se sista sidan samt \Mothership{} sidorna 13–16 för mer information om utrustningen.
\SL{} anpassar regelmekaniska effekter till det här regelsystemet efter behov.
