Ni kan utse en \RP{} till er \intro{ledare}.
Hen kan som sitt \dragx{} ge \UTS{} av er order för era nästa \drag{}.
Var och en som följer hens order får \fordelx{} \REM{när de \slarfor{\GE}} på sitt \drag{}.
\REM{Ingen är bunden att följa hens order men var och en som gör det får \fordelx{} \REM{när de \slarfor{\GE}} på sitt \drag{}.}

\REM{
på \REM{ett} \term{tärningsslag}{grundegenskapsslag} på sitt \drag{}.
}
