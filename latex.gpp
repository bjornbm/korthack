\define{\REM}{}

\REM{

  The REM macro is used for comments, such as this.

  Macros can be defined either with GPP (\define) or in LaTeX
  (\newcommand) directly. The syntax is almost identical, but for
  LaTeX the number of parameters must be specified (e.g., [1]).

  The advantage of LaTeX macros is that they don't mess with table
  column widths with respect to the markdown source. A disadvantage
  is that a \newcommand without arguments needs to be followed by
  "{}" to not gobble up whitespace.
}

\REM{
  Alias for compatibility with gpp.
  Perhaps not needed (could alias newcommand for gpp instead)
}
\let\Newcommand\newcommand

\REM{ Command to "hide" LaTeX contents from Pandoc }
\newcommand{\latex}[1]{#1}

\REM{ Command for contents ONLY for LaTeX output. }
\define{\latexonly}{#1}

\REM{ Command for contents ONLY for wiki output. }
\define{\wikionly}{}


\REM{ ****************************
      *       Layout macros      *
      **************************** }


\REM{
  The following code reduces whitespace around section headings.
  The code is taken from [1] and modified to pass properly through
  GPP(?) and pandoc.
  [1]: https://tex.stackexchange.com/a/108824/157211
}

  \newcommand\cardsection[1]{
    \Needspace*{62mm}
    \origsection{#1}
    \vspace{-1ex}
    \vspace{-0.5\baselineskip} \REM{Remove this once `tectonic` updates parskip (https://github.com/tectonic-typesetting/tectonic/issues/377).}
  }

  \newcommand\cardsectionstar[1]{
    \Needspace*{62mm}
    \origsection*{#1}
    \vspace{-1ex}
    \vspace{-0.5\baselineskip} \REM{Remove this once `tectonic` updates parskip (https://github.com/tectonic-typesetting/tectonic/issues/377).}
  }

\REM{
  \let\origsection\section
  \makeatletter
  \renewcommand\section{\@@ifstar{\cardsectionstar}{\cardsection}}
  \makeatother
}

\REM{ End of section heading whitespace code. }

\REM{ Don't show top and bottom rules on tables. }
  \let\originaltoprule\toprule
  \let\originalbottomrule\bottomrule
  \renewcommand{\toprule}{\originaltoprule[0pt]}
  \renewcommand{\bottomrule}{\originalbottomrule[0pt]}

\REM{ Reduce whitespace above and below tables.
  \setlength{\abovetopsep}{-2ex}
  \setlength{\belowbottomsep}{-3.5ex}
}

\REM{ No space between paragraphs
  \setlength{\parskip}{0ex plus 0.0ex minus 0.0ex}
}

\REM{ No page numbering }
  \pagestyle{empty}


\REM{ Phantom space for alignment }
  \newcommand{\hh}[1]{\phantom{#1}}
  \newcommand{\p}[1]{\phantom{#1}}



\REM{
  Substitute for GPP's broken built-in \include macro.
  Note that this one will of course not search include
  directories so specify path if necessary.
}



\define{\Include{namn}}{\defeval{x}{\exec{cat "\namn.md"}}\x}

\REM{
  Create a card with given title.
}
\define{\card{namn}{ref}}{
\ifeq{\dobreak}{yes}\newpage\endif

#### \exec{basename -s md "\namn"}

\Include{\namn}
}


\REM{
  Create a card with given title and No Number.
}
\define{\cardNN{namn}{ref}}{
\ifeq{\dobreak}{yes}\newpage\endif

# \exec{basename -s md "\namn" | tr -d '\n'} {-}

\Include{\namn}
}


\REM{ Section (card) references }

  \define{\secx}{\small^[#1]^\normalsize\phantom{}}
  \define{\sec}{\secx{[-@@sec:#1]}}
  \REM{
    \define{\xr}{[#1](#sec:#2)\sec{#2}}
  }
  \define{\xr}{[#1](#sec:#2)}

  \define{\intro}{**#1**}
  \define{\termI}{#1}
  \define{\term}{[#1](#sec:#2)}

  \define{\keyI}{_\termI{#1}_}
  \define{\key}{_\term{#1}{#2}_}
  \REM{
    \define{\termx}{[#1](#sec:#2)\sec{#2}}
    \define{\keyx}{_\term{#1}{#2}_\sec{#2}}
  }
  \define{\key}{\term{#1}{#2}}
  \define{\keyI}{\termI{#1}}
  \define{\termx}{\term}
  \define{\keyx}{\key}

\REM{ \newcommand{\ex}[1]{{\small{(\emph{#1})}}} }
\define{\ex}{\small(_#1_)\normalsize\phantom{}}
\newcommand{\TAG}[1]{\textsc{#1}}
\newcommand{\introge}[1]{\textsc{#1}}

\REM{ Hyphen hints hidden from pandoc. }
\newcommand{\br}{\-}


\REM{ Valuta }

\newcommand{\smsymbol}{$\mathfrak{s}$}
\newcommand{\sm}[1]{#1\thinspace\smsymbol}

\define{\ggr}{\latex{$\times$}}

\REM{ Tärningar }
\Newcommand{\D}{\textsc{t}}


\REM{ Resultat }

\define{\K}{$\mathbb{K}$}
\define{\M}{$\mathbb{M}$}
\define{\L}{$\mathbb{L}$}
\define{\E}{$\mathbb{E}$}

\define{\K}{$\mdblkdiamond$}
\define{\M}{$\mdwhtdiamond$}
\define{\L}{$\medwhitestar$}
\define{\E}{$\medblackstar$}

\define{\K}{\symbol{"24C0}}
\define{\M}{\symbol{"24C2}}
\define{\L}{\symbol{"24C1}}
\define{\E}{\symbol{"24BA}}


\REM{ Pil för eskalering }
\newcommand{\ra}[1]{$\rightarrow$#1}

\REM{ Markeringar }

\Newcommand{\checkO}{$\mdlgwhtsquare$}
\Newcommand{\checkS}{$\boxbslash$}
\Newcommand{\checkH}{$\boxtimes$}
\Newcommand{\checkD}{$\mdlgblksquare$}
\Newcommand{\checkEP}{$\squoval$}


\REM{ Include macros common to all formats }
\include{macros.gpp}

