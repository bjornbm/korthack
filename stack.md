\REM{##### FRONT MATTER #####}

\REM{
\noindent
![The Book of Romance (1902)](../bilder/HJF_battle_top.png "Henry Justice Ford"){ width=100% }\hfill

\noindent
![The Book of Romance (1902)](../bilder/HJF_battle.png "Henry Justice Ford")
REM}

### Den heliga armén


\small
*Björn Buckwalter, 2020-01-16*
\normalsize
\
\
\

\noindent
För ett år sedan regnade eld från himlen ned väster om bergen.
Skådespelet pågick i tre dagar och kunde ses ända från huvudstaden på de bördiga slättlanden i Öster.

Rikets visaste män och skickligaste siare är eniga med Solkyrkans överstepräster om järtecknets innebörd:
Frön av ondska, komna till jorden på brinnande skepp, har såtts i hedningalanden i väster.
Nu växer där en mörk här med syfte att förinta alla Sols folk och, därigenom, ända Sols heliga herravälde över himlen.

För att möta detta existentiella hot rustar Kungen en armé och har ålagt sina vasaller och rikets alla städer och byar att bistå med manskap och krigsmateriel.
Du är en av dem som rådet i din bergsby sänt för att fylla byns kvot.

\REM{
Brinnande skepp från natthimlen har sått frön av Solfrånvänd ondska i hedningalanden i väster.
Nu växer där en mörk här med syfte att förinta alla Sols folk och därigenom ända Sols herravälde över himlen.

\noindent
För två somrar sedan lystes himlen väster om VÄSTbergen upp av meteorer som regnade ner i brinnande stråk.
Skådespelet varade i tre dygn
och under veckorna och månaderna som följde nåddes huvudstaden på de bördiga slättlanden i öster av fler och fler oroande rapporter från bergen. 

Efter veckor av samråd med sina visaste rådgivare och skickligaste siare deklarerade Kung XXX tillsammans med Solkyrkans överstepräster att fröna till en här av \REM{Solfrånvänt} mörker kommit till jorden på brinnande skepp. Nu gror den \REM{bland de solfrånvända hedningarna i väster} med syfte att förinta alla Sols folk och därigenom bringa Sols herravälde över himlen till ända.

För att möta detta existentiella hot rustar Kungen en armé och har ålagt alla sina vasaller och rikets alla städer och byar att bistå honom med manskap och materiel.
Du är en av dem som rådet i din hemby i VÄSTbergen sänt för att fylla byns kvot.

Efter flera veckors samråd med sina visaste rådgivare och skickligaste siare deklarerade Kung XXX tillsammans med Solkyrkans överstepräster att brinnande skepp från natthimmlens mörker sått frön av Solfrånvänd ondska i hedningalanden i väster.
Nu växer där en solfrånvänd här med syfte att förinta alla Sols folk och därigenom ända Sols herravälde över himlen.

Efter flera veckors samråd med sina visaste rådgivare och skickligaste siare deklarerade Kung XXX tillsammans med Solkyrkans överstepräster att frön av Solfrånvänd ondska, på brinnande skepp sända till jorden ur natthimlens mörker, såtts i hedningalanden i väster.
Nu växer där en solfrånvänd här med syfte att förinta alla Sols folk och därigenom ända Sols herravälde över himlen.

Efter flera veckors samråd med sina visaste rådgivare, skickligaste siare och Solkyrkans överstepräster deklarerade Kung XXX att frön av ondska, komna till jorden på brinnande skepp, har såtts i väster.
Nu växer där en mörk och Solfrånvänd här i styrka, med syfte att förinta alla Sols folk och därigenom ända Sols herravälde över himlen.

Kung XXX kallade sina visaste rådgivare och skickligaste siare att tillsammans med Solkyrkans överstepräster utröna innebörden av dessa järtecken.

Rådets slutsats blev att frön av ondska, transporterade till jorden på brinnande skepp, har såtts i väster. Nu växer där en mörk och solfrånvänd här i styrka, med syfte att förinta alla Sols folk och därigenom ända Sols herravälde över himlen.
}

\card{Introduktion}

\REM{
och Kung XXX kallade samman sina visaste rådgivare och skickligaste siare att utröna innebörden av detta järtecken.

I huvudstaden på de bördiga slättlanden i öster kallade 

Skådespelet varade i tre dagar och Kung XXX kallade samman sina visaste rådgivare och skickligaste siare samt Solkyrkans överstepräster att utröna dess innebörd.

Järtecknets natur samt oroande vittnesmål ledde till slutsatsen att frön av ondska, transporterade till jorden på brinnande skepp, har såtts i väster. En mörk och solfrånvänd här, med syfte att förinta alla Sols folk och därigenom ända Sols herravälde över himlen, växer där i styrka.

Kungen, övertygad om att detta existentiella hot måste mötas, börjar förbereda för krig och kallar samman och rustar en armé från rikets alla hörn. Detta arbete har pågått i två år nu och armén är på väg till och genom bergen där rekrytering fortgår.

Kung XXX samlar en armé för att möta det växande hotet och har ålagt alla rikets städer och byar att bidra med manskap och materiel till hans styrkor.
Du är en av dem som byrådet i din hemby i VÄSTbergen sänt för att fylla byns kvot.
}

\break

#### Din rollperson

\define{\tabell2}{

\noindent
\minipagebegin{[t][8\baselineskip][t]}{1\columnwidth}

#1

#2

\minipageend{}\hspace{-0.5\columnwidth}\minipagebegin{[t][8\baselineskip]}{0.5\columnwidth}
\hfill

\hfill

#3

\minipageend{}

\vspace{-0.6666666667\baselineskip}
}

\define{\tabell}{

\noindent
\minipagebegin{[t][7\baselineskip][t]}{1\columnwidth}

#1

#2

\minipageend{}\hspace{-0.5\columnwidth}\minipagebegin{[t][7\baselineskip]}{0.5\columnwidth}
\hfill

#3

\minipageend{}

\vspace{-0.6666666667\baselineskip}
}


Slumpa fram din \RP{} med listorna nedan.
Gör ett slag per lista.

\REM{
\tabell{Du heter \ex{kombinera resultaten} …}{

1.  Nol-
1.  Arn-
1.  Hall-
1.  Bry-
1.  Thir-
1.  Stul-

}{

1.  -feid/-frid
1.  -grim/-grunn
1.  -mund/-rana
1.  -vard/-veig
1.  -drik/-gerd
1.  -bera/-bella

}
}

\tabell{Du kommer från byn \ex{kombinera resultaten} …}{

1.  Troll-
1.  Berg-
1.  Get-
1.  Varg-
1.  Bond-
1.  Sten-

}{

1.  -kvarn
1.  -dammen
1.  -klyfta
1.  -åsen
1.  -fallet
1.  -mossen

}


\tabell{
Där arbetade du som \ex{slå på vänstra listan} …
}{

\REM{1.  bonde}
1.  dräng/piga
1.  herde
1.  jägare
1.  skogshuggare
1.  stenhuggare
1.  \ex{slå på högra listan}

}{

1.  snickare
1.  skräddare
1.  underhållare
1.  handelsbiträde
1.  ministrant \REM{korgosse}
1.  dödgrävare

}

\REM{Du mönstrade \REM{för din bys räkning} för att …}
\tabell{Du tjänstgör i byns soldatrote för att …}{

1.  byrådet utsåg dig genom lottning.
1.  det är din plikt.
1.  vinna frihet från träldom.
1.  du vill se världen.
1.  du vill imponera på någon.
1.  bli förmögen genom plundring.

}

\tabell{Byn rustade dig med …}{

1.  ett svärd
1.  en stridsyxa
1.  ett spjut \ex{\langt{}}
1.  ett kortspjut
1.  \REM{fyra} kastspjut
1.  en pilbåge

}{

1.  en hjälm
1.  en sköld
1.  en rustning
1.  en brandflaska
1.  en rökkruka
1.  ett bländskott

}

\tabell{Dina föräldrar gav dig …}{

1.  en skyddsamulett \ex{ignorera en \attack{}}
1.  en lyckotalisman \ex{få en automatisk \traff{}}
1.  en relik         \ex{sätter skräck i onda väsen?}
1.  en styrketår     \ex{återfå alla dina \FP{}}
1.  en salvekur\REM{-omslag}   \ex{läk en skada över natten}
1.  en silverdolk    \ex{effektiv mot onda väsen?}
\REM{1.  en påse med måndamm}
}

\tabell{Dessutom äger du …}{

1.  en oljelykta
1.  en spade
1.  penna & papper
1.  två stora säckar
1.  en kompass
1.  en tång

}{

1.  en spegel
1.  en visselpipa
1.  vaxljus
1.  kritor
1.  två vinflaskor
1.  20 silvermynt

}

\REM{
\tabell{Du …}{

1.  är välgödd
1.  är utmärglad
1.  är lång
1.  är liten
1.  är senig
1.  är låghalt

}{

1.  har stora ögon
1.  har ärrat ansikte
1.  har snett leende
1.  saknar 4 tänder
1.  saknar 2 fingrar
1.  är hårig

}
}


\break

\card{Introduktion}
\NOcard{Förkortningar}


\REM{##### REGLER #####}




\card{Grundegenskaper}
\noindent
\Include{Tärningsslag}
\Include{Erfarenhet}


\card{Försvarspoäng}

\card{Turordning}

\card{Ledare}

\card{Följeslagare}

\break

\card{Strider}

\card{Stridsmoral}

\break

\card{Utrustning}
\Include{Beväpning}



\REM{#### ÖVRIGT (ej booklet) #####}

\NOcard{Besvärjelser}

\NOcard{Hyrd hjälp}



\REM{
### Anteckningar och husregler

.\dotfill\
\
\noindent.\dotfill\
\
\noindent.\dotfill\
\
\noindent.\dotfill\
\
\noindent.\dotfill\
\
\noindent.\dotfill\
\
\noindent.\dotfill\
}



\NOcard{Ljuskällor}

\NOcard{Prislista}
\NOcard{Tips}

\REM{##### ROLLPERSONER #####}

\NOcard{Skapa din rollperson}
\NOcard{Bakgrund}

\NOcard{Startutrustning}

\NOcard{Namn}


\REM{##### END MATTER #####}

\NOcard{Förkortningar}
\NOcard{Källor}

\NOinclude{magi/krafter.md}
\NOinclude{slbok.md}


\REM{
\noindent
![The Book of Romance (1902)](../bilder/HJF_battle_bottom.png "Henry Justice Ford"){ width=100% }\hfill

\vfill
\noindent
\p{}\hfill
![Lathgertha? (190x?)](../bilder/Lathgertha.png "Artist?"){width=40%}
\hfill\p{}
REM}

