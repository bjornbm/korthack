\REM{

# Startutrustning {#startutrustning}

Du har
enkla kläder,
ett bälte,
en ryggsäck,
en yllefilt,
ett vattenskinn,
ett elddon,
\sm{\D100},
\D4 dagransoner mat,
ett valfritt närstridsvapen
och en valfri mystisk kraft\sec{krafter}.
Välj därtill:

1.  Ett valfritt vapen,
    rustning,
    sköld,
    slipsten,
    signalhorn,
    hammare,
    3\D6 järnspikar,
    \D4 facklor,
    en flaska brännvin.
2.  \D4 kastvapen,
    dyrkar,
    fotanglar,
    snubbeltråd,
    rep,
    änterhake,
    riktad lykta,
    \D4 oljeflaskor,
    mörk kåpa,
    säck,
    kofot.
3.  Ett skjutvapen med 2\D6 projektiler,
    \D4 snaror,
    fångstnät,
    fiskelina och krokar,
    visselpipa,
    spade,
    krita,
    \D4 facklor.
4.  Imponerande klädsel och smycken,
    signetring,
    lack,
    penna,
    bläck,
    pergament,
    stålspegel,
    \D6 ljus,
    \D4 flaskor fint vin.

Köp kompletterande utrustning från prislistan\sec{prislista} om du önskar.

}

\REM{


# Startutrustning {#startutrustning}

Du har
enkla kläder,
ett bälte,
en ryggsäck,
en yllefilt,
ett vattenskinn,
ett elddon,
\sm{\D100},
\D4 dagransoner mat,
ett lätt eller långt närstridsvapen
och en valfri mystisk kraft\sec{krafter}.
Välj därtill:

1.  \Tungt{} vapen,
    rustning,
    sköld,
    slipsten,
    horn,
    \D4 facklor.
2.  \Tungt{} skjutvapen,
    koger,
    3\D6 pilar,
    rustning,
    \D4 facklor.
3.  \D4 kastvapen,
    dyrkar,
    fotanglar,
    riktad lykta,
    oljeflaska.
4.  Skjutvapen,
    2\D6 projektiler,
    nät,
    \D4 snaror,
    \D4 facklor.
5.  Imponerande klädsel och smycken,
    signetring,
    \D4 vaxljus.

a.  Hammare,
    3\D6 järnspikar,
    rep,
    änterhake,
    visselpipa.
b.  snubbeltråd,
    säck,
    kofot.
    visselpipa,
    spade,
    krita,
c.  Penna,
    bläck,
    pergament,
    lack,
    stålspegel,
    kortlek.

}

\REM{


# Startutrustning {#startutrustning}

Du har
enkla kläder,
ett bälte,
en ryggsäck,
en yllefilt,
ett vattenskinn,
ett elddon,
\sm{\D100},
\D4 facklor,
\D4 dagransoner mat,
ett lätt eller långt vapen\sec{vapen}
och en valfri mystisk kraft\sec{krafter}.
Välj därtill en från varje kategori:

A.  \Tungt{} vapen,
    \tungt{} skjutvapen med 3\D6 projektiler,
    dyrkar och fotanglar,
    fångstnät och \D4 snaror,
    imponerande klädsel och smycken,
    ett riddjur,
    ett lastdjur,
    ett vaktdjur.
B.  Rustning,
    skjutvapen med 2\D6 projektiler,
    riktad lykta och \D4 oljeflaskor,
    penna med bläckhorn och pergament.
C.  Sköld,
    \D4 kastvapen,
    hammare och 3\D6 järnspikar,
    \D4 flaskor fint vin,
    fiskelina och krokar,
    krita,
    kortlek,
    \D6 vaxljus.
D.  Rep med änterhake,
    kofot,
    spade,
    visselpipa,
    stålspegel.

}

\REM{


# Startutrustning {#startutrustning}

Du har
enkla kläder,
ett bälte,
en ryggsäck,
en yllefilt,
ett vattenskinn,
ett elddon,
\sm{\D100},
\D4 dagransoner mat,
\D4 facklor,
ett lätt eller långt vapen\sec{vapen}
och en valfri mystisk kraft\sec{krafter}.
Välj därtill:

A.  \Tungt{} vapen,
    rustning,
    sköld,
    horn,
    hammare, 3\D6 spikar.
B.  \Tungt{} skjutvapen, 3\D6 projektiler,
    rustning,
    krita,
    kortlek.
D.  \D4 kastvapen,
    dyrkar,
    fotanglar,
    snubbeltråd,
    riktad lykta, \D4 oljeflaskor,
    rep, änterhake,
    kofot,
    säck,
    mörk kåpa.
C.  Skjutvapen, 2\D6 projektiler,
    \D4 snaror,
    nät,
    fiskelina, metkrokar,
    visselpipa,
    spade,
    rep,
    säck,
    tält.
E.  Imponerande klädsel och smycken,
    penna, bläckhorn, pergament, lack,
    stålspegel,
    \D6 ljus,
    \D4 flaskor fint vin.
F.  \sm{150}.

}

\REM{

# Startutrustning {#startutrustning}

Du har
enkla kläder,
bälte,
ryggsäck,
yllefilt,
vattenskinn,
elddon,
\sm{\D100},
\D4 dagransoner mat,
ett lätt eller långt vapen\sec{vapen}
och en valfri mystisk kraft\sec{krafter}.
Välj därtill en rad från varje lista:


1.  \Tungt{} vapen, rustning, sköld.
2.  \Tungt{} skjutvapen, 3\D6 projektiler, rustning.
3.  \D4 kastvapen, dyrkar, fotanglar, snubbeltråd, mörk kåpa.
4.  Skjutvapen, 2\D6 projektiler, \D4 snaror, nät, lina, krokar.
5.  Imponerande kläder och smycken, signetring, lack, \D6 ljus.
6.  \sm{150}.

i.  2\D4 facklor,
    hammare, 3\D6 spikar,
    rep,
    änterhake,
    krita.
ii. Riktad lykta, \D4 oljeflaskor,
    visselpipa,
    kofot,
    rep,
    säck.
iii.  Penna, bläck, pergament,
    stålspegel,
    spade,
    kortlek,
    tält.

}
\REM{


# Startutrustning {#startutrustning}

Du har
enkla kläder,
bälte,
ryggsäck,
yllefilt,
vattenskinn,
elddon,
\sm{\D100},
\D4 dagransoner mat,
ett lätt eller långt vapen\sec{vapen}
och en valfri mystisk kraft\sec{krafter}.
Välj därtill en från varje kategori:

A.  Valfritt vapen,
    dyrkar och fotanglar,
    imponerande kläder och smycken,
    riddjur,
    lastdjur,
    vaktdjur,
    bok.
B.  Rustning,
    hammare och 3\D6 spikar,
    riktad lykta,
    kofot,
    nät och \D4 snaror,
    penna med bläck och 2\D4 pergament,
C.  Sköld,
    3\D6 projektiler,
    \D4 kastvapen,
    \D4 flaskor vin,
    spade,
    rep med änterhake,
    musikinstrument.
D.  Krita,
    visselpipa,
    stålspegel,
    kortlek,
    lack och \D6 ljus,
    säck.
E.  \D4 oljeflaskor,
    2\D4 facklor,
    fiskelina och krokar,
    tält, rep.

}

\REM{


# Startutrustning {#startutrustning}

Du har
enkla kläder,
bälte,
ryggsäck,
yllefilt,
vattenskinn,
elddon,
\sm{\D100},
\D4 dagransoner mat,
en valfri mystisk kraft\sec{krafter}
och två valfria vapen\sec{vapen}
(om du väljer ett skjutvapen så får du 3\D6 projektiler, om du väljer ett kastvapen så får du \D4 stycken).

Välj därtill:

1.  Rustning, sköld, 2\D4 facklor, spade.
2.  Dyrkar, fotanglar, snubbeltråd, mörk kåpa, riktad lykta.
3.  Imponerade kläder och smycken, signetring, lack, \D6 ljus.
4.  \D4 snaror, nät, fiskelina och krokar, rep.

Välj därtill en från varje rad:

1.  Rustning, dyrkar, imponerande kläder, tält, bok.
2.  Sköld, fotanglar, penna och pergament, fångstnät, spade.
3.  Riktad lykta, stålspegel, kofot, visselpipa, 2\D4 facklor.
4.  \D4 oljeflaskor, hammare och 3\D6 spikar, rep, säck, krita.
5.  Lack och \D6 ljus, visselpipa, änterhake, \D4 snaror, kortlek.

}

\cred{ Svärd och Svartkonst – Spelarens Handbok: val av "paket" }
\cred{ Maze Rats – prylar? }


\REM{

Svärd och svartkonst:

# Övrig utrustning

Alla börjar med en ryggsäck, vattenskinn, elddon och proviant för sju dagar. Välj sedan ett av följande paket:

A: Stor säck, lykta, 3 oljeflaskor, 12 järnspikar, hammare, spade.
B: 2 stora säckar, 8 facklor, påle, rep, änterhake.
C: 4 små säckar, liten stålspegel, visselpipa, krita, kofot.


# Startutrustning {#startutrustning}

Du har
enkla kläder,
bälte,
ryggsäck,
yllefilt,
vattenskinn,
elddon,
\sm{\D100},
\D4 dagransoner mat,
ett lätt eller långt vapen\sec{vapen}
och en valfri mystisk kraft\sec{krafter}.
Välj därtill två från varje kolumn:

-------------------- ------------------ ----------------- --------------
rustning             sköld              snubbeltråd       fiskelina
\tungt{} vapen       kofot              \D4 kastvapen     visselpipa
\tungt{} skjutvapen  mörk kåpa          3\D6 projektiler  stålspegel
dyrkar               skjutvapen         nät & \D4 snaror  spade
riktad lykta         fotanglar          20 meter rep      kortlek
fina kläder          smycken            2\D4 facklor      säck
signetring & lack    pergament          penna & bläck     \D6 ljus
riddjur              lykta              hammare           3\D6 spikar
tält                 änterhake          \D4 oljeflaskor   krita
-------------------- ------------------ ----------------- ------------

}

\REM{

# Startutrustning {#startutrustning}

Du har
enkla kläder,
ett bälte,
en ryggsäck,
en yllefilt,
ett vattenskinn,
ett elddon,
\sm{\D100},
\D4 dagransoner mat,
och ett lätt eller långt vapen\sec{vapen}.
Välj därtill tre:

|    |                                     |    |
|---:|-------------------------------------|---:|-------------------------------------
|  1.| rustning, 2\D4 facklor              | 10.| 2\D4 facklor, kofot, säck
|  2.| \tungt{} vapen, sköld               | 11.| mörk kåpa, snubbeltråd
|  3.| \tungt{} skjutvapen                 | 12.| hammare, 3\D6 spikar, rep
|  4.| skjutvapen, lykta                   | 13.| visselpipa, nät, \D4 snaror
|  5.| \D4 kastvapen, sköld                | 14.| \D4 oljeflaskor, spade, rep
|  6.| riktad lykta, stålspegel            | 15.| penna, bläck, pergament
|  7.| fina kläder, smycken                | 16.| signetring, lack, \D6 ljus
|  8.| riddjur, tält, spade                | 17.| änterhake, rep, kofot
|  9.| dyrkar, fotanglar, säck             | 18.| 3\D6 projektiler, rep, krita

}

\REM{

# Startutrustning {#startutrustning}

Du har
enkla kläder,
ett bälte,
en ryggsäck,
en yllefilt,
ett vattenskinn,
ett elddon,
\sm{\D100},
\D4 dagransoner mat,
och ett lätt eller långt vapen\sec{vapen}.
Välj därtill tre:

|    |                                     |    |
|---:|-------------------------------------|---:|-------------------------------------
|  1.| ett vapen\sec{vapen}                | 10.| en sköld
|  2.| ett \tungt{} vapen\sec{vapen}       | 11.| hammare, spade & kofot
|  3.| 3 kastvapen\sec{vapen}              | 12.| imponerande kläder
|  4.| en rustning\sec{rustning}           | 13.| ett smycke
|  5.| dyrkar                              | 14.| ett tält och kokärl
|  6.| ett arbetsdjur                      | 15.| en signetring & lack
|  7.| 3 snaror & ett nät                  | 16.| en penna, bläck & 5 pergament
|  8.| en riktad lykta                     | 17.| en bok på valfritt ämne
|  9.| en änterhake & 20 m rep             | 18.| en livegen tjänare \ex{nivå 0}

-------------------- ------------------ ----------------- ------------
fina kläder          smycken            2\D4 facklor      säck
signetring & lack    pergament          penna & bläck     \D6 ljus
riddjur              lykta              hammare           3\D6 spikar
tält                 änterhake          \D4 oljeflaskor   krita
-------------------- ------------------ ----------------- ------------



# Äventyrarutrustning {#utrustning}

Du har ett äventyrarkit bestående av:

|    |                  |    |
|---:|------------------|---:|---------------------------
|  1.| enkla kläder     | 11.| en sköld\sec{rustning}
|  2.| ett bälte        | 12.| ett valfritt vapen\sec{vapen}
|  3.| en ryggsäck      | 13.| en fiskelina
|  4.| en yllefilt      | 14.| en slev
|  5.| ett vattenskinn  | 15.| en kniv
|  6.| ett elddon       | 16.| metkrokar
|  7.| 10 meter rep     | 17.| en spade
|  8.| ett kokkärl      | 18.| en krita
|  9.| en visselpipa    | 19.| \D6 facklor\sec{ljus}
| 10.| en stålspegel    | 20.| \D6 dagsransoner

|                            |                          |
|----------------------------|--------------------------|-----------------
| rustning\sec{rustning}     | hammare, 3\D6 spikar     | dyrkar
| valfritt vapen\sec{vapen}  | 3\D6 projektiler         | riktad lykta
| \D4 kastvapen \sec{vapen}  | änterhake, 10 meter rep  | fina kläder
| penna och bläck            | \D6 pergament            | musikinstrument
| lockhorn
| snaror
| nät

}
\REM{

# Startutrustning {#startutrustning}

Du äger förutom grundläggande personlig utrustning
\ex{enkla kläder,
ryggsäck,
filt,
vattenskinn,
elddon,
kokkärl,
kniv…}
ett valfritt vapen\sec{vapen},
en sköld\sec{rustning}
och \sm{35}.
Välj därtill två rader från varje kolumn nedan
och köp till förbrukningsvaror
\ex{facklor, pilar, ljus, lampolja, papper, spikar, snaror…}
och färdkost
för \sm{1} per styck\sec{prislista}.

|                                  |                                  |
|----------------------------------|----------------------------------|
| rustning\sec{rustning}           | penna och bläck                  |
| ett vapen/3 kastvapen\sec{vapen} | ett verktyg                      |
| dyrkar                           | visselpipa eller stålspegel      |
| riktad lykta\sec{ljus}           | 10 meter rep                     |
| änterhake+10 meter rep           | griffeltavla och kritor          |
| fina kläder och smycken          | lykta\sec{ljus}                  |
| musikinstrument                  | nät, fiskelina och krokar        |

}
\REM{

# Startutrustning {#startutrustning}

\FANTASY

Du äger förutom grundläggande personlig utrustning
\ex{enkla kläder,
ryggsäck,
filt,
vattenskinn,
elddon…}
ett valfritt vapen\sec{vapen},
en sköld\sec{rustning},
ett verktyg
och \sm{35}.
Välj därtill en från varje kolumn:

|    |                           |                           |                           |
|---:|---------------------------|---------------------------|---------------------------|
|  1.| rustning\sec{rustning}    | valfritt vapen\sec{vapen} | penna & bläck             |
|  2.| musikinstrument           | 3 kastvapen\sec{vapen}    | stålspegel                |
|  3.| 5 läkande omslag          | ett verktyg               | visselpipa                |
|  4.| änterhake & rep           | riktad lykta\sec{ljus}    | 10 meter rep              |
|  5.| fina kläder               | krita & griffeltavla      | lykta\sec{ljus}           |
|  6.| bok                       | dyrkar                    | metrev                    |

Komplettera med förbrukningsvaror
\ex{facklor, pilar, ljus, lampolja, papper, spikar, fotanglar, snaror…}
och färdkost
för \sm{1} per styck\sec{prislista}.

\REM{Alternativ:
Tält
Teleskop
Arbetsdjur (jakt/vakt/last/drag)
Riddjur
Livegen tjänare
}

\REM{Personlig utrustning:
bälte
slev
}

}

\REM{
# Startutrustning {#startutrustning}

Förutom personlig utrustning så äger du
ett valfritt vapen\sec{vapen}
\ex{kastvapen får du 4 av, skjutvapen får 10 projektiler},
en sköld\sec{rustning},
mat och vatten för 2 dagar
och \sm{25}.
Välj därtill ett paket:

\newcommand{\olja}{\& olja\ex{5}}
\newcommand{\omslag}{läkande omslag}
\newcommand{\vapen}{ett till vapen}

| Stridspaketet              | Skuggpaketet               | Äventyrspaketet            |
|----------------------------|----------------------------|----------------------------|
| rustning\sec{rustning}     | \vapen\sec{vapen}          | \omslag\sec{läkekonst}\ex{5} |
| \vapen\sec{vapen}          | änterhake & rep            | spade                      |
| hammare & spik\ex{5}       | dyrkar                     | 10 meter rep               |
| kofot                      | fotanglar\ex{3}            | krita & griffeltavla       |
| visselpipa                 | stålspegel                 | björnsax                   |
| 5 facklor\sec{ljus}        | 5 vaxljus\sec{ljus}        | lykta\sec{ljus} \olja{}    |

\ex{En siffra inom parentes avser antal användningar för utrustningen.}
}

\REM{
# Startutrustning {#startutrustning}

Förutom personlig utrustning så äger du
ett valfritt vapen\sec{vapen}
\ex{kastvapen får du 4 av, skjutvapen får du 10 projektiler till},
en sköld\sec{rustning},
4 dagsransoner mat
och \sm{25}.

Välj därtill en från varje rad:

\newcommand{\olja}{\& olja\ex{5}}
\newcommand{\omslag}{läkande omslag}
\newcommand{\vapen}{ett till vapen}

|   |                            |                            |                            |
|---|----------------------------|----------------------------|----------------------------|
| 1.| rustning\sec{rustning}     | \omslag\sec{läkekonst}\ex{5} | fina kläder              |
| 2.| \vapen\sec{vapen}          | lykta\sec{ljus} \olja{}    | änterhake & rep            |
| 3.| dyrkar                     | hammare & spik\ex{5}       | 5 facklor\sec{ljus}        |
| 4.| visselpipa                 | stålspegel                 | krita                      |
| 5.| fotanglar\ex{3}            | spade                      | kofot                      |

\ex{Siffror inom parenteser avser antal användningar.}
}

\REM{ DEN HÄR VAR HELT OK MEN SAKNAR HJÄLM

\newcommand{\eller}{{\small{}\emph{eller}}}

Förutom personliga ägodelar \ex{se rollformuläret} börjar du med:

1.  ett lätt \vapenx{} \eller{} ett långt \vapenx{},
2.  en \sköldx{} \eller{} ett \skjutvapenx{} med 10 projektiler,
3.  tre \omslagx{} \eller{} en \oljelyktax{} \eller{} en dyrksats,
4.  tre \kastvapenx{} \eller{} fem \facklorx{} \eller{} ett rep,
5.  ett verktyg \ex{spade, hammare, kofot, såg, tång, sax…},
6.  en småsak \ex{krita, visselpipa, stålspegel, snöre, tärning…},
7.  fem dagsransoner mat,
8.  och 25 silvermynt (\smsymbol{}).

}
\REM{
# Startutrustning 2

Utöver personliga ägodelar \ex{se rollformuläret} börjar du med:
Välj en sak per rad per värde på din motsvaranderade \GE:

Välj lika många saker per rad som värdet på din angivna \GE:

|      |              |              |                          |
|------|--------------|--------------|--------------------------|
| \STY | \lättvapenx  | \långtvapen  | 3 \kastvapen           |
| \TÅL | \sköldx      | \hjälmx      | presenning med snören    |
| \PRE | \skjutvapen  | dyrksats     | änterhake & 20 meter rep |
| \VIS | \oljelyktax  | 4 \facklor | 4 \omslagx             |
| \UTS | mantel       | instrument   | skrivdon & 4 pergament |

Välj därtill ett verktyg \ex{spade, kofot, hammare & spik\ex{5}, såg, tång, sax…}
och en småsak \ex{krita, visselpipa, stålspegel, snöre, tärning, vinflaska…}


- \D6 facklor
- \oljelyktax
- björnsax
- presenning
- tält
- bok
- kikare
- arbetsdjur
- mantel
- instrument
- smycke
- \D4 vinflaskor
- Horn
- 10-fotspåle
- fotanglar
- Religiös symbol
- handskar

Lyxartikel: smycke, musikinstrument, kikare, bok


# Startutrustning 5

Utöver personliga ägodelar \ex{se rollformuläret} har du 5 dagsransoner mat och 25 \silvermyntx{} (\smsymbol).
Välj därtill 2 från varje:

1.  
\lättvapenx{},
\långtvapen{},
\skjutvapen{} med 10 projektiler,
3 \kastvapen{},
\sköldx,
\hjälmx.

2.  
4 \omslagx,
\oljelyktax,
hammare & 10 spikar,
dyrksats,
änterhake & 20 meter rep,

3.  
spade,
kofot,
tång,
skrivdon & 5 pergament,
rep,
5 \facklor,
fotanglar

3.  
spegel,
visselpipa,
krita,
snöre,
sax,
handskar,

4.  
vinflaska,
lack,
vaxljus,
stor säck,
3 små säckar.
}

\REM{

Beväpna dig med två av dessa:
\lättvapenx,
\långtvapen,
\skjutvapen,
3 \kastvapen,
\sköldx,
\hjälmx.



Utrusta dig med en sak från varje rad:

1.  \oljelyktax, dyrkar, 4 \omslagx, änterhake & rep,
2.  spade, kofot, tång, hammare & 10 järnspikar,
3.  rep, 5 \facklor, fotanglar, skrivdon & 4 pergament,
4.  visselpipa, stålspegel, stor säck, koger med 10 pilar,
5.  2 små säckar, snöre, krita, 5 slungstenar i en påse.

Vidare har du personliga ägodelar \ex{se rollformuläret},
5 dagsransoner mat
och \sm{30} (\silvermyntx).


# Startutrustning 2

Du har personliga ägodelar \ex{se rollformuläret},
5 dagsransoner mat
och \sm{\D4\ggr{10}} (\silvermyntx).
Välj beväpning:

1.  ett \lättvapenx{}, ett \kastvapenx{} och en \sköldx{},
2.  ett \långtvapenx{}, ett \kastvapenx{} och en \hjälmx{},
3.  ett \skjutvapenx{} med 10 projektiler och ett \lättvapenx{}.
4.  ett \skjutvapenx{} med 10 projektiler och ett \långtvapenx{}.

<!-- end of list -->

Välj ett utrustningspaket:

1.  \oljelyktax{}, spade, visselpipa, skrivdon, 4 pergament.
2.  dyrksats, hammare, 10 järnspikar, rep, stålspegel.
3.  tång, 4 \omslagx{}, stor säck, krita, snöre.
4.  änterhake, rep, kofot, fotanglar, 5 \ljus{}.


# Startutrustning 2
}\REM{

Du har personliga ägodelar \ex{se rollformuläret},
5 dagsransoner mat,
4 \facklorx{}
och \D4\ggr{10} silvermynt (\smsymbol).
Välj därtill din \xr{beväpning}{vapen} samt ett utrustningspaket nedan \ex{en rad per lista}.

1.  Ett \term{kortsvärd}{vapen}, ett \term{kastspjut}{vapen} och en \sköldx{}.
2.  Ett \term{kortsvärd}{vapen} och en \term{kortbåge}{vapen} med 10 pilar.
3.  Ett \term{spjut}{vapen}, en \term{kastyxa}{vapen} och en \hjälmx{}.
4.  Ett \term{spjut}{vapen} och en \term{kortbåge}{vapen} med 10 pilar.

<!-- end of list -->

1.  \Oljelykta{}, spade, fotanglar, skrivdon, 4 pergament. \REM{39}
2.  Dyrksats, rep, stålspegel, hammare, 10 järnspikar. \REM{55}
3.  Tång, krita, snöre, stor säck, tält, 4 vinflaskor. \REM{39}
4.  Änterhake, rep, kofot, visselpipa, 2 \vaxljus{}. \REM{38}

# Startutrustning 3

Välj beväpning samt ett utrustningspaket nedan \ex{en rad per lista}.
Därtill har du kläder på kroppen, en ryggsäck, ett elddon och 30 silvermynt (\smsymbol).

1.  Ett \termx{kortsvärd}{vapen}, ett \term{kastspjut}{vapen} och en \sköldx{}.
2.  Ett \term{kortsvärd}{vapen} och en \term{kortbåge}{vapen} & pilar.
3.  Ett \term{spjut}{vapen}, en \term{kastyxa}{vapen} och en \hjälmx{}.
4.  Ett \term{spjut}{vapen} och en \term{kortbåge}{vapen} & pilar.

<!-- end of list -->

1.  Lägerutrustning, en spade och en stålspegel.
2.  En kofot, \facklorx{} och ett skrivdon & pergament.
3.  Klätterutrustning, en tång och krita.
4.  En \lykta{} & lampolja, säckar och snöre.

\REM{ Alla bär på elddon? Eller ingår i lägerutrustning? }


# Startutrustning

\REM{
Välj så många saker nedan som du själv kan/vill \xr{bära}{utrustning}.
Utöver dessa så äger du dina kläder, en ryggsäck, och 50 \silver{} (\smsymbol{}).
}

Du äger kläder, en ryggsäck och \sm{50} (\silver{}).
Välj därtill upp till \xr{8 saker}{utrustning} nedan.
\ex{Du får välja samma sak flera gånger.}
\
\columnTwo

1.  ett \vapenx{}
2.  en \skoldx{}
3.  en \hjalmx{}
4.  en \rustningx{} (\tung{})
5.  \facklorx{}
6.  en \oljelyktax{}
7.  lägerutrustning
8.  färdkost
9.  vatten
10. bandage

\next{\columnTwo}

11. klätterutrustning
12. ett verktyg \ex{spade, kofot…} \REM{kofot/bräckjärn: 1400-tal?}
13. en spegel
14. en kompass \REM{ 1300-tal. Setting? }
15. ett musikinstrument
16. en dekokt \ex{vad?} i flaska \REM{syra, gift, sömnmedel, "napalm", sprit, blod…}
17. en stor säck
18. dyrkar
19. skrivdon & pergament
20. en lång påle

\columnend

\REM{
13. en hammare & spikar  % Del av klätterutrustning.
Teleskop [17-tal]
Förstoringsglas (424 före kristus)
En flaska \ex{syra, gift, sömnmedel, nafta…}
14. En dekokt i flaska \REM{syra, gift, sömnmedel, "napalm", sprit, blod…}
19. kritor & griffeltavla
}

# Startutrustning 2
}

\hyphenation{klätt-er-ut-rust-ning}

Du äger kläder, en ryggsäck och \sm{50} (\silver{}).
Gör därtill 3 val från vardera listan nedan.
\REM{Komplettera med färdkost, vatten, bandage, lägerutrustning och facklor efter behov.
}\
\columnTwo

a.  stridsyxa och \skoldx{}
a.  spjut (\termx{långt}{vapen})
a.  två \termx{kastspjut}{avstånd}

\next{\columnTwo}

d. \term{pilbåge}{avstånd} med pilar
a.  \hjalmx{}
a.  \rustningx{} (\tungx{})

\columnend

\noindent
\minipagebegin{[t]}{0.30\columnwidth}

1.  spade
1.  kofot
1.  oljelykta
1.  visselpipa

\next\minipagebegin{[t]}{0.30\columnwidth}

5.  säck
1.  tång
1.  skrivdon \REM{krita, penna?}
1.  kompass

\next\minipagebegin{[t]}{0.40\columnwidth}

9.  stålspegel
1.  dyrkar
1.  vinflaska
1.  klätterutrustning

\columnend

\REM{
1.  Färdkost
2.  Vatten
3.  Bandage
4.  Lägerutrustning
5.  Facklor
}
