# Bakgrund

Du är \D10+13 år gammal och har jobbat som (\D6): 1–3 jordbrukare, 4–5 hantverkare, 6 annat.

    Jordbrukare (\D12)  Hantverkare (\D8)  Övriga (\D8)
--- ------------------- ------------------ --------------
1   Jägare              Skräddare          Handlare
2   Fiskare             Slaktare           Apotekare
3   Herde               Smed               Budbärare
4   Dräng/piga          Krukmakare         Uppassare
5   Skogshuggare        Garvare            Milis
6   Gruvarbetare        Bagare             Notarie
7   Oxdrivare           Snickare           Klockslagare
8+  Bonde               Skomakare          Underhållare


\REM{
Stenhuggare
uppassare
borstbindare
dödgrävare
timmerman
utkastare
}


}
