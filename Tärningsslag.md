\REM{
För att avgöra om du lyckas med något kan du behöva \intro{slå} din för situationen mest relevanta \GE{}.
Då slår du lika många tärningar \ex{sexsidiga} som dess värde och \intro{lyckas} om någon av dem visar minst

\REM{* 5 under normala omständigheter.}
* 5 om omständigheterna är neutrala.
* 4 om omständigheterna ger dig en \mbox{betydande} \intro{fördel}.
* 6 om omständigheterna ger dig en \mbox{betydande} \intro{nackdel} \ex{trumfar \keyI{fördel}}.

\REM{
För att avgöra om du lyckas med något kan du behöva \intro{slå för en \GE{}}.
Då slår du lika många tärningar som värdet på din för situationen mest relevanta \GE{}.
Du \intro{lyckas} om någon av tärningarna visar minst

I normala fall så \intro{lyckas} du om minst en tärning visar en femma eller sexa.
Om omständigheter ger dig betydande \intro{fördel} så \termI{lyckas} du även med en fyra.
Om de ger dig \intro{nackdel} så behöver du en sexa.
\keyI{Nackdel} trumfar \keyI{fördel}.
}

\REM{ Variant med fixt målvärde men tärningsantalet varierar. }

}För att avgöra huruvida du lyckas med något kan du behöva \intro{slå} din för situationen mest relevanta \GE{}.
Då slår du lika många tärningar \REM{sexsidiga} som dess värde
och räknar femmor och sexor som \intro{träffar}.
Du \intro{lyckas} (helt eller delvis) om du får minst en \traff{}.
\REM{Oftast \intro{lyckas} du om du får minst en \traff{}.}
\REM{men det kan ibland behövas flera.}
\REM{Tärningar som visar femmor och sexor räknas som \REM{kallas för} \intro{träffar}.}


Om omständigheter ger dig betydande \intro{fördel} räknas även fyror som \traffar{}.
Om de ger dig betydande \intro{nackdel} \ex{trumfar \fordel} räknas endast sexor.
\REM{\Nackdel{} trumfar \fordel{}.}

\REM{
Om omständigheter ger dig en betydande \intro{fördel} får du slå en extra tärning.
Om de ger dig betydande \intro{nackdel} slår du en extra tärning men bortser från en \traff{}.
\Nackdelar{} och \fordelar{} är inte kumulativa och \nackdel{} trumfar \fordel{}.

Om omständigheter ger dig en betydande \intro{fördel} eller \intro{nackdel} slår du en extra tärning,
men vid \nackdel{} bortser du från en \traff{}.
\Nackdelar{} och \fordelar{} är inte kumulativa och \nackdel{} trumfar \fordel{}.
}

\REM{
För att avgöra om du lyckas med något kan du behöva \intro{slå} din för situationen mest relevanta \GE{}.
Då slår du lika många tärningar \ex{sexsidiga} som dess värde.
Om omständigheter ger dig en betydande \intro{fördel} eller \intro{nackdel} slår du en extra tärning,
men vid \nackdel{} \ex{trumfar \fördel{}} måste du bortse från den högsta.
Du \intro{lyckas} om någon av tärningarna visar minst 5.
}
