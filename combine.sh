#!/bin/bash

set -o nounset
set -o errexit
#set -o verbose


function usage {
  echo $"Usage: $0 {cards|book|wiki} jobname [{1L|2P|4L|8P|9L|16L|25L|RF}] [pages]"
  exit 1
}


mkdir -p "${WORKDIR:=tmp}"
mkdir -p "${OUTDIR:=out}"

gpp="gpp -x -T --nostdinc -I. --include latex.gpp"
jobname=$(basename -s .md "$2")
jobdir=$(dirname "$2")
jobfile="${jobdir}/${jobname}.md"
jobfile="$2"

# Make sure job file exists.
if [ ! -r "$jobfile" ]
  then
    echo "Error: input file '$jobfile' does not exist."
    usage
fi

COMBINATION=${1:-}
if [ -n "$COMBINATION" ]
then  # Pages will be combined.

    # Make sure chosen combination is valid.
    if [ ! -r "combine/combine$COMBINATION.tex" ]
    then
        usage
    fi

fi

# Combine pages.

    PAGES=${3:-"1-"}

    cp "$jobfile" "$WORKDIR/combine.pdf"
    cp "$jobfile" "$WORKDIR/combineIn.pdf"
    gpp -T -DJOBNAME="combineIn" -DPAGES="$PAGES" "combine/combine${COMBINATION}.tex" > "$WORKDIR/combine.tex"
    tectonic --print "$WORKDIR/combine.tex"

    datedpdf="$OUTDIR/$(date +%Y-%m-%d) $jobname $COMBINATION.pdf"
    cp "$WORKDIR/combine.pdf" "$datedpdf"
    open "$datedpdf"

  else  # Don't combine pages (default).

    datedpdf="$OUTDIR/$(date +%Y-%m-%d) $jobname.pdf"
    cp "$WORKDIR/$jobname.pdf" "$datedpdf"
    open "$datedpdf"

fi
