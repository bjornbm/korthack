*   Se till att ha redundans på viktig \utrustningx{} i gruppen. \REM{såsom vatten, bandage och \ljuskallor{}.}
*   Skatter kan vara \tunga{}, se till att ni \term{orkar}{utrustning} bärga dem.
*   Att vara utan \ljusx{} i underjorden är en nästan säker död.
*   \xr{Anställ}{hyrd-hjälp} \tjanare{} och \vagvisare{} för att hjälpa dig bära \ljuskallor{} och \utrustning{} \ex{och skatter om du vågar lita på dem}.
*   \term{Utrusta}{prislista} era \term{anställda}{hyrd-hjälp} så att de överlever längre. \REM{\term{Anställda}{hyrd-hjälp} överlever längre om ni rustar dem ordentligt.}
*   \Stridx{} är dödligt och bör ses som en sista utväg.
*    Inled aldrig en \strid{} frivilligt utan ett rejält övertag.
*   \Tungax{} \vapenx{} kan vara effektiva vid en stormning.
*   \Langavapen{} kan vara effektiva mot en stormning.
*   Du kan inte smyga, simma eller sova iklädd \rustning{}. \TODO{ Överflödig? }
