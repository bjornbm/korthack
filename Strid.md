\REM{
\REM{
\Slåx{\KRP} när du \intro{attackerar} någon och lägg till ditt \vapensx{} \term{bonus}{vapen}.
Resultatet är den \skadax{} din fiende får.
\Skadan{} från ett \mkttungt{} \vapen{} får spridas över flera fiender.
}

När du \intro{attackerar} någon får hen \skadax{} lika med \sKRP{} plus ditt \vapensx{} \term{bonus}{vapen}.
\Skadan{} från ett \mkttungt{} \vapen{} får spridas över flera närbelägna fiender.

\Langavapen{} används med två händer och ger dig en gratis attack mot fiender som stormar dig utan \langavapen{} \ex{förutsatt att du inte redan är inblandad i närstrid}.

\REM{\Långavapen{} används med två händer och ger dig en gratis attack när fiender stormar dig \ex{om du inte redan är inblandad i närstrid}.}

Du kan inte använda \skjutvapen{} om du är inblandad i närstrid\REM{ och du får \nackdel{} om du använder \avståndsvapen{} mot någon annan som är inblandad i närstrid}.

Du kan som ett \dragx{} \intro{försvara} någon.
Fram till ditt nästa \drag{} hanteras då attacker mot henom istället som attacker mot dig.
Om du försvarar dig själv så får attacker mot dig \nackdelx{}.


\REM{

\Skjutvapen, \långavapen{} och \mkttunga{} \vapen{} används med två händer.
Ett \långtvapen{} kan ge \gratisattackerx{} mot angripare
och \skadan{} från ett \mkttungt{} \vapen{} får spridas över flera fiender.

Du kan inte använda \skjutvapen{} om du är inblandad i närstrid\REM{ och du får \nackdel{} om du använder \avståndsvapen{} mot någon annan som är inblandad i närstrid}.

Du kan som ett \dragx{} \intro{försvara} någon.
Fram till ditt nästa \drag{} hanteras då attacker mot henom istället som attacker mot dig.

Ett \långtvapen{} ger dig en gratis attack mot en fiende som försöker gå i närstrid med dig, förutsatt att hen inte själv har ett \långtvapen{} och att du inte redan slåss med någon.

Ett \långtvapen{} ger dig en gratis attack första gången en fiende utan attackerar dig, förutsatt att du inte redan slåss med någon.

Ett \långtvapen{} ger dig en gratis attack mot första fienden som försöker gå i närstrid med dig och inte själva har \långavapen{}.

Om du inte redan slåss med någon så ger ett \långtvapen{} dig en gratis attack när en fiende utan försöker gå i närstrid med dig.

Om du inte redan slåss med någon så ger ett \långtvapen{} dig en gratis attack mot fiender som försöker gå i närstrid med dig utan \långavapen.

Om du inte redan slåss med någon så ger ett \långtvapen{} dig en gratis attack mot fiender som stormar dig utan \långavapen{}.

Om du inte redan slåss med någon så ger ett \långtvapen{} dig en gratis attack när fiender utan \långavapen{} stormar dig.

Om du inte redan slåss med någon så ger ett \långtvapen{} dig en gratis attack när någon försöker gå i närstrid med dig.

Om du inte redan slåss med någon så ger ett \långtvapen{} dig en gratis attack när någon stormar dig.

Ett \långtvapen{} ger dig en gratis attack mot fiender som angriper dig, förutsatt att du inte redan var inblandad i närstrid.

Ett \långtvapen{} ger dig en gratis attack mot fiender som stormar dig, förutsatt att du inte slåss med någon.

Ett \långtvapen{} ger dig en gratis attack mot den första fiende som stormar dig.

Ett \långtvapen{} ger dig gratis attacker mot fiender som stormar dig om du inte redan är inblandad i närstrid.
}

\REM{
\Långavapen{} och \mkttunga{} \vapen{} används med \term{två händer}{bärförmåga-och-belastning}.

Ett \långtvapen{} kan ge dig \gratisattackerx{} mot angripare utan.

Du får sprida \skadan{} från ett \mkttungt{} \vapen{} över flera fiender.
}
\REM{Du får sprida \skadan{} från ett \mkttungt{} \vapen{} över flera fiender i en grupp (gör bara en \prövningx{} för att träffa).}

\REM{I närstrid får du \fördel{} när du attackerar någon som inte ser dig eller saknar något att försvara sig med.}


# Strid
}
\REM{
Du kan \intro{attackera} någon för att skada henom eller med våld försöka tvinga henom till något.
Det går till som att \slaforx{\KRP} men resultatet jämförs med din fiendes \intro{försvarsvärde} (\introFV).

Om resultatet >\introFV{} så \lyckas{} attacken och din fiende måste välja mellan att underkasta sig din vilja eller bli \sarad{}.
Du får dessutom genast utföra en till attack mot henom eller en någon annan intill dig.

Du kan \intro{attackera} någon för att skada eller med våld tvinga henom till något.
Då \slarduforx{\KRP} men \lyckas{} om resultatet är högre än din fiendes \intro{försvarsvärde} (\introFV{}).

Du kan \intro{attackera} någon för att skada eller med våld tvinga henom till något.
Det går till som att \slaforx{\KRP} men du \lyckas{} om resultatet är högre än din fiendes \intro{försvarsvärde} (\introFV{}).

Du kan \intro{attackera} någon för att skada eller med våld tvinga henom till något.
\Slaforx{\KRP} men jämför resultatet med din fiendes \intro{försvarsvärde} (\introFV);
om resultatet >\introFV{} så \lyckas{} attacken.

Om attacken \lyckas{} måste din fiende välja mellan att underkasta sig din vilja eller bli \saradx{}.
Du får dessutom genast utföra en till attack mot henom eller någon annan intill er.
}

Du kan \intro{attackera} någon för att \sarax{} eller med våld försöka tvinga henom till något.
\Slaforx{\KRP} men jämför resultatet med din fiendes \introA{försvarsvärde}{fv}.

Om resultatet >\introFV{} så lyckas din attack och fienden måste välja mellan att underkasta sig din vilja eller bli \sarad{}.
Du får dessutom genast utföra en till attack \ex{mot henom eller någon annan}.

\REM{
\Slaforx{\KRP} men justera resultatet med ditt \vapens{} \bonus{} och jämför det med din fiendes \intro{försvarsvärde} (\introFV).

Om resultatet >\introFV{} så \lyckas{} attacken och din fiende måste välja mellan att underkasta sig din vilja eller bli \sarad{}.
Du får dessutom genast utföra en till attack mot henom eller en annan fiende intill dig.
}



\REM{
Om resultatet >\introFV{} måste hen välja mellan att underkasta sig din vilja eller bli \sarad{} (\svartsarad{} om >\introFV+2).

* Om resultatet är > fiendens \introFV{} blir fienden \saradx{}.
* Om resultatet är > fiendens \dubbelt{\introFV} blir fienden \svartsarad{}.
}

\REM{
Ditt \introFV{} är lika med din \KRP{}+2 men en \intro{rustning} och/eller en \intro{sköld} ökar ditt \introFV{} med +1 vardera.

Ditt \introFV{} är lika med din \KRP{}+2 men kan höjas av en \intro{rustning} (+1) och/eller en \intro{sköld} (+1).

Ditt \introFV{} är lika med din \KRP{}+2 men ökar till \KRP+3 om du bär \intro{rustning} eller \intro{sköld} och \KRP+4 om du bär båda.

Ditt \introFV{} är lika med \KRP+2 samt +1 vardera för \intro{rustning} och \intro{sköld}.
}

Ditt \introFV{} är lika med 3+\KRP{}.
En \intro{rustning} ger dig ytterligare +1 och en \tungx{} rustning ger +2.

Du kan som ett \dragx{} \intro{försvara} någon.
Fram till ditt nästa \drag{} hanteras då attacker mot henom istället som attacker mot dig.
Om du försvarar dig själv så får attacker mot dig \nackdelx{}.
