\FANTASY{ Silvermynt }

\tabbingbegin
\qqquad\TS\qquad\kill

\secx{$x$}      \TT{}hänvisning till kort $x$
\TE{}\FP        \TT{}\xrr{försvarspoäng}
\TE{}\GE        \TT{}\grundegenskapx{}
\TE{}\HVD       \TT{}\xr{huvud}{grundegenskaper}
\TE{}\KRP       \TT{}\xr{kropp}{grundegenskaper}
\TE{}\RP        \TT{}\xr{rollperson}{introduktion}
\TE{}\smsymbol  \TT{}silvermynt
\TE{}\SL        \TT{}\xr{spelledare}{introduktion}
\TE{}\SLP       \TT{}\xr{spelledarperson}{introduktion}
\TE{}\UTS       \TT{}\xr{utstrålning}{grundegenskaper}

\tabbingend

\REM{

# Termer

*   Bra
*   Mkt bra
*   Tung
*   Mkt tung
*   Sekunder
*   Timmar
*   Dagar
*   Nära
*   Långt
*   Arbete
*   Tröskel
*   Arena
*   Grad
*   Lindrig
*   Allvarlig
*   Svår
*   Utmaning
*   Öka nivå
*   Erfarenhet
*   Guld

*   Du
*   Rollperson
*   Karaktär
*   Spelledare
}


\REM{
Nedan visas hur man får till en vettig tvåkolumnslayout. Notera at nbsp är nödvändigt för att inte kolumnbredderna ska balla ur av någon anledning.


# Förkortningar 2

\columnTwoS

\tabbingbegin

\qqquad\TS\qquad\kill
\secx{$x$}      \TT{}hänvisning till kort $x$
\TE{}\GE        \TT{}\grundegenskapx{}
\TE{}\HVD       \TT{}\xr{huvud}{grundegenskaper}
\TE{}\KP        \TT{}\xrr{kroppspoäng}
\TE{}\KRP       \TT{}\xr{kropp}{grundegenskaper}
\TE{}\mkt       \TT{}mycket
\TE{}\RP        \TT{}\xr{rollperson}{introduktion}
\TE{}\smsymbol  \TT{}silvermynt
\TE{}\SL        \TT{}\xr{spelledare}{introduktion}
\TE{}\SLP       \TT{}\xr{spelledarperson}{introduktion}
\TE{}\D$x$      \TT{}$x$-sidig tärning
\TE{}\UTS       \TT{}\xr{utstrålning}{grundegenskaper}

\tabbingend

 

\next{\columnTwoS}

\tabbingbegin
\qqquad\TS\qquad\kill
\secx{$x$}      \TT{}hänvisning till kort x \TE{}\GE        \TT{}\grundegenskapx{}
\TE{}\HVD       \TT{}\xr{huvud}{grundegenskaper}
\TE{}\KP        \TT{}\xrr{kroppspoäng}
\TE{}\KRP       \TT{}\xr{kropp}{grundegenskaper}
\TE{}\mkt       \TT{}mycket
\TE{}\RP        \TT{}\xr{rollperson}{introduktion}
\TE{}\smsymbol  \TT{}silvermynt
\TE{}\SL        \TT{}\xr{spelledare}{introduktion}
\TE{}\SLP       \TT{}\xr{spelledarperson}{introduktion}
\TE{}\D$x$      \TT{}$x$-sidig tärning
\TE{}\UTS       \TT{}\xr{utstrålning}{grundegenskaper}
\tabbingend

 

\columnend
}
