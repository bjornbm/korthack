\
\noindent
\intro{Teamsters}
(\KHU{2}{1}{1})
are the rough and tumble crew and workers out in space. If Ripley from Aliens is your hero you’ll want to play a Teamster.

  - Utrustningspaket: A eller B.
  - Specialisering: *Zero G*, *Mechanical Repair*, *Heavy Machinery* eller *Piloting*.
  - Once per session, a Teamster may re-roll a roll on the Panic Effect Table.

\hyphenation{geo-logy}
\hyphenation{ev-ery}
\hyphenation{crew-member}

\noindent
\intro{Scientists}
(\KHU{1}{2}{1})
are doctors, researchers and anyone who wants to slice open aliens (or infected crew members) with a scalpel.

  - Utrustningspaket: A, B eller D.
  - Specialisering: *Biology*, *Hydroponics*, *Geology*, *Computers*, *Mathematics* eller *Chemistry*.
  - When\REM{ever} a Scientist fails a Sanity Save, every crewmember nearby gains 1 Stress.



\noindent
\intro{Marines}
(\KHU{2}{1}{1})
are here to shoot bugs and chew bubblegum. They’re handy in a fight and good when grouped together, but whenever a Marine Panics it may cause problems for the rest of the crew.

  - Utrustningspaket: C.
  - Specialisering: *Firearms*, *Close-Quarters Combat*, *Athletics* eller *Explosives*.
  - When\REM{ever} a Marine Panics, every crewmember nearby must make a Fear Save.



\noindent
\intro{Officers}
(\KHU{1}{1}{2})
are commanders, leaders and negotiators with the soft skills required to build tight crews and negotiate the deals that keep them afloat.

  * Utrustningspaket: A, B, C eller D.
  * Specialisering: valfri \ex{bekräftas med \SL}.
  * When\REM{ever} an Officer fails a Fear Save, every friendly player nearby gains 1 Stress.



\noindent
\intro{Androids}
(\KHU{1}{2}{1})
  are a terrifying and exciting addition to any crew. They tend to unnerve other crew with their cold inhumanity.

  - Utrustningspaket: A, B eller D.
  - Specialisering: *Linguistics*, *Computers* eller *Mathematics*.
  - Fear Saves made in the presence of Androids have Disadvantage.
  - Androider är immuna mot Fear.
  - Androider kan inte öka sin \UTS{}.
